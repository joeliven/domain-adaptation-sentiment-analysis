__author__ = 'Kaiyuan_Wang'

import os
import pickle
import collections
import numpy as np
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from util.util import load_Google_word2vec
from util.constant import UNK, MASK
from util.parameters import SAVE_WORD2IDX_PATH, SAVE_IDX2VEC_PATH, SAVE_IDX2VEC_ARR_PATH, S_PATH, T_PATH, VSM_PATH, VOCAB_PATH, VOCAB_SIZE, WORD2V_D

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)['reviewText']

def collect_vocabulary(data_paths, lower_case=False, remove_stop_words=False):
    if os.path.isfile(VOCAB_PATH):
        with open(VOCAB_PATH, 'rb') as f:
            return pickle.load(f)
    vocabulary = {}
    for data_path in data_paths:
        for review in read_review(data_path):
            for sent in sent_tokenize(review):
                if lower_case:
                    sent = sent.lower()
                for word in word_tokenize(sent):
                    if word not in vocabulary:
                        vocabulary[word] = 1
                    else:
                        vocabulary[word] += 1
    if remove_stop_words:
        for stopword in stopwords.words('english'):
            if stopword in vocabulary:
                del vocabulary[stopword]
    with open(VOCAB_PATH, "wb") as f:
        pickle.dump(vocabulary, f)
    return vocabulary

def most_common_K_words(vocabulary, k):
    counter = collections.Counter(vocabulary)
    return counter.most_common(k)

# def word2vec_model(k_most_common_words, google_word2vec_model):
#     if os.path.isfile(WORD2VEC_MODEL_PATH):
#         with open(WORD2VEC_MODEL_PATH, 'rb') as f:
#             return pickle.load(f)
#     word2vec_model = dict()
#     for common_word in k_most_common_words:
#         if common_word in google_word2vec_model:
#             word2vec_model[common_word] = google_word2vec_model[common_word]
#     with open(WORD2VEC_MODEL_PATH, "wb") as f:
#         pickle.dump(word2vec_model, f)
#     return word2vec_model

def create_word2idx_dict(vocabulary, vocab_size):
    common_word_list = [ tup[0] for tup in most_common_K_words(vocabulary, vocab_size) ]
    # word2vec_dict = word2vec_model(common_word_list, google_word2vec_model)
    common_word_list.insert(0, UNK)
    common_word_list.insert(0, MASK)
    word2idx_dict = { word: idx for idx, word in enumerate(common_word_list) }
    with open(SAVE_WORD2IDX_PATH, "wb") as f:
        pickle.dump(word2idx_dict, f)
    return word2idx_dict

def create_idx2vec_arr(word2idx_dict, word2vec_dict, WORD2VEC_D):
    UNK_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    MASK_EMBEDDING = np.zeros((WORD2VEC_D,))
    idx2vec_dict = { word2idx_dict[MASK]: MASK_EMBEDDING }
    for word in word2idx_dict.keys():
        if word in word2vec_dict:
            idx2vec_dict[word2idx_dict[word]] = word2vec_dict[word]
        else:
            idx2vec_dict[word2idx_dict[word]] = UNK_EMBEDDING
    idx2v_arr = np.zeros((len(idx2vec_dict), WORD2VEC_D))
    for idx, vec in idx2vec_dict.items():
        idx2v_arr[idx] = vec
    np.save(SAVE_IDX2VEC_ARR_PATH, idx2v_arr)
    return idx2v_arr

def create_idx2vec_dict(word2idx_dict, word2vec_dict, WORD2VEC_D):
    UNK_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    MASK_EMBEDDING = np.zeros((WORD2VEC_D,))
    idx2vec_dict = { word2idx_dict[MASK]: MASK_EMBEDDING, word2idx_dict[UNK]: UNK_EMBEDDING }
    for word in word2idx_dict.keys():
        if word in word2vec_dict:
            idx2vec_dict[word2idx_dict[word]] = word2vec_dict[word]
        else:
            idx2vec_dict[word2idx_dict[word]] = UNK_EMBEDDING
    with open(SAVE_IDX2VEC_PATH, "wb") as f:
        pickle.dump(idx2vec_dict, f)
    return idx2vec_dict

def create_files_for_autoencoder(S_PATH, T_PATH, VSM_PATH, WORD2VEC_D=300, VOCAB_SIZE=10000, REMOVE_STOPS=False, TO_LOWER=True):
    print 'Building vocabulary...'
    vocabulary = collect_vocabulary([S_PATH, T_PATH], TO_LOWER)
    print 'Done'
    print 'Building word to index dictionary...'
    word2idx_dict = create_word2idx_dict(vocabulary, VOCAB_SIZE)
    print 'Done'
    print len(word2idx_dict)
    print 'Loading Google Word2Vec model...'
    word2vec_dict = load_Google_word2vec(VSM_PATH)
    print 'Done'
    # print 'Building index to word vec array...'
    # idx2vec_arr = create_idx2vec_arr(word2idx_dict, word2vec_dict, WORD2VEC_D)
    # print 'Done'
    print 'Saving index to word vec dictionary...'
    idx2vec_dict = create_idx2vec_dict(word2idx_dict, word2vec_dict, WORD2VEC_D)
    print 'Done'
    # print idx2vec_arr.shape
    print len(idx2vec_dict)

def main():
    create_files_for_autoencoder(S_PATH, T_PATH, VSM_PATH, WORD2VEC_D=WORD2V_D, VOCAB_SIZE=VOCAB_SIZE)

if __name__ == '__main__':
    main()