__author__ = 'Kaiyuan_Wang'

import os
import sys
import itertools
import numpy as np
from nltk.tokenize import sent_tokenize, word_tokenize
from keras.preprocessing import sequence
from keras.utils import np_utils
from keras.models import Model
from keras import backend as K
from keras.models import model_from_json
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import Input, Masking, Dense, Embedding, RepeatVector, Dropout, Activation
from keras.layers.recurrent import LSTM
from keras.regularizers import l2, activity_l2
from autoencoder import load_data
from util.util import save_model, save_model_weights
from util.constant import UNK, MASK
from util.parameters import CLASSIFIER_TRAINING_DATA_PATH, CLASSIFIER_VALIDATION_DATA_PATH, CLASSIFIER_TEST_DATA, AUTOENCODER_PATHS, CLASSIFIER_CHECKPOINTS_WEIGHTS_PATH, CLASSIFIER_SAVE_MODEL_ARCH_PATH, CLASSIFIER_SAVE_MODEL_WEIGHTS_PATH, CLASSIFIER_BATCH_SIZE, CLASSIFIER_EPOCH_SIZE, CLASSIFIER_N_EPOCHS, CLASSIFIER_OPTIMIZER, CLASSIFIER_DROPOUT_LSTM_W, CLASSIFIER_DROPOUT_LSTM_U, CLASSIFIER_DENSE_L2_W, CLASSIFIER_DENSE_L2_B, VOCAB_SIZE, MAXLEN_SENT, MAXLEN_REV, SENT2VEC_D, REV2VEC_D

sys.setrecursionlimit(10000)

def load_model(MODEL_PATH, WEIGHTS_PATH):
    with open(MODEL_PATH, "r") as f:
        model = model_from_json(f.read())
    model.load_weights(WEIGHTS_PATH)
    return model

def load_autoencoders(**kwargs):
    autoencoders = list()
    sorted_keys = sorted(kwargs.keys())
    for key in sorted_keys:
        model_path, weights_path = kwargs[key]
        model = load_model(model_path, weights_path)
        encode_func = K.function([model.get_layer("Input_T").input, K.learning_phase()], [model.get_layer("LSTM_Encoder_L").output])
        autoencoders.append(encode_func)
    return autoencoders

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)

def transform_to_vec(encode_funcs, review_batch, word2idx, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, SENT2VEC_D):
    sentences_batch = [ sent_tokenize(review['reviewText']) for review in review_batch ]
    reviews = [[word_tokenize(sentence) for sentence in sentences] for sentences in sentences_batch]
    for ridx, review in enumerate(reviews):
        for sidx, sentence in enumerate(review):
            reviews[ridx][sidx] = [word if word in word2idx else UNK for word in sentence]
    train_batch_idi = [[[word2idx[word] for word in sentence] for sentence in review] for review in reviews]
    train_batch_idi_padding = sequence.pad_sequences([sequence.pad_sequences(train_batch_idx, MAXLEN_SENT) for train_batch_idx in train_batch_idi], MAXLEN_REV)
    # train_batch_one_hot = np.asanyarray([[np_utils.to_categorical(words, VOCAB_SIZE + 2) for words in sentences] for sentences in train_batch_idi_padding], dtype=np.uint16)
    train_batch_idi_padding = np.asanyarray(train_batch_idi_padding)
    # print train_batch_one_hot.shape
    # print train_batch_idi_padding.shape
    train_data = np.zeros((len(sentences_batch), MAXLEN_REV, len(encode_funcs) * SENT2VEC_D))
    # print train_data.shape
    for r, review in enumerate(train_batch_idi_padding):
        for e, encoder in enumerate(encode_funcs):
            # print review.shape
            encoded_rev = encoder([review, 0])[0]
            # print encoded_rev.shape
            train_data[r, :, e * SENT2VEC_D:(e + 1) * SENT2VEC_D] = encoded_rev
    # for b, batch in enumerate(train_batch_one_hot):
    #     for r, review in enumerate(batch):
    #         for e,encoder in enumerate(encode_funcs):
    #             print review.shape
    #             encoded_rev = encoder([review, 0])[0]
    #             print encoded_rev.shape
    train_labels = np.asanyarray([0 if float(review["overall"]) < 3 else 1 for review in review_batch], dtype=np.uint16)
    return train_data, train_labels
    # train_batch_one_hot = np.asanyarray([np_utils.to_categorical(words, VOCAB_SIZE + 2) for words in train_batch_idx_padding], dtype=np.uint16)
    # return train_batch_idx_padding, train_batch_one_hot

def read_batch(encode_funcs, DATA_PATH, word2idx, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, BATCH_SIZE, SENT2VEC_D):
    review_gtor = read_review(DATA_PATH)
    while True:
        review_batch = list(itertools.islice(review_gtor, BATCH_SIZE))
        if not review_batch or len(review_batch) < BATCH_SIZE:
            break
        yield transform_to_vec(encode_funcs, review_batch, word2idx, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, SENT2VEC_D)

def read_all(encode_funcs, DATA_PATH, word2idx, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, BATCH_SIZE, SENT2VEC_D):
    review_gtor = read_review(DATA_PATH)
    review_all = list(itertools.islice(review_gtor, 0, None))
    return transform_to_vec(encode_funcs, review_all, word2idx, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, SENT2VEC_D)

def build_classifier(AUTOENCODER_PATHS, MAXLEN_REV, SENT2VEC_D, REV2VEC_D, DROPOUT_LSTM_W, DROPOUT_LSTM_U, DENSE_L2_W, DENSE_L2_B):
    NUM_ENCODERS = len(AUTOENCODER_PATHS)
    Input_T = Input(shape=(MAXLEN_REV, NUM_ENCODERS * SENT2VEC_D), name="Input_T")
    Masking_L = Masking(mask_value=0, input_shape=(MAXLEN_REV, NUM_ENCODERS * SENT2VEC_D), name="Masking_L")
    Input_Masked_T = Masking_L(Input_T)
    LSTM_Classifier_L = LSTM(REV2VEC_D, return_sequences=False, name="LSTM_Classifier_L", activation="tanh", inner_activation="hard_sigmoid", dropout_W=DROPOUT_LSTM_W, dropout_U=DROPOUT_LSTM_U)
    Review_Encode_T = LSTM_Classifier_L(Input_Masked_T)
    Dense_L = Dense(output_dim=1, init='glorot_uniform', activation='sigmoid', W_regularizer=l2(DENSE_L2_W), b_regularizer=l2(DENSE_L2_B), name="Dense_L")
    Sentiment_Score_T = Dense_L(Review_Encode_T)
    classifier = Model(input=Input_T, output=Sentiment_Score_T)
    return classifier

def run_classifier(classifier, train_batch_gtor, validation_set, OPTIMIZER, SAVE_MODEL_ARCH_PATH, SAVE_MODEL_WEIGHTS_PATH, CHECKPOINTS_WEIGHTS_PATH, EPOCH_SIZE, N_EPOCHS):
    print 'Compiling classifier...'
    classifier.compile(loss='binary_crossentropy', # 'categorical_crossentropy',
              optimizer=OPTIMIZER,
              metrics=['accuracy'])
    print 'Done'
    print 'Saving model...'
    save_model(classifier, SAVE_MODEL_ARCH_PATH)
    print 'Done'
    early_stopping = EarlyStopping(monitor='val_acc', patience=2)
    checkpoints = ModelCheckpoint(CHECKPOINTS_WEIGHTS_PATH, monitor='val_acc', verbose=0, save_best_only=True, mode='auto')
    callbacks = [checkpoints]
    print 'Training...'
    hist = classifier.fit_generator(train_batch_gtor, EPOCH_SIZE, N_EPOCHS, verbose=1, callbacks=callbacks, validation_data=validation_set, nb_val_samples=None, class_weight={})
    print 'Done'
    print 'Saving model weights...'
    save_model_weights(classifier, SAVE_MODEL_WEIGHTS_PATH)
    print 'Done'
    print 'Classifier training history:'
    print hist.history
    print 'Classifier summary:'
    classifier.summary()

def main():
    print 'Loading word2idx dictionary and idx2vec array...'
    word2idx_dict, idx2vec_arr = load_data()
    print 'Done'
    print 'Loading autoencoders...'
    autoencoders = load_autoencoders(**AUTOENCODER_PATHS)
    print 'Done'
    print 'Reading and transforming training data...'
    train_batch = read_batch(autoencoders, CLASSIFIER_TRAINING_DATA_PATH, word2idx_dict, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, CLASSIFIER_BATCH_SIZE, SENT2VEC_D)
    # print next(train_batch)
    print 'Done'
    print 'Reading and transforming validation data...'
    val_padding, val_one_hot = read_all(autoencoders, CLASSIFIER_VALIDATION_DATA_PATH, word2idx_dict, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, CLASSIFIER_BATCH_SIZE, SENT2VEC_D)
    # print val_padding.shape
    # print val_one_hot.shape
    # print len([x for x in val_one_hot if x == 0])
    print 'Done'

    print 'Build LSTM classifier'
    classifier = build_classifier(AUTOENCODER_PATHS, MAXLEN_REV, SENT2VEC_D, REV2VEC_D, CLASSIFIER_DROPOUT_LSTM_W, CLASSIFIER_DROPOUT_LSTM_U, CLASSIFIER_DENSE_L2_W, CLASSIFIER_DENSE_L2_B)
    print 'Done'
    print 'Run LSTM autoencoder'
    run_classifier(classifier, train_batch, (val_padding, val_one_hot), CLASSIFIER_OPTIMIZER, CLASSIFIER_SAVE_MODEL_ARCH_PATH, CLASSIFIER_SAVE_MODEL_WEIGHTS_PATH, CLASSIFIER_CHECKPOINTS_WEIGHTS_PATH, CLASSIFIER_EPOCH_SIZE, CLASSIFIER_N_EPOCHS)
    print 'Done'

if __name__ == '__main__':
    main()