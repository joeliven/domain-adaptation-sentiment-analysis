__author__ = 'Kaiyuan_Wang'

import os
import sys
import pickle
import itertools
import numpy as np
from nltk.tokenize import sent_tokenize, word_tokenize
from keras.preprocessing import sequence
from keras.utils import np_utils
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models import Model
from keras.layers import Input, Dense, Embedding, RepeatVector, Dropout, Activation
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.regularizers import l2, activity_l2
from util.util import save_model, save_model_weights
from util.constant import UNK, MASK
from util.parameters import AUTOENCODER_TRAINING_DATA_PATH, AUTOENCODER_VALIDATION_DATA_PATH, AUTOENCODER_CHECKPOINTS_WEIGHTS_PATH, AUTOENCODER_SAVE_MODEL_ARCH_PATH, AUTOENCODER_SAVE_MODEL_WEIGHTS_PATH, AUTOENCODER_BATCH_SIZE, AUTOENCODER_EPOCH_SIZE, AUTOENCODER_N_EPOCHS, AUTOENCODER_DROPOUT_DLSTM_U, AUTOENCODER_DROPOUT_DLSTM_W, AUTOENCODER_DROPOUT_ELSTM_U, AUTOENCODER_DROPOUT_ELSTM_W, AUTOENCODER_SOFTMAX_L2_W, AUTOENCODER_SOFTMAX_L2_B, AUTOENCODER_OPTIMIZER, LOAD_WORD2IDX_PATH, LOAD_IDX2VEC_ARR_PATH, VOCAB_SIZE, MAXLEN_SENT, WORD2V_D, SENT2VEC_D
# import theano.sandbox.cuda
# theano.sandbox.cuda.use("gpu0")
# theano.config.device = 'gpu'
# theano.config.floatX = 'float32'

def load_data():
    with open(LOAD_WORD2IDX_PATH, "rb") as f:
        word2idx_dict = pickle.load(f)
    idx2vec_arr = np.load(LOAD_IDX2VEC_ARR_PATH)
    return word2idx_dict, idx2vec_arr

def read_sentence(data_path):
    for line in open(data_path, 'r'):
        review = eval(line)['reviewText']
        for sentence in sent_tokenize(review):
            yield sentence

def transform_to_vec(sentence_batch, word2idx, MAXLEN_SENT, VOCAB_SIZE):
    words_batch = [ word_tokenize(sentence) for sentence in sentence_batch ]
    for idx, words in enumerate(words_batch):
        words_batch[idx] = [word if word in word2idx else UNK for word in words]
    train_batch_idx = [[word2idx[word] for word in words] for words in words_batch]
    train_batch_idx_padding = sequence.pad_sequences(train_batch_idx, MAXLEN_SENT)
    train_batch_one_hot = np.asanyarray([np_utils.to_categorical(words, VOCAB_SIZE + 2) for words in train_batch_idx_padding], dtype=np.uint16)
    return train_batch_idx_padding, train_batch_one_hot

def read_batch(DATA_PATH, word2idx, MAXLEN_SENT, VOCAB_SIZE, BATCH_SIZE):
    sent_gtor = read_sentence(DATA_PATH)
    while True:
        sentence_batch = list(itertools.islice(sent_gtor, BATCH_SIZE))
        if not sentence_batch or len(sentence_batch) < BATCH_SIZE:
            break
        yield transform_to_vec(sentence_batch, word2idx, MAXLEN_SENT, VOCAB_SIZE)

def read_all(DATA_PATH, word2idx, MAXLEN_SENT, VOCAB_SIZE):
    sent_gtor = read_sentence(DATA_PATH)
    sentence_all = list(itertools.islice(sent_gtor, 0, None))
    return transform_to_vec(sentence_all, word2idx, MAXLEN_SENT, VOCAB_SIZE)

def build_autoencoder(CHECKPOINTS_WEIGHTS_PATH, idx2vec_arr, MAXLEN_SENT, WORD2V_D, VOCAB_SIZE, SENT2VEC_D, DROPOUT_ELSTM_W, DROPOUT_ELSTM_U, DROPOUT_DLSTM_W, DROPOUT_DLSTM_U, SOFTMAX_L2_W, SOFTMAX_L2_B):
    Input_T = Input(shape=(MAXLEN_SENT,), dtype='int32', name="Input_T")
    Embedding_L = Embedding(output_dim=WORD2V_D, input_dim=VOCAB_SIZE + 2, input_length=MAXLEN_SENT, mask_zero=True, weights=[idx2vec_arr], name="Embedding_L")
    Embedding_T = Embedding_L(Input_T)
    LSTM_Encoder_L = LSTM(SENT2VEC_D, return_sequences=False, name="LSTM_Encoder_L", activation="tanh", inner_activation="hard_sigmoid", dropout_W=DROPOUT_ELSTM_W, dropout_U=DROPOUT_ELSTM_U)
    LSTM_Encoder_T = LSTM_Encoder_L(Embedding_T)
    Repeat_L = RepeatVector(MAXLEN_SENT, name="Repeat_L")
    Mirrored_Encoder_Output_T = Repeat_L(LSTM_Encoder_T)
    LSTM_Decoder_L = LSTM(WORD2V_D, return_sequences=True, name="LSTM_Decoder_L", activation="tanh", inner_activation="hard_sigmoid", dropout_W=DROPOUT_DLSTM_W, dropout_U=DROPOUT_DLSTM_U)
    Decoder_Output_T = LSTM_Decoder_L(Mirrored_Encoder_Output_T)
    Softmax_L = Dense(output_dim=VOCAB_SIZE + 2, init='glorot_uniform', activation='softmax', W_regularizer=l2(SOFTMAX_L2_W), b_regularizer=l2(SOFTMAX_L2_B), name="Softmax_L")
    Softmax_TD_L = TimeDistributed(Softmax_L, batch_input_shape=(None, MAXLEN_SENT, WORD2V_D), name="Softmax_TD_L")
    Softmax_TD_Output_T = Softmax_TD_L(Decoder_Output_T)
    autoencoder = Model(input=Input_T, output=Softmax_TD_Output_T)
    return autoencoder

def run_autoencoder(autoencoder, train_batch_gtor, validation_set, OPTIMIZER, SAVE_MODEL_ARCH_PATH, SAVE_MODEL_WEIGHTS_PATH, CHECKPOINTS_WEIGHTS_PATH, EPOCH_SIZE, N_EPOCHS):
    print 'Compiling autoencoder...'
    autoencoder.compile(loss='categorical_crossentropy',
              optimizer=OPTIMIZER,
              metrics=['accuracy'])
    print 'Done'
    print 'Saving model...'
    save_model(autoencoder, SAVE_MODEL_ARCH_PATH)
    print 'Done'
    early_stopping = EarlyStopping(monitor='val_acc', patience=2)
    checkpoints = ModelCheckpoint(CHECKPOINTS_WEIGHTS_PATH, monitor='val_acc', verbose=0, save_best_only=True, mode='auto')
    callbacks = [checkpoints]
    print 'Training...'
    hist = autoencoder.fit_generator(train_batch_gtor, EPOCH_SIZE, N_EPOCHS, verbose=1, callbacks=callbacks, validation_data=validation_set, nb_val_samples=None, class_weight={})
    print 'Done'
    print 'Saving model weights...'
    save_model_weights(autoencoder, SAVE_MODEL_WEIGHTS_PATH)
    print 'Done'
    print 'Autoencoder training history:'
    print hist.history
    print 'Autoencoder summary:'
    autoencoder.summary()

def main():
    print 'Loading word2idx dictionary and idx2vec array...'
    word2idx_dict, idx2vec_arr = load_data()
    print 'Done'
    # print len(word2idx_dict)
    # print idx2vec_arr.shape
    # print 'Train set:'
    print 'Reading and transforming training data...'
    train_batch = read_batch(AUTOENCODER_TRAINING_DATA_PATH, word2idx_dict, MAXLEN_SENT, VOCAB_SIZE, AUTOENCODER_BATCH_SIZE)
    print 'Done'
    # train_batch_padding, train_batch_one_hot = next(train_batch)
    # print train_batch_padding
    # print train_batch_padding.shape
    # print train_batch_one_hot
    # print train_batch_one_hot.shape
    # print 'Validation set:'
    print 'Reading and transforming validation data...'
    val_padding, val_one_hot = read_all(AUTOENCODER_VALIDATION_DATA_PATH, word2idx_dict, MAXLEN_SENT, VOCAB_SIZE)
    print 'Done'
    # print val_padding
    # print val_padding.shape
    # print val_one_hot
    # print val_one_hot.shape
    # print val_padding[0]
    # print val_one_hot[0]
    print 'Build LSTM autoencoder'
    autoencoder = build_autoencoder(AUTOENCODER_CHECKPOINTS_WEIGHTS_PATH, idx2vec_arr, MAXLEN_SENT, WORD2V_D, VOCAB_SIZE, SENT2VEC_D, AUTOENCODER_DROPOUT_ELSTM_W, AUTOENCODER_DROPOUT_ELSTM_U, AUTOENCODER_DROPOUT_DLSTM_W, AUTOENCODER_DROPOUT_DLSTM_U, AUTOENCODER_SOFTMAX_L2_W, AUTOENCODER_SOFTMAX_L2_B)
    print 'Done'
    print 'Run LSTM autoencoder'
    run_autoencoder(autoencoder, train_batch, (val_padding, val_one_hot), AUTOENCODER_OPTIMIZER, AUTOENCODER_SAVE_MODEL_ARCH_PATH, AUTOENCODER_SAVE_MODEL_WEIGHTS_PATH, AUTOENCODER_CHECKPOINTS_WEIGHTS_PATH, AUTOENCODER_EPOCH_SIZE, AUTOENCODER_N_EPOCHS)
    print 'Done'

if __name__ == '__main__':
    main()