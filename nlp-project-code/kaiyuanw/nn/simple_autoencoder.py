__author__ = 'Kaiyuan_Wang'

import os
import itertools
import pickle
import numpy as np
from nltk import sent_tokenize, word_tokenize
from keras.preprocessing import sequence
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, RepeatVector
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from util.parameters import VOCAB_SIZE
from util.util import save_model, save_model_weights
import sys
sys.setrecursionlimit(40000)

PROJECT_DIR = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
TRAIN_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books100_Electronics0_100000.txt'
# TEST_SOURCE_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Books/test.txt'
# TEST_TARGET_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Electronics/test.txt'
VALIDATION_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books100_Electronics0_1000_val.txt'
LOAD_WORD2IDX_PATH = PROJECT_DIR + '/gen/PosNeg/BE/word2idx.pickle'
LOAD_IDX2VEC_PATH = PROJECT_DIR + '/gen/PosNeg/BE/idx2vec.pickle'
CHECKPOINTS_WEIGHTS_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_1.weights.best.hdf5'
SAVE_MODEL_ARCH_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_1.model.json'
SAVE_MODEL_WEIGHTS_PATH = PROJECT_DIR + "/gen/PosNeg/BE/BE_simple_autoencoder_1.weights.hdf5"

UNK = "UNK"
BATCH_SIZE = 32
MAXLEN = 80
WORD2V_D = 300
EPOCH_SIZE = 500
N_EPOCHS = 8
REPRESENT_VEC_SIZE = 256

def load_data(LOAD_WORD2IDX_PATH, LOAD_IDX2VEC_PATH):
    with open(LOAD_WORD2IDX_PATH, "rb") as f:
        word2idx_dict = pickle.load(f)
    with open(LOAD_IDX2VEC_PATH, "rb") as f:
        idx2vec_dict = pickle.load(f)
    return word2idx_dict, idx2vec_dict

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)

def transform_to_vec(review_batch, word2idx, idx2vec_dict):
    sentences_batch = [ sent_tokenize(review['reviewText'].lower()) for review in review_batch ]
    words_batch = [[word_tokenize(sentence) for sentence in sentences] for sentences in sentences_batch]
    reviews = [ [ word for sentence in review for word in sentence ] for review in words_batch ]
    for r, review in enumerate(reviews):
        reviews[r] = [ word if word in word2idx else UNK for word in review ]
    reviews = [ [ word2idx[word] for word in review ] for review in reviews ]
    X_train = np.asanyarray(reviews)
    y_train = np.asanyarray([0 if float(review["overall"]) < 3 else 1 for review in review_batch], dtype=np.uint16)
    X_train = sequence.pad_sequences(X_train, maxlen=MAXLEN)
    X_train = np.asanyarray([[idx2vec_dict[idx] for idx in train] for train in X_train])
    return X_train, y_train

def read_batch(DATA_PATH, word2idx, idx2vec_dict, BATCH_SIZE):
    review_gtor = read_review(DATA_PATH)
    while True:
        review_batch = list(itertools.islice(review_gtor, BATCH_SIZE))
        if not review_batch or len(review_batch) < BATCH_SIZE:
            break
        yield transform_to_vec(review_batch, word2idx, idx2vec_dict)

def read_all(DATA_PATH, word2idx, idx2vec_dict):
    review_gtor = read_review(DATA_PATH)
    review_batch = list(itertools.islice(review_gtor, None))
    return transform_to_vec(review_batch, word2idx, idx2vec_dict)

def main():
    word2idx, idx2vec_dict = load_data(LOAD_WORD2IDX_PATH, LOAD_IDX2VEC_PATH)
    # train_gtor = read_batch(TRAIN_DATA_PATH, word2idx, BATCH_SIZE)
    X_train, y_train = read_all(TRAIN_DATA_PATH, word2idx, idx2vec_dict)
    X_val, y_val = read_all(VALIDATION_DATA_PATH, word2idx, idx2vec_dict)
    print X_train.shape
    print X_val.shape
    # train = next(train_gtor)
    # val = next(val_gtor)
    model = Sequential()
    model.add(LSTM(REPRESENT_VEC_SIZE, input_dim=WORD2V_D, dropout_W=0.2, dropout_U=0.2, return_sequences=True))
    model.add(LSTM(WORD2V_D, dropout_W=0.2, dropout_U=0.2, return_sequences=True))
    # model.add(LSTM(REPRESENT_VEC_SIZE, input_dim=WORD2V_D, return_sequences=False, name='Encoder_L'))
    # model.add(RepeatVector(MAXLEN, name='Repeat_L'))
    # model.add(LSTM(WORD2V_D, return_sequences=True, name='Decoder_L'))
    # model.add(Activation('linear'))

    model.compile(loss='mean_squared_error',
              optimizer='adam')

    save_model(model, SAVE_MODEL_ARCH_PATH)

    print('Train...')
    early_stopping = EarlyStopping(monitor='loss', patience=3)
    checkpoints = ModelCheckpoint(CHECKPOINTS_WEIGHTS_PATH, monitor='loss', verbose=0, save_best_only=True, mode='auto')
    # model.fit(X_train, X_train, batch_size=BATCH_SIZE, nb_epoch=15, validation_data=(X_val, X_val), callbacks=[early_stopping, checkpoints])
    model.fit(X_train, X_train, nb_epoch=N_EPOCHS, callbacks=[early_stopping, checkpoints])
    save_model_weights(model, SAVE_MODEL_WEIGHTS_PATH)
    # score, acc = model.evaluate(X_val, X_val, batch_size=BATCH_SIZE)
    # print('Test score:', score)
    # print('Test accuracy:', acc)

if __name__ == '__main__':
    main()