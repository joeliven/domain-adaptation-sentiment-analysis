__author__ = 'Kaiyuan_Wang'

import os
import itertools
import numpy as np
from nltk import sent_tokenize, word_tokenize
from keras.preprocessing import sequence
from util.util import load_model
from simple_classifier import load_data, load_autoencoders

PROJECT_DIR = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
SIMPLE_LSTM_CLASSIFIER_ARCH_PATH = PROJECT_DIR + "/gen/PosNeg/EM/ME_simple_classifier_iden.model.json"
SIMPLE_LSTM_CLASSIFIER_WEIGHTS_PATH = PROJECT_DIR + "/gen/PosNeg/EM/ME_simple_classifier_iden.weights.best.hdf5"
LOAD_WORD2IDX_PATH = PROJECT_DIR + '/gen/PosNeg/EM/word2idx.pickle'
LOAD_IDX2VEC_PATH = PROJECT_DIR + '/gen/PosNeg/EM/idx2vec.pickle'
# TRAIN_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books0_Movies_and_TV100_100000.txt'
TRAIN_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Movies_and_TV/test.txt'
TEST_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Electronics/test.txt'

AUTOENCODER1_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/EM/simple_autoencoder_iden_5.model.json'
AUTOENCODER1_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/EM/simple_autoencoder_iden_5.weights.best.hdf5'
# AUTOENCODER2_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_2.model.json'
# AUTOENCODER2_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_2.weights.best.hdf5'
AUTOENCODER3_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/EM/simple_autoencoder_iden_3.model.json'
AUTOENCODER3_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/EM/simple_autoencoder_iden_3.weights.best.hdf5'
# AUTOENCODER4_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_4.model.json'
# AUTOENCODER4_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_4.weights.best.hdf5'
AUTOENCODER5_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/EM/simple_autoencoder_iden_1.model.json'
AUTOENCODER5_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/EM/simple_autoencoder_iden_1.weights.best.hdf5'
AUTOENCODER_PATHS = {
    'autoencoder1': (AUTOENCODER1_MODEL_PATH, AUTOENCODER1_WIGHT_PATH),
    # 'autoencoder2': (AUTOENCODER2_MODEL_PATH, AUTOENCODER2_WIGHT_PATH),
    'autoencoder3': (AUTOENCODER3_MODEL_PATH, AUTOENCODER3_WIGHT_PATH),
    # 'autoencoder4': (AUTOENCODER4_MODEL_PATH, AUTOENCODER4_WIGHT_PATH),
    'autoencoder5': (AUTOENCODER5_MODEL_PATH, AUTOENCODER5_WIGHT_PATH)
}

UNK = "UNK"
MAXLEN = 100
REPRESENT_VEC_SIZE = 256

def feed_autoencoders(data, autoencoders):
    concatenated_data = np.zeros((data.shape[0], data.shape[1], REPRESENT_VEC_SIZE * len(autoencoders)))
    # concatenated_data = np.zeros((data.shape[0], data.shape[1], REPRESENT_VEC_SIZE))
    # concatenated_data = np.zeros((data.shape[0], REPRESENT_VEC_SIZE * len(autoencoders)))
    for r, review in enumerate(data):
        for e, encoder in enumerate(autoencoders):
            encoded_rev = encoder([[review], 0])[0]
            # print encoded_rev.shape
            # print encoded_rev
            # print concatenated_data.shape
            # print concatenated_data
            # sys.exit()
            concatenated_data[r, :, e * REPRESENT_VEC_SIZE : (e + 1) * REPRESENT_VEC_SIZE] = encoded_rev
            # concatenated_data[r, :, :] += encoded_rev[0]
        # concatenated_data[r, :, :] = concatenated_data[r, :, :] / len(autoencoders)
        # print concatenated_data[r,:,:]
    # print concatenated_data.shape
    # sys.exit()
    return concatenated_data

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)

def transform_to_vec(review_batch, word2idx, idx2vec_dict, autoencoders):
    sentences_batch = [ sent_tokenize(review['reviewText'].lower()) for review in review_batch ]
    words_batch = [[word_tokenize(sentence) for sentence in sentences] for sentences in sentences_batch]
    reviews = [ [ word for sentence in review for word in sentence ] for review in words_batch ]
    for r, review in enumerate(reviews):
        reviews[r] = [ word if word in word2idx else UNK for word in review ]
    reviews = [ [ word2idx[word] for word in review ] for review in reviews ]
    X_train = np.asanyarray(reviews)
    y_train = np.asanyarray([0 if float(review["overall"]) < 3 else 1 for review in review_batch], dtype=np.uint16)
    X_train = sequence.pad_sequences(X_train, maxlen=MAXLEN)
    X_train = np.asanyarray([[idx2vec_dict[idx] for idx in train] for train in X_train])
    X_train = feed_autoencoders(X_train, autoencoders)
    return X_train, y_train

# def read_batch(DATA_PATH, word2idx, idx2vec_dict, autoencoders, BATCH_SIZE):
#     review_gtor = read_review(DATA_PATH)
#     while True:
#         review_batch = list(itertools.islice(review_gtor, BATCH_SIZE))
#         if not review_batch or len(review_batch) < BATCH_SIZE:
#             review_gtor = read_review(DATA_PATH)
#             continue
#         yield transform_to_vec(review_batch, word2idx, idx2vec_dict, autoencoders)

def read_all(DATA_PATH, word2idx, idx2vec_dict, autoencoders):
    review_gtor = read_review(DATA_PATH)
    review_batch = list(itertools.islice(review_gtor, None))
    return transform_to_vec(review_batch, word2idx, idx2vec_dict, autoencoders)

def run_simple_lstm_classifier():
    print 'Loading word2idx dictionary and idx2vec array...'
    word2idx_dict, idx2vec_dict = load_data(LOAD_WORD2IDX_PATH, LOAD_IDX2VEC_PATH)
    print 'Done'
    print 'Loading autoencoders...'
    autoencoders = load_autoencoders(**AUTOENCODER_PATHS)
    print 'Done'
    simple_lstm_classifier = load_model(SIMPLE_LSTM_CLASSIFIER_ARCH_PATH, SIMPLE_LSTM_CLASSIFIER_WEIGHTS_PATH)
    simple_lstm_classifier.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

    print 'Reading and transforming train data...'
    X_test, y_test = read_all(TRAIN_DATA_PATH, word2idx_dict, idx2vec_dict, autoencoders)
    print 'Done'
    score, acc = simple_lstm_classifier.evaluate(X_test, y_test)
    print('Train S, Test S, score:', score)
    print('Train S, Test S, accuracy:', acc)

    print 'Reading and transforming test data...'
    X_test, y_test = read_all(TEST_DATA_PATH, word2idx_dict, idx2vec_dict, autoencoders)
    print 'Done'
    score, acc = simple_lstm_classifier.evaluate(X_test, y_test)
    print('Train S, Test T, score:', score)
    print('Train S, Test T, accuracy:', acc)

def main():
    run_simple_lstm_classifier()

if __name__ == '__main__':
    main()