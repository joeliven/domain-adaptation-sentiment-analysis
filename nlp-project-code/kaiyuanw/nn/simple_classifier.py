__author__ = 'Kaiyuan_Wang'

import os
import sys
from keras.models import model_from_json
from keras import backend as K
import itertools
import numpy as np
from nltk import sent_tokenize, word_tokenize
from keras.preprocessing import sequence
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from simple_autoencoder import load_data
from util.util import save_model, save_model_weights

PROJECT_DIR = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
LOAD_WORD2IDX_PATH = PROJECT_DIR + '/gen/PosNeg/BE/word2idx.pickle'
LOAD_IDX2VEC_PATH = PROJECT_DIR + '/gen/PosNeg/BE/idx2vec.pickle'
TRAINING_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books100_Electronics0_100000.txt'
TEST_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Electronics/test.txt'
VALIDATION_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books100_Electronics0_1000_val.txt'
AUTOENCODER1_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_1.model.json'
AUTOENCODER1_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_1.weights.best.hdf5'
# AUTOENCODER2_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_2.model.json'
# AUTOENCODER2_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_2.weights.best.hdf5'
AUTOENCODER3_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_3.model.json'
AUTOENCODER3_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_3.weights.best.hdf5'
# AUTOENCODER4_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_4.model.json'
# AUTOENCODER4_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_4.weights.best.hdf5'
AUTOENCODER5_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_5.model.json'
AUTOENCODER5_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_iden_5.weights.best.hdf5'
# AUTOENCODER1_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_1.model.json'
# AUTOENCODER1_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_1.weights.best.hdf5'
# AUTOENCODER2_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_2.model.json'
# AUTOENCODER2_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_2.weights.best.hdf5'
# AUTOENCODER3_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_3.model.json'
# AUTOENCODER3_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_3.weights.best.hdf5'
# AUTOENCODER4_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_4.model.json'
# AUTOENCODER4_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_4.weights.best.hdf5'
# AUTOENCODER5_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_5.model.json'
# AUTOENCODER5_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_autoencoder_compress_5.weights.best.hdf5'
AUTOENCODER_PATHS = {
    'autoencoder1': (AUTOENCODER1_MODEL_PATH, AUTOENCODER1_WIGHT_PATH),
    # 'autoencoder2': (AUTOENCODER2_MODEL_PATH, AUTOENCODER2_WIGHT_PATH),
    'autoencoder3': (AUTOENCODER3_MODEL_PATH, AUTOENCODER3_WIGHT_PATH),
    # 'autoencoder4': (AUTOENCODER4_MODEL_PATH, AUTOENCODER4_WIGHT_PATH),
    'autoencoder5': (AUTOENCODER5_MODEL_PATH, AUTOENCODER5_WIGHT_PATH)
}
CHECKPOINTS_WEIGHTS_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_simple_classifier_iden.weights.best.hdf5'
SAVE_MODEL_ARCH_PATH = PROJECT_DIR + "/gen/PosNeg/BE/BE_simple_classifier_iden.model.json"
SAVE_MODEL_WEIGHTS_PATH = PROJECT_DIR + "/gen/PosNeg/BE/BE_simple_classifier_iden.weights.hdf5"
MAXLEN = 100
WORD2V_D = 300
BATCH_SIZE = 64
REPRESENT_VEC_SIZE = 256
EPOCH_SIZE = 1024
N_EPOCHS = 50
UNK = "UNK"

def load_model(MODEL_PATH, WEIGHTS_PATH):
    with open(MODEL_PATH, "r") as f:
        model = model_from_json(f.read())
    model.load_weights(WEIGHTS_PATH)
    return model

def load_autoencoders(**kwargs):
    autoencoders = list()
    sorted_keys = sorted(kwargs.keys())
    for key in sorted_keys:
        model_path, weights_path = kwargs[key]
        model = load_model(model_path, weights_path)
        encode_func = K.function([model.layers[0].input, K.learning_phase()], [model.layers[0].output])
        autoencoders.append(encode_func)
    return autoencoders

def feed_autoencoders(data, autoencoders):
    concatenated_data = np.zeros((data.shape[0], data.shape[1], REPRESENT_VEC_SIZE * len(autoencoders)))
    # concatenated_data = np.zeros((data.shape[0], data.shape[1], REPRESENT_VEC_SIZE))
    # concatenated_data = np.zeros((data.shape[0], REPRESENT_VEC_SIZE * len(autoencoders)))
    for r, review in enumerate(data):
        for e, encoder in enumerate(autoencoders):
            encoded_rev = encoder([[review], 0])[0]
            # print encoded_rev.shape
            # print encoded_rev
            # print concatenated_data.shape
            # print concatenated_data
            # sys.exit()
            concatenated_data[r, :, e * REPRESENT_VEC_SIZE : (e + 1) * REPRESENT_VEC_SIZE] = encoded_rev
            # concatenated_data[r, :, :] += encoded_rev[0]
        # concatenated_data[r, :, :] = concatenated_data[r, :, :] / len(autoencoders)
        # print concatenated_data[r,:,:]
    # print concatenated_data.shape
    # sys.exit()
    return concatenated_data

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)

def transform_to_vec(review_batch, word2idx, idx2vec_dict, autoencoders):
    sentences_batch = [ sent_tokenize(review['reviewText'].lower()) for review in review_batch ]
    words_batch = [[word_tokenize(sentence) for sentence in sentences] for sentences in sentences_batch]
    reviews = [ [ word for sentence in review for word in sentence ] for review in words_batch ]
    for r, review in enumerate(reviews):
        reviews[r] = [ word if word in word2idx else UNK for word in review ]
    reviews = [ [ word2idx[word] for word in review ] for review in reviews ]
    X_train = np.asanyarray(reviews)
    y_train = np.asanyarray([0 if float(review["overall"]) < 3 else 1 for review in review_batch], dtype=np.uint16)
    X_train = sequence.pad_sequences(X_train, maxlen=MAXLEN)
    X_train = np.asanyarray([[idx2vec_dict[idx] for idx in train] for train in X_train])
    X_train = feed_autoencoders(X_train, autoencoders)
    return X_train, y_train

def read_batch(DATA_PATH, word2idx, idx2vec_dict, autoencoders, BATCH_SIZE):
    review_gtor = read_review(DATA_PATH)
    while True:
        review_batch = list(itertools.islice(review_gtor, BATCH_SIZE))
        if not review_batch or len(review_batch) < BATCH_SIZE:
            review_gtor = read_review(DATA_PATH)
            continue
        yield transform_to_vec(review_batch, word2idx, idx2vec_dict, autoencoders)

def read_all(DATA_PATH, word2idx, idx2vec_dict, autoencoders):
    review_gtor = read_review(DATA_PATH)
    review_batch = list(itertools.islice(review_gtor, None))
    return transform_to_vec(review_batch, word2idx, idx2vec_dict, autoencoders)

def main():
    print 'Loading word2idx dictionary and idx2vec array...'
    word2idx_dict, idx2vec_dict = load_data(LOAD_WORD2IDX_PATH, LOAD_IDX2VEC_PATH)
    print 'Done'
    print 'Loading autoencoders...'
    autoencoders = load_autoencoders(**AUTOENCODER_PATHS)
    print 'Done'

    print 'Reading and transforming training data...'
    train_batch = read_batch(TRAINING_DATA_PATH, word2idx_dict, idx2vec_dict, autoencoders, BATCH_SIZE)
    # print X_train.shape
    # X_train = feed_autoencoders(X_train, autoencoders)
    # print X_train.shape
    # train_batch = read_batch(autoencoders, CLASSIFIER_TRAINING_DATA_PATH, word2idx_dict, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, CLASSIFIER_BATCH_SIZE, SENT2VEC_D)
    print next(train_batch)[0].shape
    print 'Done'
    print 'Reading and transforming validation data...'
    X_val, y_val = read_all(VALIDATION_DATA_PATH, word2idx_dict, idx2vec_dict, autoencoders)
    # X_val = feed_autoencoders(X_val, autoencoders)
    # print val_padding.shape
    # print val_one_hot.shape
    # print len([x for x in val_one_hot if x == 0])
    # print next(val_batch)[0].shape
    print 'Done'

    '''LSTM classifier'''
    model = Sequential()
    model.add(LSTM(300, input_shape=(MAXLEN, REPRESENT_VEC_SIZE * len(AUTOENCODER_PATHS)), dropout_W=0.2, dropout_U=0.2))
    # model.add(LSTM(128, input_shape=(MAXLEN, REPRESENT_VEC_SIZE), dropout_W=0.2, dropout_U=0.2))
    model.add(Dropout(0.2))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

    save_model(model, SAVE_MODEL_ARCH_PATH)

    print('Training simple classifier...')
    checkpoints = ModelCheckpoint(CHECKPOINTS_WEIGHTS_PATH, monitor='val_acc', verbose=0, save_best_only=True, mode='auto')
    model.fit_generator(train_batch, EPOCH_SIZE, N_EPOCHS, validation_data=(X_val, y_val), callbacks=[checkpoints])
    save_model_weights(model, SAVE_MODEL_WEIGHTS_PATH)
    print 'Done'
    print 'Reading and transforming test data...'
    X_test, y_test = read_all(TEST_DATA_PATH, word2idx_dict, idx2vec_dict, autoencoders)
    print 'Done'
    score, acc = model.evaluate(X_test, y_test, batch_size=BATCH_SIZE)
    print('Test score:', score)
    print('Test accuracy:', acc)

    '''Traditional classifier with review_representation as a single vector'''
    # model = Sequential()
    # # X_train.shape = (100000, 80, 300)
    # model.add(Dense(output_dim=REPRESENT_VEC_SIZE, input_dim=REPRESENT_VEC_SIZE * len(AUTOENCODER_PATHS), init='uniform', activation='relu'))
    # model.add(Dropout(0.1))
    # model.add(Dense(REPRESENT_VEC_SIZE, activation='relu'))
    # model.add(Dropout(0.1))
    # model.add(Dense(1, activation='sigmoid'))
    #
    # model.compile(loss='binary_crossentropy',
    #           optimizer='adam', #'rmsprop'
    #           metrics=['accuracy'])
    #
    # save_model(model, SAVE_MODEL_ARCH_PATH)
    #
    # print('Training simple classifier...')
    # checkpoints = ModelCheckpoint(CHECKPOINTS_WEIGHTS_PATH, monitor='val_acc', verbose=0, save_best_only=True, mode='auto')
    # model.fit_generator(train_batch, EPOCH_SIZE, N_EPOCHS, validation_data=(X_val, y_val), callbacks=[checkpoints])
    # save_model_weights(model, SAVE_MODEL_WEIGHTS_PATH)
    # print 'Done'
    # print 'Reading and transforming test data...'
    # X_test, y_test = read_all(TEST_DATA_PATH, word2idx_dict, idx2vec_dict, autoencoders)
    # print 'Done'
    # score, acc = model.evaluate(X_test, y_test, batch_size=BATCH_SIZE)
    # print('Test score:', score)
    # print('Test accuracy:', acc)


if __name__ == '__main__':
    main()