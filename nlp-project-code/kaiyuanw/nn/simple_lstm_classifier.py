__author__ = 'Kaiyuan_Wang'

import os
import itertools
import pickle
import numpy as np
from nltk import sent_tokenize, word_tokenize
from keras.preprocessing import sequence
from keras.callbacks import ModelCheckpoint
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from util.parameters import VOCAB_SIZE
from util.util import save_model, save_model_weights

PROJECT_DIR = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
TRAIN_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books0_Electronics100_100000.txt'
# TEST_SOURCE_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Books/test.txt'
TEST_TARGET_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Books/test.txt'
VALIDATION_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books0_Electronics100_1000_val.txt'
LOAD_WORD2IDX_PATH = PROJECT_DIR + '/gen/PosNeg/BE/word2idx.pickle'
CHECKPOINTS_WEIGHTS_PATH = PROJECT_DIR + '/gen/PosNeg/BE/EB_simple_lstm_classifier_batch.weights.best.hdf5'
SAVE_MODEL_ARCH_PATH = PROJECT_DIR + '/gen/PosNeg/BE/EB_simple_lstm_classifier_batch.model.json'
SAVE_MODEL_WEIGHTS_PATH = PROJECT_DIR + "/gen/PosNeg/BE/EB_simple_lstm_classifier_batch.weights.hdf5"

UNK = "UNK"
BATCH_SIZE = 128
MAXLEN = 80
EPOCH_SIZE = 4096
N_EPOCHS = 250

def load_data(LOAD_WORD2IDX_PATH):
    with open(LOAD_WORD2IDX_PATH, "rb") as f:
        word2idx_dict = pickle.load(f)
    return word2idx_dict

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)

def transform_to_vec(review_batch, word2idx):
    sentences_batch = [ sent_tokenize(review['reviewText']) for review in review_batch ]
    words_batch = [[word_tokenize(sentence) for sentence in sentences] for sentences in sentences_batch]
    reviews = [ [ word for sentence in review for word in sentence ] for review in words_batch ]
    for r, review in enumerate(reviews):
        reviews[r] = [ word if word in word2idx else UNK for word in review ]
    reviews = [ [ word2idx[word] for word in review ] for review in reviews ]
    X_train = np.asanyarray(reviews)
    y_train = np.asanyarray([0 if float(review["overall"]) < 3 else 1 for review in review_batch], dtype=np.uint16)
    X_train = sequence.pad_sequences(X_train, maxlen=MAXLEN)
    return X_train, y_train

def read_batch(DATA_PATH, word2idx, BATCH_SIZE):
    review_gtor = read_review(DATA_PATH)
    while True:
        review_batch = list(itertools.islice(review_gtor, BATCH_SIZE))
        if not review_batch or len(review_batch) < BATCH_SIZE:
            review_gtor = read_review(DATA_PATH)
            continue
        yield transform_to_vec(review_batch, word2idx)

def read_all(DATA_PATH, word2idx):
    review_gtor = read_review(DATA_PATH)
    review_batch = list(itertools.islice(review_gtor, None))
    return transform_to_vec(review_batch, word2idx)

def main():
    word2idx = load_data(LOAD_WORD2IDX_PATH)
    X_train_gtor = read_batch(TRAIN_DATA_PATH, word2idx, BATCH_SIZE)
    # X_train, y_train = read_all(TRAIN_DATA_PATH, word2idx)
    X_val, y_val = read_all(VALIDATION_DATA_PATH, word2idx)
    X_test, y_test = read_all(TEST_TARGET_DATA_PATH, word2idx)
    # train = next(train_gtor)
    # val = next(val_gtor)
    model = Sequential()
    model.add(Embedding(output_dim=128, input_dim=VOCAB_SIZE + 2, input_length=MAXLEN, dropout=0.2))
    model.add(LSTM(128, dropout_W=0.2, dropout_U=0.2))
    model.add(Dropout(0.2))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

    save_model(model, SAVE_MODEL_ARCH_PATH)

    print('Train...')
    checkpoints = ModelCheckpoint(CHECKPOINTS_WEIGHTS_PATH, monitor='val_acc', verbose=0, save_best_only=True, mode='auto')
    model.fit_generator(X_train_gtor, EPOCH_SIZE, N_EPOCHS, validation_data=(X_val, y_val), callbacks=[checkpoints])
    # model.fit(X_train, y_train, batch_size=BATCH_SIZE, nb_epoch=N_EPOCHS, validation_data=(X_val, y_val), callbacks=[checkpoints])
    save_model_weights(model, SAVE_MODEL_WEIGHTS_PATH)
    score, acc = model.evaluate(X_test, y_test, batch_size=BATCH_SIZE)
    print('Test score:', score)
    print('Test accuracy:', acc)

if __name__ == '__main__':
    main()