__author__ = 'Kaiyuan_Wang'

import os
import itertools
import pickle
import numpy as np
from nltk import sent_tokenize, word_tokenize
from keras.preprocessing import sequence
from util.util import load_model

PROJECT_DIR = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
LOAD_WORD2IDX_PATH = PROJECT_DIR + '/gen/PosNeg/BE/word2idx.pickle'
LOAD_MODEL_ARCH_PATH = PROJECT_DIR + '/gen/PosNeg/BE/EB_simple_lstm_classifier_batch.model.json'
LOAD_MODEL_WEIGHTS_PATH = PROJECT_DIR + "/gen/PosNeg/BE/EB_simple_lstm_classifier_batch.weights.best.hdf5"
SOURCE_TEST_PATH = PROJECT_DIR + '/data/PosNeg/Electronics/test.txt'
TARGET_TEST_PATH = PROJECT_DIR + '/data/PosNeg/Books/test.txt'

UNK = "UNK"
BATCH_SIZE = 128
MAXLEN = 80
EPOCH_SIZE = 4096
N_EPOCHS = 500

def load_data(LOAD_WORD2IDX_PATH):
    with open(LOAD_WORD2IDX_PATH, "rb") as f:
        word2idx_dict = pickle.load(f)
    return word2idx_dict

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)

def transform_to_vec(review_batch, word2idx):
    sentences_batch = [ sent_tokenize(review['reviewText']) for review in review_batch ]
    words_batch = [[word_tokenize(sentence) for sentence in sentences] for sentences in sentences_batch]
    reviews = [ [ word for sentence in review for word in sentence ] for review in words_batch ]
    for r, review in enumerate(reviews):
        reviews[r] = [ word if word in word2idx else UNK for word in review ]
    reviews = [ [ word2idx[word] for word in review ] for review in reviews ]
    X_train = np.asanyarray(reviews)
    y_train = np.asanyarray([0 if float(review["overall"]) < 3 else 1 for review in review_batch], dtype=np.uint16)
    X_train = sequence.pad_sequences(X_train, maxlen=MAXLEN)
    return X_train, y_train

def read_batch(DATA_PATH, word2idx, BATCH_SIZE):
    review_gtor = read_review(DATA_PATH)
    while True:
        review_batch = list(itertools.islice(review_gtor, BATCH_SIZE))
        if not review_batch or len(review_batch) < BATCH_SIZE:
            review_gtor = read_review(DATA_PATH)
            continue
        yield transform_to_vec(review_batch, word2idx)

def read_all(DATA_PATH, word2idx):
    review_gtor = read_review(DATA_PATH)
    review_batch = list(itertools.islice(review_gtor, None))
    return transform_to_vec(review_batch, word2idx)

def main():
    print 'Loading word2idx dictionary...'
    word2idx = load_data(LOAD_WORD2IDX_PATH)
    print 'Done'
    simple_lstm_classifier = load_model(LOAD_MODEL_ARCH_PATH, LOAD_MODEL_WEIGHTS_PATH)
    simple_lstm_classifier.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

    print 'Reading and transforming source test data...'
    X_test, y_test = read_all(SOURCE_TEST_PATH, word2idx)
    print 'Done'
    score, acc = simple_lstm_classifier.evaluate(X_test, y_test)
    print('Train S, Test S, score:', score)
    print('Train S, Test S, accuracy:', acc)

    print 'Reading and transforming target test data...'
    X_test, y_test = read_all(TARGET_TEST_PATH, word2idx)
    print 'Done'
    score, acc = simple_lstm_classifier.evaluate(X_test, y_test)
    print('Train S, Test T, score:', score)
    print('Train S, Test T, accuracy:', acc)

if __name__ == '__main__':
    main()