#imports
# get_ipython().magic('load_ext autoreload')
# get_ipython().magic('autoreload 2')
import util.data_utils_nlp as du
import util.funcs as fn

from keras.models import Model
from keras.models import Sequential
from keras import backend as K
from keras.layers import Input, Dense, Embedding, RepeatVector, Dropout, Activation, Masking
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.preprocessing import sequence
from keras.utils import np_utils
from keras.optimizers import SGD
from keras.regularizers import l2, activity_l2
from keras.callbacks import Callback
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
import theano

import scipy as sci
import numpy as np

import os, sys
import pickle
import itertools
import argparse


def main():
    cmdln = prep_cmdln_parser()
    args = cmdln.parse_args()
    args_dict = vars(args)
    vprint(args, "START CLASSIFIER...")
    for key,value in args_dict.items():
        vprint(args, "%s:\t%s", (key,value))
    if args.GPU == True:
        # theano.config.device = 'gpu'
        # theano.config.floatX = 'float32'
        vprint(args, "Using GPU if available.")

    # LOAD VOCAB:
    idx2w, w2idx, w2v, vocab_list, idx2v = load_vocab(args)

    # LOAD AUTOENCODER MODELS:
    autoencoders = list() # a python list of our autoencoder models...autoencoders[0] = S, autoencoders[-1] = T
    for i in range(0, len(args.AUTOENCODER_ARCH_FILES)):
        arch_file = args.AUTOENCODER_ARCH_FILES[i]
        weights_file = args.AUTOENCODER_WEIGHTS_FILES[i]
        autoencoders.append(fn.load_model(arch_file, weights_file))

    # CONVERT AUTOENCODER MODELS INTO ENCODER FUNCTIONS:
    encoders = list()
    for a,autoencoder in enumerate(autoencoders):
        # autoencoder.summary()
        # print("autoencoder.get_layer('input_idxs_T')")
        # print(autoencoder.get_layer("input_idxs_T"))
        # print("type(autoencoder.get_layer('input_idxs_T').output)")
        # print(type(autoencoder.get_layer("input_idxs_T").output))
        # print("type(autoencoder.get_layer('input_idxs_T').input)")
        # print(type(autoencoder.get_layer("input_idxs_T").input))
        # print("autoencoder.get_layer('input_idxs_T').get_config()")
        # print(autoencoder.get_layer("input_idxs_T").get_config())

        # print("autoencoder.get_layer('lstm_encoder_L')")
        # print(autoencoder.get_layer("lstm_encoder_L"))
        # print("autoencoder.get_layer('lstm_encoder_L').get_config()")
        # print(autoencoder.get_layer("lstm_encoder_L").get_config())
        # print("autoencoder.get_layer('lstm_encoder_L').output")
        # print(autoencoder.get_layer("lstm_encoder_L").output)
        # print("type(autoencoder.get_layer('lstm_encoder_L').output)")
        # print(type(autoencoder.get_layer("lstm_encoder_L").output))
        # print("autoencoder.layers")
        # print(autoencoder.layers)
        # input("...")

        # encoder = K.function([autoencoder.get_layer("input_idxs_T").input, K.learning_phase()],
        #                           [autoencoder.get_layer("lstm_encoder_L").output])
        encoder = K.function([autoencoder.get_layer("input_idxs_T").input, K.learning_phase()],
                                  [autoencoder.get_layer("lstm_encoder_L").output])

        encoders.append(encoder)

    # # CONVERT AUTOENCODER MODELS INTO ENCODER FUNCTIONS: (ALTERNATIVE WAY)
    # encoders = list()
    # for a,autoencoder in enumerate(autoencoders):
    #     input_idxs_T = autoencoder.get_layer("input_idxs_T")
    #     lstm_L = autoencoder.get_layer("lstm_L")
    #     encoder = Model(input=input_idxs_T, output=lstm_L)
    #     encoders.append(encoder)

    # LOAD TRAINING DATA AS GENERATORS:
    print("loading training data generator...")
    XY_train_gtors = du.amazon_process_gtors_classifier(encoders, args.TRAINING_DATA_FILE, w2idx, idx2w, args.MAXLEN_SENT, args.MAXLEN_REV, args.VOCAB_SIZE, args.BATCH_SIZE, args.EPOCH_SIZE, args.SENT2VEC_D)

    print("loading validation data generator...")
    # LOAD VALIDATION DATA AS GENERATORS:
    XY_val_gtors = du.amazon_process_gtors_classifier(encoders, args.VAL_DATA_FILE, w2idx, idx2w, args.MAXLEN_SENT, args.MAXLEN_REV, args.VOCAB_SIZE, args.BATCH_SIZE, args.EPOCH_SIZE, args.SENT2VEC_D)


    # SETUP CALLBACK FUCNTIONS:
    early_stopping = EarlyStopping(monitor='val_acc', patience=2)
    checkpoints = ModelCheckpoint(args.CHECKPOINTS_PATH, monitor='val_acc', verbose=0, save_best_only=False, mode='auto')
    callbacks = list()
    if args.SAVE_CHECKPOINTS:
        callbacks.append(checkpoints)

    # LOAD OR BUILD MODEL:
    if args.LOAD_MODEL:
        # load the model from file:
        if args.LOAD_ARCH_FILE is not None and args.LOAD_WEIGHTS_FILE is not None:
            classifier = fn.load_model(args.LOAD_ARCH_FILE, args.LOAD_WEIGHTS_FILE)
        else:
            print("ERROR: either --load-model-arch or --load-model-weights was not specified in command line args. Exiting.")
            sys.exit(1)
    else:
        # build the model:
        classifier = build_model(args, idx2v)

    # COMPILE THE MODEL:
    classifier.compile(loss='binary_crossentropy', # 'categorical_crossentropy',
              optimizer=args.OPTIMIZER,
              metrics=['accuracy'])

    # SAVE MODEL ARCHITECTURE:
    if args.SAVE_MODEL:
        fn.save_model_arch(classifier, os.path.split(args.SAVE_ARCH_FILE)[0], args.SAVE_ARCH_FILE)

    # PRINT BATCH AND EPOCH INFO:
    vprint(args, "BATCH_SIZE: %d, N_EPOCHS %d" % (args.BATCH_SIZE, args.N_EPOCHS))

    # TRAIN THE MODEL:
    if args.TRAIN:
        hist = classifier.fit_generator(XY_train_gtors, args.EPOCH_SIZE, args.N_EPOCHS, verbose=1, callbacks=callbacks, validation_data=XY_val_gtors, nb_val_samples=None, class_weight={})

    # SAVE MODEL WEIGHTS:
    if args.SAVE_MODEL:
        fn.save_model_weights(classifier, os.path.split(args.SAVE_ARCH_FILE)[0], args.SAVE_WEIGHTS_FILE)

    if args.VERBOSE:
        print("Printing model (classifier) training history:")
        print(hist.history)
        print("Printing model (classifier) summary:")
        classifier.summary()
    vprint(args, "END CLASSIFIER.")


#############################################################
def split_val_data(args, XY_train_gtors):
    # SPLIT OFF VALIDATION DATA:
    (X_val, Y_val) = next(XY_train_gtors)
    val_num = 1
    while val_num < args.VAL_BATCHES:
        (X_val_,Y_val_) = next(XY_train_gtors)
        X_val = np.concatenate((X_val, X_val_), axis=0)
        Y_val = np.concatenate((Y_val, Y_val_), axis=0)
        val_num += 1
    # print("X_val.shape: " + str(X_val.shape))
    # print("Y_val.shape: " + str(Y_val.shape))
    # print("X_val[0]: ")
    # print(X_val[0])
    # print("Y_val[0]: ")
    # print(Y_val[0])
    return (X_val, Y_val)

def build_model(args, idx2v):
    """
        args:                       namespace object containing all the commandline arguments that were passed to the program
                                        e.g.    args.VERBOSE, args.MAXLEN, args.N_BATCHES, args.SENT2VEC_D, etc.
        idx2v:
            datatype:             numpy 2d array
            rows:                   word indexes in the vocabulary, as defined in word2idx_dict
            cols:                    pretrained word vectors (embeddings), as defined in word2v_dict
            purpose:              array mapping each word from its index to its pretrained embedding vector for use in the lstm autoencoder

        Note: In this function variables ending in "_T" are theano TENSORS (dressed up with Keras attributes),
                 while variables ending in "_L" are keras LAYERS
    """
    ########## ENCODER ############
    # INPUT (TENSOR):
    input_encoded_T = Input(shape=(args.MAXLEN_REV, args.NUM_ENCODERS*args.SENT2VEC_D), name="input_encoded_T")
    # might need to add: dtype='float64'

    print(input_encoded_T._keras_shape)
    assert input_encoded_T._keras_shape == (None, args.MAXLEN_REV, args.NUM_ENCODERS*args.SENT2VEC_D)
    print(input_encoded_T.dtype)

    # MASKING LAYER:
    masking_L = Masking(mask_value=0, input_shape=(args.MAXLEN_REV, args.NUM_ENCODERS*args.SENT2VEC_D), name="masking_L")

    # MASKING LAYER OUTPUT (TENSOR):
    input_encoded_masked_T = masking_L(input_encoded_T)

    print(input_encoded_masked_T._keras_shape)
    assert input_encoded_masked_T._keras_shape == (None, args.MAXLEN_REV, args.NUM_ENCODERS*args.SENT2VEC_D)
    print(input_encoded_masked_T.dtype)

    print(masking_L.input_shape) # == (N_BATCHES, MAXLEN_REV, args.NUM_ENCODERS*args.SENT2VEC_D)
    print(masking_L.output_shape) # == (N_BATCHES, MAXLEN, args.NUM_ENCODERS*args.SENT2VEC_D)
    assert masking_L.input_shape == (None, args.MAXLEN_REV, args.NUM_ENCODERS*args.SENT2VEC_D)
    assert masking_L.output_shape == (None, args.MAXLEN_REV, args.NUM_ENCODERS*args.SENT2VEC_D)
    assert masking_L.output == input_encoded_masked_T

    # LSTM CLASSIFIER LAYER:
    lstm_classifier_L = LSTM(args.REV2VEC_D, return_sequences=False, name="lstm_classifier_L", activation="tanh", inner_activation="hard_sigmoid",dropout_W=args.DROPOUT_LSTM_W, dropout_U=args.DROPOUT_LSTM_U)

    # LSTM CLASSIFIER LAYER OUTPUT (TENSOR):
    review_encoded_T = lstm_classifier_L(input_encoded_masked_T)

    print(lstm_classifier_L.output) # == review_encoded_T
    print(lstm_classifier_L.output_shape) # == (None, REV2VEC_D)
    print(review_encoded_T._keras_shape) # == (None, REV2VEC_D)
    assert lstm_classifier_L.output == review_encoded_T
    assert lstm_classifier_L.output_shape == (None, args.REV2VEC_D)
    assert review_encoded_T._keras_shape == (None, args.REV2VEC_D)

    ########## CLASSIFICATION ############
    # DENSE LAYER:
    dense_L = Dense(output_dim=1, init='glorot_uniform', activation='sigmoid', W_regularizer=l2(args.DENSE_L2_W), b_regularizer=l2(args.DENSE_L2_B), name="dense_L")

    # DENSE LAYER OUTPUT (TENSOR):
    predicted_sentiment_T = dense_L(review_encoded_T)

    print(dense_L.output)# == predicted_sentiment_T
    print(dense_L.output_shape)# == (None, 1)
    print(predicted_sentiment_T._keras_shape)# == (None, 1)
    assert dense_L.output == predicted_sentiment_T
    assert dense_L.output_shape == (None, 1)
    assert predicted_sentiment_T._keras_shape == (None, 1)

    # INSTANTIATE THE MODEL:
    classifier = Model(input=input_encoded_T, output=predicted_sentiment_T)
    return classifier

def load_vocab(args):
    vprint(args, "Loading necessary vocab related data structures from disk...")
    vprint(args, "\tloading: idx2w (python list)...")
    with open(args.IDX2W_FILE, "rb") as f:
        idx2w = pickle.load(f)
    vprint(args, "\tloading: w2idx (python dict)...")
    with open(args.W2IDX_FILE, "rb") as f:
        w2idx = pickle.load(f)
    vprint(args, "\tloading: w2v (python dict)...")
    with open(args.W2V_FILE, "rb") as f:
        w2v = pickle.load(f)
    vprint(args, "\tloading: vocab_list (python list)...")
    with open(args.VOCAB_FILE, "rb") as f:
        vocab_list = pickle.load(f)
    vprint(args, "\tloading: idx2v (numpy array)...")
    idx2v = np.load(args.IDX2V_FILE)
    idx2v = idx2v[0:args.VOCAB_SIZE+1,:]
    vprint(args, "\tidx2v.shape is:")
    vprint(args, idx2v.shape)
    vprint(args, "DONE loading necessary vocab related data structures from disk...")
    assert idx2v.shape == (args.VOCAB_SIZE+1, args.WORD2VEC_D)
    return idx2w, w2idx, w2v, vocab_list, idx2v

def vprint(args, string, fillers=None):
    """
        args: cmdln args object
        str: string to print
    """
    if args.VERBOSE == True:
        if fillers is not None:
            print(string % fillers)
        else:
            print(string)

def prep_cmdln_parser():
    usage = "usage: %prog [options]"
    cmdln = argparse.ArgumentParser(usage)
    cmdln.add_argument("--verbose", action="store_true", dest="VERBOSE", default=False,
                                help="print out more information during runtime [default: %default.")
    cmdln.add_argument("--gpu", action="store_true", dest="GPU", default=False,
                                help="train the model on a GPU (if supported) [default: %default].")
    cmdln.add_argument("--train", action="store_true", dest="TRAIN", default=False,
                                help="train the model [default: %default]. Note, if this set then --training-data=FILE must also be be specified.")
    cmdln.add_argument("--training-data", action="store", dest="TRAINING_DATA_FILE", default="",
                                help="load the training data from specified file [default: %default]. Note, this must be specified in order to train the model.")
    cmdln.add_argument("--val-data", action="store", dest="VAL_DATA_FILE", default="",
                                help="load the validation data from specified file [default: %default]. Note, this must be specified in order to train the model.")
    cmdln.add_argument("--test", action="store_true", dest="TEST", default=False,
                                help="test the model [default: %default]. Note, if this set then --test-data=FILE must also be be specified.")
    cmdln.add_argument("--test-data", action="store", dest="TEST_DATA_FILE", default="",
                                help="load the training data from specified file [default: %default]. Note, this must be specified in order to test the model.")
    cmdln.add_argument("--autoencoder-arch", action="append", dest="AUTOENCODER_ARCH_FILES", default=[], required=True,
                                help="load the architecture for one autoencoder model from specified file. Use this option multiple times to load the architecture for multiple autoencoders. Note, that at least one autoencoder is needed for the model to work at all, and at least three autoencoders should be specified for the model to function as intended. Also note that for each autoencoder, both an architecture file (.json) and a weights file (.hdf5) must be specified. Use --autoencoder-weights FILE to specify the weights file for each autoencoder [default: %default].")
    cmdln.add_argument("--autoencoder-weights", action="append", dest="AUTOENCODER_WEIGHTS_FILES", default=[], required=True,
                                help="load the weights for one autoencoder model from specified file. Use this option multiple times to load the weights for multiple autoencoders. Note, that at least one autoencoder is needed for the model to work at all, and at least three autoencoders should be specified for the model to function as intended. Also note that for each autoencoder, both an architecture file (.json) and a weights file (.hdf5) must be specified. Use --autoencoder-arch FILE to specify the weights file for each autoencoder [default: %default].")
    cmdln.add_argument("--load-model", action="store_true", dest="LOAD_MODEL", default=False,
                                help="load the model architecture and weights from disk [default: %default]. Note, if this is set then --load-model-arch=FILE and --load-model-weights=FILE must also be set.")
    cmdln.add_argument("--load-model-arch", action="store", dest="LOAD_ARCH_FILE", default="",
                                help="load the model architecture from specified file [default: %default]. Note, --load-model and --load-model-weights=FILE must also be set to actually load the model.")
    cmdln.add_argument("--load-model-weights", action="store", dest="LOAD_WEIGHTS_FILE", default="",
                                help="load the model weights from specified file [default: %default]. Note, --load-model and --load-model-arch=FILE must also be set to actually load the model.")
    cmdln.add_argument("--save-model", action="store_true", dest="SAVE_MODEL", default=False,
                                help="save the model architecture (before training) and weights (after training) to disk [default: %default]. Note, if this is set then --save-model-arch=FILE and --save-model-weights=FILE must also be set.")
    cmdln.add_argument("--save-model-arch", action="store", dest="SAVE_ARCH_FILE", default="",
                                help="save the model architecture (before training) to specified file [default: %default]. Note, --save-model must also be set to actually save the model architecture.")
    cmdln.add_argument("--save-model-weights", action="store", dest="SAVE_WEIGHTS_FILE", default="",
                                help="save the model weights (after training) to specified file [default: %default]. Note, --save-model must also be set to actually save the model weights.")
    cmdln.add_argument("--save-checkpoints", action="store_true", dest="SAVE_CHECKPOINTS", default=False,
                                help="save the model weights to disk at the end of each training epoch [default: %default]. Note, if this is set then --save-checkpoints-weights=FILE must also be set.")
    cmdln.add_argument("--save-checkpoints-weights", action="store", dest="CHECKPOINTS_PATH", default="",
                                help="save the model weights (during training) to specified file at the end of epoch of training  [default: %default]. Note, --save-checkpoints must also be set to actually save the model weights during training. Also note, the filename should have the following format to provide information about each checkpoint file and to ensure they don't overwrite each other: <name>.weights.{epoch:02d}-{val_acc:.2f}.hdf5")
    cmdln.add_argument("--word2v-d", action="store", dest="WORD2VEC_D", default=300, type=int,
                                help="the dimension to use for the word embedding layear of the model [default: %default]. Note, this must match the dimensionality of the index2vector matrix and word2vector dictionary that are passed to the program as well.")
    cmdln.add_argument("--sent2v-d", action="store", dest="SENT2VEC_D", default=256, type=int,
                                help="the dimension to use for the sentence embedding layear of the model [default: %default]. Note, if loading the model from file, this must match the dimensionality of the sentence embedding used in the loaded model.")
    cmdln.add_argument("--rev2v-d", action="store", dest="REV2VEC_D", default=128, type=int,
                                help="the dimension that the review will be encoded into before being passed through the final fully connected Dense classification layer(s) of the model [default: %default]. Note, if loading the model from file, this must match the dimensionality of the review encoding used in the loaded model.")
    cmdln.add_argument("--num-encoders", action="store", dest="NUM_ENCODERS", default=5, type=int, required=True,
                                help="the number of autoencoders that this model is using to transform each input sentence in a review into a domain-invariant embedded representation [default: %default]. Note, if loading the model from file, this must match the number of autoencoders used in the loaded model.")
    cmdln.add_argument("--vocab-size", action="store", dest="VOCAB_SIZE", default=10000, type=int,
                                help="the size of the vocabulary being used [default: %default]. Note, this must match the size of the vocab dictionary that is being passed to the program.")
    cmdln.add_argument("--batch-size", action="store", dest="BATCH_SIZE", default=64, type=int,
                                help="the batch size to use while training the model [default: %default].")
    cmdln.add_argument("--n-epochs", action="store", dest="N_EPOCHS", default=20, type=int,
                                help="the number of epochs to train the model for [default: %default].")
    cmdln.add_argument("--epoch-size", action="store", dest="EPOCH_SIZE", default=6400, type=int,
                                help="the model uses an infinite generator as its data source (meaning it continually loops over the training data), so this specifies the number of training examples that should be seen before 1 epoch is considered to have passed [default: %default]. Note, this must be a multiple of the batch size.")
    cmdln.add_argument("--maxlen-sent", action="store", dest="MAXLEN_SENT", default=20, type=int,
                                help="the maximum length of a sentence (number of words). All longer sentences will be (front-end) cropped to this length and all shorter sentences will be (front-end) padded (using masking) to this length to allow uniform training of the network [default: %default].")
    cmdln.add_argument("--maxlen-rev", action="store", dest="MAXLEN_REV", default=10, type=int,
                                help="the maximum length of a review (number of sentences). All longer reviews will be (front-end) cropped to this length and all shorter reviews will be (front-end) padded (using masking) to this length to allow uniform training of the network [default: %default].")
    cmdln.add_argument("--optimizer", action="store", dest="OPTIMIZER", default="adam",
                            help="the optimizer to use for the classifier model (sgd, rmsprop, adagrad, adadelta, adamax, adam, etc.) [default: %default].")
    cmdln.add_argument("--lr-start", action="store", dest="LR_START", default=0.001, type=float,
                            help="the starting learning rate to use while training the classifier model [default: %default].")
    cmdln.add_argument("--lr-end", action="store", dest="LR_END", default=0.001, type=float,
                            help="the ending learning rate to use while training the classifier model [default: %default].")
    cmdln.add_argument("--lr-hyp", action="store", dest="LR_HYP", default=0.0, type=float,
                            help="hyper parameter related to controlling the learning rate used while training the classifier model (e.g. learning rate decay, etc. Note, what this stands for exactly depends on the optimzer being used) [default: %default].")
    cmdln.add_argument("--dropout-lstm-w", action="store", dest="DROPOUT_LSTM_W", default=0.0, type=float,
                                help="the dropout rate to use for the input gates of the lstm layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--dropout-lstm-u", action="store", dest="DROPOUT_LSTM_U", default=0.0, type=float,
                                help="the dropout rate to use for the recurrent connections of the lstm layer(s) of the classifier model [default: %default].")
    # cmdln.add_argument("--dropout-dense", action="store", dest="DROPOUT_DENSE", default=0.0, type=float,
    #                             help="the dropout rate to use for the final dense layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--dense-l2-w", action="store", dest="DENSE_L2_W", default=0.001, type=float,
                                help="the L2 regularization strength to use for the weights on the final dense layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--dense-l2-b", action="store", dest="DENSE_L2_B", default=0.001, type=float,
                                help="the L2 regularization strength to use for the bias on the final dense layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--val-batches", action="store", dest="VAL_BATCHES", default=20, type=int,
                                help="the number of batches of the training data to set aside for validation at the end of each epoch. The model is not trained on this data. Note, all validation data is pulled from the front of the training data, so training data should be shuffled in advance. [default: %default].")
    cmdln.add_argument("--idx2v", action="store", dest="IDX2V_FILE", default="", required=True,
                                help="load the index2vector numpy weights matrix (.npy file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--idx2w", action="store", dest="IDX2W_FILE", default="", required=True,
                                help="load the index2word python list (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--w2idx", action="store", dest="W2IDX_FILE", default="", required=True,
                                help="load the word2index python dict (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--w2v", action="store", dest="W2V_FILE", default="", required=True,
                                help="load the word2vector python dict (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--vocab-list", action="store", dest="VOCAB_FILE", default="", required=True,
                                help="load the vocab python list of tuples (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--expID", action="store", dest="EXPID", default="",
                                help="name or ID of the experiment [default: %default].")
    return cmdln


if __name__ == "__main__":
    main()
