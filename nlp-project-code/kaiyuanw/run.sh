#!/bin/zsh

DEPARTMENTS=(
        "Books"
        "Electronics"
        "Home_and_Kitchen"
        "Movies_and_TV"
)

BROWNSIZES=(
        "1000"
        "2000"
        "3000"
        "4000"
        "5000"
        "7000"
        "10000"
        "13000"
        "17000"
        "21000"
)

# ===================  Utils  ===================

function count.reviews.for.all() {
        logname="info/reviews.count.log"
        rm ${logname}
        echo "Counting number of reviews of all departments ..."
	for dirname in $(ls -d raw/*/ | head -100); do
		for filename in $(ls -d ${dirname}* | head -100); do
                        ./util.sh --count ${filename} >> ${logname}
                done
	done
        echo "Done"
}

function shuffle.reviews.all() {
        interpolationdir="data/PosNeg/Interpolation"
        echo "Shuffling all data files ..."
        for filepath in $(ls -d ${interpolationdir}/* | head -100); do
                ./util.sh --shuffle ${filepath}
        done
        echo "Done"
}

function generate.shuffle.test.all() {
        posnegpath="data/PosNeg"
	for department in ${DEPARTMENTS[@]}; do
		departmentpath=${posnegpath}"/"${department}
		echo "Generating and shuffling test set for ${department}..."
		./util.sh --generate-test ${departmentpath}
		echo "Done"
	done
}

function change.seed.ctrl.experiment.wsj.test() {
	for size in ${WSJSIZES[@]}; do
		seedNum=${size}
		echo "Running seed set of size ${seedNum} on WSJ, disenable self-training, testing on WSJ..."
		./run.sh -p --train Parser/concat/wsj/wsj_${seedNum}.mrg --test Parser/concat/wsj/wsj_test.mrg 1> logs/change.seed/ctrl/test.wsj/seed-${seedNum}.out 2> logs/change.seed/ctrl/test.wsj/seed-${seedNum}.err
		echo "Done"
	done
}

function change.self.test() {
	for size in ${BROWNSIZES[@]}; do
		selfNum=${size}
		echo "Running self-training set of size ${selfNum} on Brown, testing on Brown ..."
		./run.sh -p --train Parser/concat/wsj/wsj_10000.mrg --self-training Parser/concat/brown/brown_${selfNum}.mrg --test Parser/concat/brown/brown_test.mrg 1> logs/change.self.training/self-${selfNum}.out 2> logs/change.self.training/self-${selfNum}.err
		echo "Done"
	done
}

# ===================  Invert source and target  ===================

function change.seed.inv.test() {
	for size in ${BROWNSIZES[@]}; do
		seedNum=${size}
		echo "Running seed set of size ${seedNum} on Brown, testing on WSJ ..."
		./run.sh -p --train Parser/concat/brown/brown_${seedNum}.mrg --self-training Parser/concat/wsj/wsj_train.mrg --test Parser/concat/wsj/wsj_test.mrg 1> logs/change.seed.inv/seed-${seedNum}.out 2> logs/change.seed.inv/seed-${seedNum}.err
		echo "Done"
	done
}

function change.seed.inv.ctrl.experiment.wsj.test() {
	for size in ${BROWNSIZES[@]}; do
		seedNum=${size}
		echo "Running seed set of size ${seedNum} on Brown, disenable self-training, testing on WSJ..."
		./run.sh -p --train Parser/concat/brown/brown_${seedNum}.mrg --test Parser/concat/wsj/wsj_test.mrg 1> logs/change.seed.inv/ctrl/test.wsj/seed-${seedNum}.out 2> logs/change.seed.inv/ctrl/test.wsj/seed-${seedNum}.err
		echo "Done"
	done
}

function change.seed.inv.ctrl.experiment.brown.test() {
	for size in ${BROWNSIZES[@]}; do
		seedNum=${size}
		echo "Running seed set of size ${seedNum} on Brown, disenable self-training, testing on Brown..."
		./run.sh -p --train Parser/concat/brown/brown_${seedNum}.mrg --test Parser/concat/brown/brown_test.mrg 1> logs/change.seed.inv/ctrl/test.brown/seed-${seedNum}.out 2> logs/change.seed.inv/ctrl/test.brown/seed-${seedNum}.err
		echo "Done"
	done
}

function change.self.inv.test() {
	for size in ${WSJSIZES[@]}; do
		selfNum=${size}
		echo "Running self-training set of size ${selfNum} on WSJ, testing on WSJ ..."
		./run.sh -p --train Parser/concat/brown/brown_10000.mrg --self-training Parser/concat/wsj/wsj_${selfNum}.mrg --test Parser/concat/wsj/wsj_test.mrg 1> logs/change.self.training.inv/self-${selfNum}.out 2> logs/change.self.training.inv/self-${selfNum}.err
		echo "Done"
	done
}

# ===================  Show results  ===================

function change.seed.show() {
	sublime logs/change.seed/*.err
}

function change.self.show() {
	sublime logs/change.self.training/*.err
}

function change.seed.inv.show() {
	sublime logs/change.seed.inv/*.err
}

function change.self.inv.show() {
	sublime logs/change.self.training.inv/*.err
}

function clean() {
	rm logs/**/*
}

function help() {
	echo "Try commands like:"
	echo "./test.sh --chseed"
	echo "./test.sh --chseed-ctrl-tbrown"
	echo "./test.sh --chseed-ctrl-twsj"
	echo "./test.sh --chself"
	echo "./test.sh --chseed-inv"
	echo "./test.sh --chseed-inv-ctrl-tbrown"
	echo "./test.sh --chseed-inv-ctrl-twsj"
	echo "./test.sh --chself-inv"
	echo "./test.sh --show-chseed"
	echo "./test.sh --show-chself"
	echo "./test.sh --show-chseed-inv"
	echo "./test.sh --show-chself-inv"
	echo "./test.sh --clean"
	echo "./test.sh --help"
}

function main() {
        case $1 in
        --count-all) shift
                count.reviews.for.all "$@"
                ;;
        --shuffle-all) shift
                shuffle.reviews.all "$@"
                ;;
        --generate-test-all) shift
                generate.shuffle.test.all "$@"
                ;;
        --chself) shift
                change.self.test "$@"
                ;;
        --chseed-inv) shift
                change.seed.inv.test "$@"
                ;;
        --chseed-inv-ctrl-twsj) shift
                change.seed.inv.ctrl.experiment.wsj.test "$@"
                ;;
        --chseed-inv-ctrl-tbrown) shift
                change.seed.inv.ctrl.experiment.brown.test "$@"
                ;;
        --chself-inv) shift
                change.self.inv.test "$@"
                ;;
        --show-chseed) shift
                change.seed.show "$@"
                ;;
        --show-chself) shift
                change.self.show "$@"
                ;;
        --show-chseed-inv) shift
                change.seed.inv.show "$@"
                ;;
        --show-chself-inv) shift
                change.self.inv.show "$@"
                ;;
        -c|--clean) shift
                clean "$@"
                ;;
        -h|--help) shift
                help "$@"
                ;;
        *)
                echo "Check arguments";;
        esac
}

main $@