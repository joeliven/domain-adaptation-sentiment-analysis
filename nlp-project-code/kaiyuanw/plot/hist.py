__author__ = 'Kaiyuan_Wang'

import os
import numpy as np
import matplotlib.pyplot as plt

def draw_transfer_loss():
    fig = plt.figure(figsize=(14, 5))
    ax = fig.add_subplot(111)
    N = 6
    svm_unigram_bigram = [7, 7, 3, 1, 5, 5]
    svm_unigram = [15, 13, 8, 6, 11, 13]
    simple_alg = [7.1, 3.33, 1.9, -1.1, 3.25, 3.77]
    complex_alg = [30.31, 30.44, 35.88, 26.84, 34.31, 35]
    ind = np.arange(N)
    width = 0.2

    rects1 = ax.bar(ind-width, svm_unigram_bigram, width,
                color='black')

    rects2 = ax.bar(ind, svm_unigram, width,
                    color='red')

    rects3 = ax.bar(ind+width, simple_alg, width,
                    color='green')

    rects4 = ax.bar(ind+2*width, complex_alg, width,
                    color='blue')

    ax.set_xlim(-2*width,len(ind))
    ax.set_ylim(-2,40)
    ax.set_ylabel('Transfer loss (%)')
    # ax.set_title('Transfer loss comparison')
    xTickMarks = ['BE', 'EB', 'BM', 'MB', 'EM', 'ME']
    ax.set_xticks(ind+width)
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=0, fontsize=10)

    ## add a legend
    ax.legend( (rects1[0], rects2[0], rects3[0], rects4[0]), ('SVM(unigram + bigram)', 'SVM(unigram)', 'Simple', 'Complex'), loc=9, ncol=4, bbox_to_anchor=(0.5,1.12), prop={'size':13} )
    ax.grid('on')

    plt.savefig('/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/report/figs/hist.png', dpi=500, bbox_inches='tight', pad_inches=0.1)

    plt.close()

    # plt.show()

    # plt.figure()
    # mu, sigma = 100, 15
    # x = np.array([[1,1,1,1], [2,2,2,2], [3,3,3,3], [4,4,4,4], [5,5,5,5], [6,6,6,6]])
    # print x.shape
    # n, bins, patches = plt.hist(x, 6, normed=False, histtype='bar',
    #                         color=['black', 'red', 'green', 'blue'],
    #                         label=['SVM (unigram + bigram)', 'SVM (unigram)', 'Simple', 'Complex'])
    # plt.legend()
    # plt.show()

def main():
    draw_transfer_loss()

if __name__ == '__main__':
    main()