__author__ = 'Kaiyuan_Wang'

import os
import time
import re
import math
import numpy
from nltk import sent_tokenize
from nltk import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import svm
from sklearn.metrics import classification_report

# source_train_data = []
# source_train_labels = []
# source_test_data = []
# source_test_labels = []
#
# target_train_data = []
# target_train_labels = []
# target_test_data = []
# target_test_labels = []
#
# source_train_vectors = None
# source_test_vectors = None
# target_train_vectors = None
# target_test_vectors = None

def read_line(path):
    for line in open(path, 'r'):
        review_dic = eval(line)
        text = review_dic['reviewText']
        # sentences = sent_tokenize(review_dic['reviewText'])
        # text = None
        # for sentence in sentences:
        #     if re.match(r'^\s*$', sentence):
        #         continue
        #     text += sentence + '\n'
        # text = reduce(lambda x,y: x + y if x == '' or y == '' else x + '\n' + y, sentences, '')
        # print [word_tokenize(sentence) for sentence in sentences]
        # tokens = reduce(lambda x,y: x + y, [word_tokenize(sentence) for sentence in sentences], [])
        # print tokens
        if float(review_dic['overall']) < 3:
            label = 'neg'
        else:
            label = 'pos'
        # if len(text) < 5:
        #     continue
        if text and label:
            yield text, label

def read_data(data_path):
    data = []
    labels = []
    for train_text, train_label in read_line(data_path):
        data.append(train_text)
        labels.append(train_label)
    return data, labels

# def read_target_train_data(train_data_path):
#     for train_text, train_label in read_line(train_data_path):
#         target_train_data.append(train_text)
#         target_train_labels.append(train_label)
#
# def read_source_test_data(test_data_path):
#     for test_text, test_label in read_line(test_data_path):
#         source_test_data.append(test_text)
#         source_test_labels.append(test_label)
#
# def read_target_test_data(test_data_path):
#     for test_text, test_label in read_line(test_data_path):
#         target_test_data.append(test_text)
#         target_test_labels.append(test_label)

def transform_tfidf_data(train_data, test_data):
    # count_vectorizer = CountVectorizer(ngram_range=(1,2), token_pattern=r'\b\w+\b', lowercase=False)
    count_vectorizer = CountVectorizer(lowercase=True)
    transformer = TfidfTransformer(sublinear_tf=True, use_idf=True)
    data = count_vectorizer.fit_transform(train_data)
    train_vectors = transformer.fit_transform(data)
    data = count_vectorizer.transform(test_data)
    test_vectors = transformer.fit_transform(data)
    return train_vectors, test_vectors

def transform_ngram_data(train_data, test_data):
    # count_vectorizer = CountVectorizer(ngram_range=(1,2), token_pattern=r'\b\w+\b', lowercase=True)
    count_vectorizer = CountVectorizer(ngram_range=(1,1), token_pattern=r'\b\w+\b', lowercase=True)
    train_vectors = count_vectorizer.fit_transform(train_data)
    test_vectors = count_vectorizer.transform(test_data)
    return train_vectors, test_vectors

def train_svm_rbf(train_vectors, train_labels, test_vectors, test_labels):
    classifier_rbf = svm.SVC()
    t0 = time.time()
    # print train_vectors
    classifier_rbf.fit(train_vectors, train_labels)
    t1 = time.time()
    prediction_rbf = classifier_rbf.predict(test_vectors)
    t2 = time.time()
    time_rbf_train = t1-t0
    time_rbf_predict = t2-t1
    print "Results for SVC(kernel=rbf)"
    print "Training time: %fs; Prediction time: %fs" % (time_rbf_train, time_rbf_predict)
    print(classification_report(test_labels, prediction_rbf))

def train_svm_linear(train_vectors, train_labels, test_vectors, test_labels):
    classifier_linear = svm.SVC(kernel='linear')
    t0 = time.time()
    classifier_linear.fit(train_vectors, train_labels)
    t1 = time.time()
    prediction_linear = classifier_linear.predict(test_vectors)
    t2 = time.time()
    time_linear_train = t1-t0
    time_linear_predict = t2-t1
    print("Results for SVC(kernel=linear)")
    print("Training time: %fs; Prediction time: %fs" % (time_linear_train, time_linear_predict))
    print(classification_report(test_labels, prediction_linear))

def train_svm_liblinear(train_vectors, train_labels, test_vectors, test_labels):
    classifier_liblinear = svm.LinearSVC()
    t0 = time.time()
    classifier_liblinear.fit(train_vectors, train_labels)
    t1 = time.time()
    prediction_liblinear = classifier_liblinear.predict(test_vectors)
    t2 = time.time()
    time_liblinear_train = t1-t0
    time_liblinear_predict = t2-t1
    print("Results for LinearSVC()")
    print("Training time: %fs; Prediction time: %fs" % (time_liblinear_train, time_liblinear_predict))
    print(classification_report(test_labels, prediction_liblinear))

def main():
    source_train_path = '/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/kaiyuanw/data/PosNeg/Interpolation/Electronics0_Movies_and_TV100_100000.txt'
    target_train_path = '/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/kaiyuanw/data/PosNeg/Interpolation/Electronics100_Movies_and_TV0_100000.txt'
    source_test_path = '/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/kaiyuanw/data/PosNeg/Movies_and_TV/test.txt'
    target_test_path = '/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/kaiyuanw/data/PosNeg/Electronics/test.txt'
    source_train_data, source_train_labels = read_data(source_train_path)
    target_train_data, target_train_labels = read_data(target_train_path)
    target_test_data, target_test_labels = read_data(target_test_path)
    # source_train_vectors, target_test_vectors = transform_tfidf_data(source_train_data, target_test_data)
    source_train_vectors, target_test_vectors = transform_ngram_data(source_train_data, target_test_data)
    # train_svm_rbf(source_train_vectors, source_train_labels, target_test_vectors, target_test_labels)
    # train_svm_linear(source_train_vectors, source_train_labels, target_test_vectors, target_test_labels)
    train_svm_liblinear(source_train_vectors, source_train_labels, target_test_vectors, target_test_labels)
    # target_train_vectors, target_test_vectors = transform_tfidf_data(target_train_data, target_test_data)
    target_train_vectors, target_test_vectors = transform_ngram_data(target_train_data, target_test_data)
    train_svm_liblinear(target_train_vectors, target_train_labels, target_test_vectors, target_test_labels)

if __name__ == '__main__':
    main()
    # print train_data[0]
    # print train_data[1]
    # print len(train_data)
    # print train_labels
    # print len([train_label for train_label in train_labels if train_label == 'neg'])