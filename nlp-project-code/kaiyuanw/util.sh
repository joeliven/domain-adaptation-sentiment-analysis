#!/bin/bash

function reviews.count() {
	wc -l $1
}

function reviews.shuffle() {
	file=${1}; shift
	gshuf ${file} --output=${file}
}

function generate.and.shuffle.test() {
	departmentpath=${1}; shift
	negtestpath=${departmentpath}"/neg_test.txt"
	postestpath=${departmentpath}"/pos_test.txt"
	testpath=${departmentpath}"/test.txt"
	cat ${negtestpath} > ${testpath}
	cat ${postestpath} >> ${testpath}
	reviews.shuffle ${testpath}
}

# ./run.sh -s --corpus-root Parser/corpus/wsj --file-list 00,01 --concat-dst test.mrg --corpus-size 2000
# ./run.sh -s --corpus-root Parser/corpus/wsj --file-list 00,01 --concat-train train.mrg --concat-test test.mrg --split 0.9
function preprocessor.run() {
	java -cp Parser.jar:Parser/libs/ejml-0.23.jar:Parser/libs/slf4j-api.jar:Parser/libs/slf4j-simple.jar \
	-server -mx4000m \
	edu.utexas.nlp.preprocessor.Preprocessor "$@"
}

function help() {
	echo "Try commands like:"
	echo "============="
	echo "./run.sh -b"
	echo "============="
	echo "./run.sh -cj"
	echo "============="
	echo "./run.sh -p args..."
	echo "args... for ./run.sh -p:"
	java -cp Parser.jar \
	-server -mx1500m \
	edu.utexas.nlp.parser.Parser --help
	echo "============="
	echo "./run.sh -s args..."
	echo "args... for ./run.sh -s:"
	java -cp Parser.jar \
	-server -mx1500m \
	edu.utexas.nlp.preprocessor.Preprocessor --help
	echo "============="
	echo "./run.sh -h"
	echo "============="
}

function main() {
        case $1 in
        -c|--count) shift
                reviews.count "$@"
                ;;
        -shuf|--shuffle) shift
                reviews.shuffle "$@"
                ;;
        -gt|--generate-test) shift
                generate.and.shuffle.test "$@"
                ;;
        -s|--preprocess) shift
                preprocessor.run "$@"
                ;;
        -h|--help) shift
                help "$@"
                ;;
        *)
                echo "Check arguments";;
        esac
}

main $@