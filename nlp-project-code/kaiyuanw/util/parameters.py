__author__ = 'Kaiyuan_Wang'

import os

'''For preprocessor, autoencoder and classifier'''
PROJECT_DIR = os.path.abspath(os.path.join(os.getcwd(), os.pardir))

VOCAB_SIZE = 10000
WORD2V_D = 300

'''For preprocessor'''
'''Books, Electronics, Movies_and_TV'''
S_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Electronics100_Movies_and_TV0_100000.txt'
T_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Electronics0_Movies_and_TV100_100000.txt'
VSM_PATH = PROJECT_DIR + '/model/GoogleNews-vectors-negative300.bin.gz'
VOCAB_PATH = PROJECT_DIR + '/gen/PosNeg/EM/vocabulary.pickle'
SAVE_WORD2IDX_PATH = PROJECT_DIR + '/gen/PosNeg/EM/word2idx.pickle'
SAVE_IDX2VEC_PATH = PROJECT_DIR + '/gen/PosNeg/EM/idx2vec.pickle'
SAVE_IDX2VEC_ARR_PATH = PROJECT_DIR + '/gen/PosNeg/EM/idx2vec'
# GOOGLE_WORD2VEC_PATH = PROJECT_DIR + '/model/GoogleNews-vectors-negative300.bin.gz'

'''For autoencoder'''
MAXLEN_SENT = 40
SENT2VEC_D = 500
AUTOENCODER_BATCH_SIZE = 64 #64
AUTOENCODER_EPOCH_SIZE = 6400 # 6400
AUTOENCODER_N_EPOCHS=20 #20
AUTOENCODER_DROPOUT_ELSTM_U = 0.2
AUTOENCODER_DROPOUT_ELSTM_W = 0.2
AUTOENCODER_DROPOUT_DLSTM_U = 0.2
AUTOENCODER_DROPOUT_DLSTM_W = 0.2
AUTOENCODER_SOFTMAX_L2_W = 0.01
AUTOENCODER_SOFTMAX_L2_B = 0.01
AUTOENCODER_OPTIMIZER = "adam"
LOAD_WORD2IDX_PATH = PROJECT_DIR + '/gen/PosNeg/BE/word2idx.pickle'
LOAD_IDX2VEC_ARR_PATH = PROJECT_DIR + '/gen/PosNeg/BE/idx2vec.npy'
LOAD_IDX2VEC_PATH = PROJECT_DIR + '/gen/PosNeg/BE/idx2vec.pickle'
AUTOENCODER_TRAINING_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books50_Electronics50_100000.txt'
AUTOENCODER_VALIDATION_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books50_Electronics50_1000_val.txt'
AUTOENCODER_CHECKPOINTS_WEIGHTS_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder3.weights.best.hdf5'
AUTOENCODER_SAVE_MODEL_ARCH_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder3.model.json'
AUTOENCODER_SAVE_MODEL_WEIGHTS_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder3.weights.hdf5'

'''For classifier'''
MAXLEN_REV = 15
REV2VEC_D = 500
CLASSIFIER_BATCH_SIZE=64 #64
CLASSIFIER_N_EPOCHS = 20 #20
CLASSIFIER_EPOCH_SIZE = 6400 #6400
CLASSIFIER_DROPOUT_LSTM_W = 0.2
CLASSIFIER_DROPOUT_LSTM_U = 0.2
CLASSIFIER_DENSE_L2_W = 0.01
CLASSIFIER_DENSE_L2_B = 0.01
CLASSIFIER_OPTIMIZER="adam"
CLASSIFIER_TRAINING_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books100_Electronics0_100000.txt'
CLASSIFIER_TEST_DATA = PROJECT_DIR + '/data/PosNeg/Electronics/test.txt'
CLASSIFIER_VALIDATION_DATA_PATH = PROJECT_DIR + '/data/PosNeg/Interpolation/Books100_Electronics0_1000_val.txt'
AUTOENCODER1_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder1.model.json'
AUTOENCODER1_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder1.weights.best.hdf5'
AUTOENCODER2_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder2.model.json'
AUTOENCODER2_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder2.weights.best.hdf5'
AUTOENCODER3_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder3.model.json'
AUTOENCODER3_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder3.weights.best.hdf5'
AUTOENCODER4_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder4.model.json'
AUTOENCODER4_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder4.weights.best.hdf5'
AUTOENCODER5_MODEL_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder5.model.json'
AUTOENCODER5_WIGHT_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_autoencoder5.weights.best.hdf5'
AUTOENCODER_PATHS = {
    'autoencoder1': (AUTOENCODER1_MODEL_PATH, AUTOENCODER1_WIGHT_PATH),
    'autoencoder2': (AUTOENCODER2_MODEL_PATH, AUTOENCODER2_WIGHT_PATH),
    'autoencoder3': (AUTOENCODER3_MODEL_PATH, AUTOENCODER3_WIGHT_PATH),
    'autoencoder4': (AUTOENCODER4_MODEL_PATH, AUTOENCODER4_WIGHT_PATH),
    'autoencoder5': (AUTOENCODER5_MODEL_PATH, AUTOENCODER5_WIGHT_PATH)
}
CLASSIFIER_CHECKPOINTS_WEIGHTS_PATH = PROJECT_DIR + '/gen/PosNeg/BE/BE_classifier.weights.best.hdf5'
CLASSIFIER_SAVE_MODEL_ARCH_PATH = PROJECT_DIR + "/gen/PosNeg/BE/BE_classifier.model.json"
CLASSIFIER_SAVE_MODEL_WEIGHTS_PATH = PROJECT_DIR + "/gen/PosNeg/BE/BE_classifier.weights.hdf5"