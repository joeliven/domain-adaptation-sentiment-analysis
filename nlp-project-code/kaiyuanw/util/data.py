__author__ = 'Kaiyuan_Wang'

import os

DEPARTMENTS = [
    'Books',
    'Electronics',
    'Home_and_Kitchen',
    'Movies_and_TV'
]

def reviews_count_key(full_key):
    return full_key[full_key.rfind('/') + 1 : full_key.rfind('.')]

def read_reviews_count():
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    f = open(project_dir + '/info/reviews.count.log', 'r')
    ls = f.read().split()
    return { reviews_count_key(ls[i + 1]): ls[i] for i in range(0, len(ls), 2) }

def main():
    print read_reviews_count()

if __name__ == '__main__':
    main()