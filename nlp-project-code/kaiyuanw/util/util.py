__author__ = 'Kaiyuan_Wang'

import os
from keras.models import model_from_json
from gensim.models import Word2Vec

def load_Google_word2vec(model_path):
    model = Word2Vec.load_word2vec_format(model_path, binary=True)
    return model

def save_model(model, path):
    model_arch = model.to_json()
    with open(path, "w+") as f:
        f.write(model_arch)
        f.truncate()
        f.close()

def save_model_weights(model, path):
    model.save_weights(path, overwrite=True)

def load_model(model_path, weights_path):
    with open(model_path, "r") as f:
        model = model_from_json(f.read())
    model.load_weights(weights_path)
    return model