__author__ = 'Kaiyuan_Wang'

import os
from nltk.tokenize import sent_tokenize, word_tokenize
from util.data import DEPARTMENTS
import pickle
import collections
from util.util import load_Google_word2vec

def read_review(data_path):
    for line in open(data_path, 'r'):
        yield eval(line)['reviewText']

def collect_vocabulary(data_paths):
    vocabulary_path = '/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/kaiyuanw/model/vocabulary.pickle'
    if os.path.isfile(vocabulary_path):
        with open(vocabulary_path, 'rb') as f:
            return pickle.load(f)
    vocabulary = {}
    for data_path in data_paths:
        for review in read_review(data_path):
            for sent in sent_tokenize(review):
                for word in word_tokenize(sent):
                    if word not in vocabulary:
                        vocabulary[word] = 1
                    else:
                        vocabulary[word] += 1
    with open(vocabulary_path, "wb") as f:
        pickle.dump(vocabulary, f)
    return vocabulary

def collect_sentence_length(data_paths):
    sentence_length_path = '/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/kaiyuanw/model/sentence_length.pickle'
    if os.path.isfile(sentence_length_path):
        with open(sentence_length_path, 'rb') as f:
            return pickle.load(f)
    sentence_length_list = []
    for data_path in data_paths:
        for review in read_review(data_path):
            for sent in sent_tokenize(review):
                sentence_length_list.append(len(word_tokenize(sent)))
    sentence_length_list.sort()
    with open(sentence_length_path, "wb") as f:
        pickle.dump(sentence_length_list, f)
    return sentence_length_list

def most_common_K_words(vocabulary, k):
    counter = collections.Counter(vocabulary)
    return counter.most_common(k)

def word2vec_sub_model(k_most_common_words, word2vec_model):
    word2vec_sub_model_path = '/Users/Kaiyuan_Wang/Secret/NLP/nlp-project/kaiyuanw/model/word2vec_sub_model.pickle'
    if os.path.isfile(word2vec_sub_model_path):
        with open(word2vec_sub_model_path, 'rb') as f:
            return pickle.load(f)
    word2vec_sub_model = dict()
    for common_word_tuple in k_most_common_words:
        common_word = common_word_tuple[0]
        if common_word in word2vec_model:
            word2vec_sub_model[common_word] = word2vec_model[common_word]
    with open(word2vec_sub_model_path, "wb") as f:
        pickle.dump(word2vec_sub_model, f)
    return word2vec_sub_model

def main():
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    data_paths = [project_dir + '/data/PosNeg/' + DEPARTMENT for DEPARTMENT in DEPARTMENTS]
    data_paths = [[data_path + '/neg_train.txt', data_path + '/pos_train.txt'] for data_path in data_paths]
    data_paths = reduce(lambda x, y: x + y, data_paths)
    # vocabulary = collect_vocabulary(data_paths)
    # print len(vocabulary)
    # common_words_list = most_common_K_words(vocabulary, 100000)
    # model_path = project_dir + '/model/GoogleNews-vectors-negative300.bin.gz'
    # word2vec_model = load_Google_word2vec(model_path)
    # word2vec_dic = word2vec_sub_model(common_words_list, word2vec_model)
    # print len(word2vec_dic)
    sentence_lengths = collect_sentence_length(data_paths)
    print sentence_lengths[0]
    print sentence_lengths[-1]
    print collections.Counter(sentence_lengths).most_common(30)
    print sum(sentence_lengths) * 1.0 / len(sentence_lengths)

if __name__ == '__main__':
    main()