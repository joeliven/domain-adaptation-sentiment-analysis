__author__ = 'Kaiyuan_Wang'

import os
import gzip
from util.data import DEPARTMENTS

def parse(path):
    g = gzip.open(path, 'r')
    for l in g:
        yield l, eval(l)

def split(path):
    start = path.rfind('/') + 1
    end = path[start:].find('.')
    review_name = path[start:][:end]
    department_name = review_name[review_name.find('_') + 1:]
    json12 = open(path[:start] + department_name + '/' + review_name + '12.json', 'w+')
    json3 = open(path[:start] + department_name + '/' + review_name + '3.json', 'w+')
    json123 = open(path[:start] + department_name + '/' + review_name + '123.json', 'w+')
    json45 = open(path[:start] + department_name + '/' + review_name + '45.json', 'w+')
    for review, review_dic in parse(path):
        if review_dic['overall'] == 1.0 or review_dic['overall'] == 2.0 :
            json12.write(review)
            json123.write(review)
        elif review_dic['overall'] == 3.0:
            json3.write(review)
            json123.write(review)
        elif review_dic['overall'] == 4.0 or review_dic['overall'] == 5.0:
            json45.write(review)
        else:
            print 'Unhandled case: ' + review
    json12.truncate()
    json12.close()
    json3.truncate()
    json3.close()
    json123.truncate()
    json123.close()
    json45.truncate()
    json45.close()


def main():
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    books_reviews_path = project_dir + '/raw/reviews_Books.json.gz'
    split(books_reviews_path)
    electronics_reviews_path = project_dir + '/raw/reviews_Electronics.json.gz'
    split(electronics_reviews_path)
    kitchen_reviews_path = project_dir + '/raw/reviews_Home_and_Kitchen.json.gz'
    split(kitchen_reviews_path)
    movies_reviews_path = project_dir + '/raw/reviews_Movies_and_TV.json.gz'
    split(movies_reviews_path)

if __name__ == '__main__':
    main()