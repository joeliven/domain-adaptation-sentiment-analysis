import os
import util.data_utils_nlp as du
import itertools
import argparse
import sys

##########################################################################
# START MAIN
##########################################################################
def main():
    # S_PATH="../../datasets/amazon/balanced/B_E/Books100_Electronics0.txt"
    # T_PATH="../../datasets/amazon/balanced/B_E/Books0_Electronics100.txt"
    # SAVE_DIR="../../datasets/amazon/balanced/B_E/vocab2"
    # SAVE_NAME="B_E_debug2"
    # VSM_PATH="../../datasets/vsms/word2vec/google_news/GoogleNews-vectors-negative300.bin"
    # STOPLIST_PATH="../../datasets/stoplists/stoplist_basic.txt"
    # WORD2V_D="300"
    # VOCAB_SIZE="10000"
    # DATA_LIM="100000"
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    S_PATH = project_dir + '/data/PosNeg/Interpolation/Books100_Electronics0_100000.txt'
    T_PATH = project_dir + '/data/PosNeg/Interpolation/Books0_Electronics100_100000.txt'
    SAVE_DIR = project_dir + '/gen/PosNeg/BE_debug'
    SAVE_NAME = 'BE_debug'
    VSM_PATH = project_dir + '/model/GoogleNews-vectors-negative300.bin.gz'
    STOPLIST_PATH = project_dir + 'gen/PosNeg/stoplist_basic.txt'
    WORD2V_D = 300
    VOCAB_SIZE = 10000
    DATA_LIM = 100000
    # sys.argv[0] = __name__
    # sys.argv[1] = '--verbose'
    # sys.argv[2] = '--brief'
    # sys.argv[3] = '--gpu'
    # sys.argv[4] = '--s-path'
    # sys.argv[5] = S_PATH
    # sys.argv[6] = '--t-path'
    # sys.argv[7] = T_PATH
    # sys.argv[8] = '--save-dir'
    # sys.argv[9] = SAVE_DIR
    # sys.argv[10] = '--save-name'
    # sys.argv[11] = SAVE_NAME
    # sys.argv[12] = '--vsm-path'
    # sys.argv[13] = VSM_PATH
    # sys.argv[14] = '--stoplist-path'
    # sys.argv[15] = STOPLIST_PATH
    # sys.argv[16] = '--word2v-d'
    # sys.argv[17] = WORD2V_D
    # sys.argv[18] = '--vocab-size'
    # sys.argv[19] = VOCAB_SIZE
    # sys.argv[20] = '--data-lim'
    # sys.argv[21] = DATA_LIM
    # sys.argv[22] = '--to-lower'
    # # PROCESS COMMANDLINE ARGS:
    # cmdln = prep_cmdln_parser()
    # args = cmdln.parse_args()
    # args_dict = vars(args)
    # vprint(args, "START PREPROCESSOR...")
    # # PRINT COMMANDLINE ARGS:
    # for key,value in args_dict.items():
    #     vprint(args, "%s:\t%s", (key,value))
    # if args.GPU == True:
    #     vprint(args, "Using GPU if available/possible.")

    # PREPROCESS THE RAW S&T DOMAIN DATA SETS TO BUILD NECESSARY VOCAB DATASTRUCTURES:
    du.amazon_preprocess_datasets(S_PATH, T_PATH, SAVE_DIR, SAVE_NAME, VSM_PATH,
                                  WORD2VEC_D=WORD2V_D, VOCAB_SIZE=VOCAB_SIZE, BRIEF=True, DATA_LIM=DATA_LIM, TO_LOWER=True)
##########################################################################
# END MAIN
##########################################################################


def vprint(args, string, fillers=None):
    """
        args: cmdln args object
        str: string to print
    """
    if args.VERBOSE == True:
        if fillers is not None:
            print(string % fillers)
        else:
            print(string)

def prep_cmdln_parser():
    usage = "usage: %prog [options]"
    cmdln = argparse.ArgumentParser(usage)
    cmdln.add_argument("-v", "--verbose", action="store_true", dest="VERBOSE", default=False,
                                help="print out more information during runtime [default: %default.")
    cmdln.add_argument("--brief", action="store_true", dest="BRIEF", default=False,
                                help="set this flag to use the brief version of the preprocesser/vocab builder (does not compute as many statistics while building vocab, so it runs slightly faster) [default: %default]")
    cmdln.add_argument("--gpu", action="store_true", dest="GPU", default=False,
                                help="train the model on a GPU (if supported) [default: %default].")

    cmdln.add_argument("--s-path", action="store", dest="S_PATH", default="", required=True,
                                help="load the raw Source Domain data from specified file [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--t-path", action="store", dest="T_PATH", default="", required=True,
                                help="load the raw Target Domain data from specified file [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--save-dir", action="store", dest="SAVE_DIR", default="", required=True,
                                help="the directory in which all generated files (data structures for this combined vocab) should be saved [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--save-name", action="store", dest="SAVE_NAME", default="S_T_",
                                help="the name to be prepended to the generated files (data structures for this combined vocab) [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--vsm-path", action="store", dest="VSM_PATH", default="S_T_", required=True,
                                help="the path in which the pretrained Google vector space model (VSM) is stored [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--stoplist-path", action="store", dest="STOPLIST_PATH", default="",
                                help="the path in which the list of stopwords to remove (if any) is stored [default: %default].")
    cmdln.add_argument("--remove-stops", action="store_true", dest="REMOVE_STOPS", default=False,
                                help="indicates that stopwords should be removed while building the vocab during preprocessing [default: %default].")
    cmdln.add_argument("--word2v-d", action="store", dest="WORD2VEC_D", default=300, type=int,
                                help="the dimension to use for the word embedding layear of the model [default: %default]. Note, this must match the dimensionality of the index2vector matrix and word2vector dictionary that are passed to the program as well.")
    cmdln.add_argument("--vocab-size", action="store", dest="VOCAB_SIZE", default=10000, type=int,
                                help="the size limit of the vocabulary being built from the Source (S) and Target (T) datasets [default: %default].")
    cmdln.add_argument("--data-lim", action="store", dest="DATA_LIM", default=-1, type=int,
                                help="set this to limit the amount of the data that is processed while constructing the vocabulary [default: %default].")
    cmdln.add_argument("--to-lower", action="store_true", dest="TO_LOWER", default=False,
                                help="set this flag to avoid converting all words in data set to lower case while processing vocabulary [default: %default].")
    return cmdln


if __name__ == "__main__":
    # if not len(sys.argv) == 2:
    #     print 'Please input 1 argument, the expected argument is the absolute path of the results folder.\n' \
    #           'For example, .../test-selection-refactorings/experiments/results'
    #     sys.exit(0)
    # result_dir = sys.argv[1]
    main()

