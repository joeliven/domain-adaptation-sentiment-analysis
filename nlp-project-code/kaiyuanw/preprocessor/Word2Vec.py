__author__ = 'Kaiyuan_Wang'

import os
import gzip
import gensim
import HTMLParser

class AmazonReviews(object):
    def __init__(self, dirname):
        self.dirname = dirname
        self.bound = 10

    def __iter__(self):
        gz = gzip.open(self.dirname, 'r')
        count = 0
        for json in gz:
            review_pack = eval(json)
            print count
            h = HTMLParser.HTMLParser()
            print h.unescape(review_pack['reviewText'])
            print review_pack
            yield review_pack['reviewText'].split()
            count += 1
            if count == self.bound:
                break


class MySentences(object):
    def __init__(self, dirname):
        self.dirname = dirname

    def __iter__(self):
        for fname in os.listdir(self.dirname):
            count = 0
            for line in open(os.path.join(self.dirname, fname)):
                print count
                print line
                yield line.split()
                count += 1
                if count == 10:
                    break

def parse(path):
    g = gzip.open(path, 'r')
    for l in g:
        yield eval(l)


def main():
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))

    # test_reviews_path = project_dir + '/test'
    # test_reviews = MySentences(test_reviews_path)
    # test_model = gensim.models.Word2Vec(test_reviews, min_count=1)
    # print test_model['a']

    book_reviews_path = project_dir + '/raw/reviews_Books.json.gz'
    book_reviews = AmazonReviews(book_reviews_path)
    model = gensim.models.Word2Vec(book_reviews, min_count=5, size=200)
    # questions_path = project_dir + '/eval/questions-words.txt'
    # print model.accuracy(questions_path)
    print len(model['the'])
    # count = 0
    # for review in parse(project_dir + "/raw/reviews_Books.json.gz"):
    # print review['reviewText']
    #     count += 1
    #     if count == 10:
    #         break


if __name__ == '__main__':
    main()