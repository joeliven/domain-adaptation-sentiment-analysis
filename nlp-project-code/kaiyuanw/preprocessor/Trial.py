__author__ = 'Kaiyuan_Wang'

import theano
import theano.tensor as T

'''
Plus operation
'''
a = T.dscalar()
b = T.dscalar()

c = a + b

f = theano.function([a,b],c)

print f(1.5, 2.5)

'''
Logistic function
'''
x = T.dmatrix('x')
s = 1 / (1 + T.exp(-x))

logistic = theano.function([x], s)
print logistic([[0,1],[-1,-2]])

s2 = (1 + T.tanh(x / 2)) / 2
logistic2 = theano.function([x], s2)
print logistic2([[0,1],[-1,-2]])

'''
Compute at the same time
'''
a,b = T.dmatrices('a','b')
diff = a - b
abs_diff = abs(diff)
diff_squared = diff ** 2
f = theano.function([a,b], [diff, abs_diff, diff_squared])

print f([[1,1],[1,1]], [[0,1],[2,3]])

'''
Default value
'''
from theano import In
from theano import function

x, y = T.dscalars('x', 'y')
z = x + y
f = function([x, In(y, value = 1)], z)
print f(33)
print f(33,2)

x,y,w = T.dscalars('x','y','w')
z = (x + y) * w
f = function([x, In(y, value=1), In(w, value=2, name='w_by_name')],z)
print f(33)
print f(33,2)
print f(33,0,1)
print f(33,w_by_name=1)
print f(33,w_by_name=1,y=0)