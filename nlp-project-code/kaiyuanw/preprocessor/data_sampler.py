__author__ = 'Kaiyuan_Wang'

import os
import random
import json
from util.data import read_reviews_count
from util.data import DEPARTMENTS

test_set_size = 10000
train_set_size = 1000000
validation_set_size = 1000
train_set_size_cap = 100000

def read_line(path):
    for line in open(path, 'r'):
        review_dic = eval(line)
        new_line = '{\"reviewText\": ' + json.dumps(review_dic['reviewText']) + ', ' \
                    '\"overall\": \"' + str(review_dic['overall']) + '\"}'
        yield new_line

def sample_pos_neg(count_dic, path_source, path_target):
    department = path_source[path_source.rfind('/') + 1:]
    neg_key = [key for key in count_dic.keys() if department + '12' in key and department + '123' not in key][0]
    pos_key = [key for key in count_dic.keys() if department + '45' in key][0]
    total_size = (test_set_size + train_set_size) / 2 # min(int(count_dic[neg_key]), int(count_dic[pos_key]))
    '''
    Assume negative reviews are less than positive reviews,
    which is true for all data set in the project
    '''
    test_index = random.sample(xrange(total_size), test_set_size / 2)
    neg_train_file = open(path_target + '/neg_train.txt', 'w+')
    neg_test_file = open(path_target + '/neg_test.txt', 'w+')
    pos_train_file = open(path_target + '/pos_train.txt', 'w+')
    pos_test_file = open(path_target + '/pos_test.txt', 'w+')
    count = 0
    for neg_review in read_line(path_source + '/' + neg_key + '.json'):
        if count in test_index:
            neg_test_file.write(neg_review + '\n')
        else:
            neg_train_file.write(neg_review + '\n')
        count += 1
        if count >= total_size:
            break
    count = 0
    for pos_review in read_line(path_source + '/' + pos_key + '.json'):
        if count in test_index:
            pos_test_file.write(pos_review + '\n')
        else:
            pos_train_file.write(pos_review + '\n')
        count += 1
        if count >= total_size:
            break
    neg_train_file.truncate()
    neg_train_file.close()
    neg_test_file.truncate()
    neg_test_file.close()
    pos_train_file.truncate()
    pos_train_file.close()
    pos_test_file.truncate()
    pos_test_file.close()

def interpolate(path_source, path_target, ratio, path_interpolation):
    neg_train_source = path_source + '/neg_train.txt'
    pos_train_source = path_source + '/pos_train.txt'
    neg_train_target = path_target + '/neg_train.txt'
    pos_train_target = path_target + '/pos_train.txt'
    train_cap = train_set_size_cap * ratio / 2
    val_cap = validation_set_size * ratio / 2
    source_department = path_source[path_source.rfind('/') + 1:]
    target_department = path_target[path_target.rfind('/') + 1:]
    train_path_save = path_interpolation + '/' + source_department + str(int(ratio * 100)) + '_' + target_department + str(int((1 - ratio) * 100)) + '_' + str(int(train_set_size_cap)) + '.txt'
    val_path_save = path_interpolation + '/' + source_department + str(int(ratio * 100)) + '_' + target_department + str(int((1 - ratio) * 100)) + '_' + str(int(validation_set_size)) + '_val.txt'
    save_file_train = open(train_path_save, 'w+')
    save_file_val = open(val_path_save, 'w+')
    count = 0
    for neg_review in read_line(neg_train_source):
        # if count >= train_cap:
        #     break
        if count < train_cap:
            save_file_train.write(neg_review + '\n')
        elif count < train_cap + val_cap:
            save_file_val.write(neg_review + '\n')
        else:
            break
        count += 1
    count = 0
    for neg_review in read_line(neg_train_target):
        if count < train_cap + val_cap:
            count += 1
            continue
        # if count >= train_set_size_cap / 2:
        #     break
        if count < train_set_size_cap / 2 + val_cap:
            save_file_train.write(neg_review + '\n')
        elif count < (train_set_size_cap + validation_set_size) / 2:
            save_file_val.write(neg_review + '\n')
        else:
            break
        count += 1
    count = 0
    for pos_review in read_line(pos_train_source):
        # if count >= train_cap:
        #     break
        if count < train_cap:
            save_file_train.write(pos_review + '\n')
        elif count < train_cap + val_cap:
            save_file_val.write(pos_review + '\n')
        else:
            break
        count += 1
    count = 0
    for pos_review in read_line(pos_train_target):
        if count < train_cap + val_cap:
            count += 1
            continue
        # if count >= train_set_size_cap / 2:
        #     break
        if count < train_set_size_cap / 2 + val_cap:
            save_file_train.write(pos_review + '\n')
        elif count < (train_set_size_cap + validation_set_size) / 2:
            save_file_val.write(pos_review + '\n')
        else:
            break
        count += 1
    save_file_train.truncate()
    save_file_train.close()
    save_file_val.truncate()
    save_file_val.close()

def sample_all():
    random.seed(20160424)
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    count_dic = read_reviews_count()
    src_books = project_dir + '/raw/Books'
    dst_books = project_dir + '/data/PosNeg/Books'
    sample_pos_neg(count_dic, src_books, dst_books)
    src_electronics = project_dir + '/raw/Electronics'
    dst_electronics = project_dir + '/data/PosNeg/Electronics'
    sample_pos_neg(count_dic, src_electronics, dst_electronics)
    src_home_and_kitchen = project_dir + '/raw/Home_and_Kitchen'
    dst_home_and_kitchen = project_dir + '/data/PosNeg/Home_and_Kitchen'
    sample_pos_neg(count_dic, src_home_and_kitchen, dst_home_and_kitchen)
    src_movies_and_tv = project_dir + '/raw/Movies_and_TV'
    dst_movies_and_tv = project_dir + '/data/PosNeg/Movies_and_TV'
    sample_pos_neg(count_dic, src_movies_and_tv, dst_movies_and_tv)

def interpolate_all():
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    search_dir = project_dir + '/data/PosNeg/'
    path_interpolation = project_dir + '/data/PosNeg/Interpolation'
    for i in range(len(DEPARTMENTS)):
        path_source = search_dir + DEPARTMENTS[i]
        for j in range(i + 1,len(DEPARTMENTS)):
            path_target = search_dir + DEPARTMENTS[j]
            for ratio in [1.0, 0.75, 0.5, 0.25, 0]:
                interpolate(path_source, path_target, ratio, path_interpolation)

def interpolate_individual(source, target):
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    search_dir = project_dir + '/data/PosNeg/'
    path_source = search_dir + source
    path_target = search_dir + target
    path_interpolation = project_dir + '/data/PosNeg/Interpolation'
    for ratio in [1.0, 0.75, 0.5, 0.25, 0]:
        interpolate(path_source, path_target, ratio, path_interpolation)

def simple_sample(file_name, cap):
    project_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    file_path = project_dir + '/data/PosNeg/Interpolation/' + file_name + '.txt'
    new_file_path = project_dir + '/data/PosNeg/Interpolation/' + file_name + '_simple_' + str(cap) + '.txt'
    save_file = open(new_file_path, 'w+')
    count = 0
    for review in read_line(file_path):
        if count >= cap:
            break
        save_file.write(review + '\n')
        count += 1
    save_file.truncate()
    save_file.close()


def main():
    # sample_all()
    interpolate_all()
    # interpolate_individual('Books', 'Electronics')
    # simple_sample('Books0_Electronics100', 2000)
    # simple_sample('Books100_Electronics0', 2000)

if __name__ == '__main__':
    main()