# import utils.data_utils_nlp_mse as du

import scipy as sci
import numpy as np

from keras.models import Sequential
from keras.models import model_from_json
from keras import backend as K
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.regularizers import l2, activity_l2
from keras.callbacks import Callback

import os

# Refnote: http://jmcauley.ucsd.edu/data/amazon/links.html
# Compute average rating
def compute_avg_rating(reviews):
    """    reviews is a generator object obtained from the amazon dataset,
            using the functions defined in data_utils.py.
            each iteration of the generator yields a dict object for a single review
    """
    ratings = []
    for review in reviews:
        ratings.append(review['overall'])
    avg = sum(ratings) / len(ratings)
    print("avg: " + str(avg))
    return avg

# Compute average rating
def get_count(reviews):
    """    gets the total number of entries in reviews, where
            reviews is a generator object obtained from the amazon dataset,
            using the functions defined in data_utils.py.
            each iteration of the generator yields a dict object for a single review
    """
    ct = 0
    for review in reviews:
        ct +=1
        if ct % 10000 == 0:
            print("ct: " + str(ct))
    print("ct: " + str(ct))
    return ct


def save_model(model, SAVE_DIR, MODEL_ARCH_PATH=None, MODEL_WEIGHTS_PATH=None, save="both"):
    if save == "both":
        if MODEL_ARCH_PATH is None or MODEL_WEIGHTS_PATH is None:
            print("MODEL_ARCH_PATH or MODEL_WEIGHTS_PATH not specified...not saving")
            return
        save_model_arch(model, SAVE_DIR, MODEL_ARCH_PATH)
        save_model_weights(model, SAVE_DIR, MODEL_WEIGHTS_PATH)
    elif save == "arch":
        if MODEL_ARCH_PATH is None:
            print("MODEL_ARCH_PATH  not specified...not saving")
            return
        save_model_arch(model, SAVE_DIR, MODEL_ARCH_PATH)
    elif save == "weights":
        if MODEL_WEIGHTS_PATH is None:
            print("MODEL_WEIGHTS_PATH not specified...not saving")
            return
        save_model_weights(model, SAVE_DIR, MODEL_WEIGHTS_PATH)
    else: # default to saving both
        print("\'save\' arg not recognized...defaulting to saving both arch and weights...")
        if MODEL_ARCH_PATH is None or MODEL_WEIGHTS_PATH is None:
            print("MODEL_ARCH_PATH or MODEL_WEIGHTS_PATH not specified...not saving")
            return
        save_model_arch(model, SAVE_DIR, MODEL_ARCH_PATH)
        save_model_weights(model, SAVE_DIR, MODEL_WEIGHTS_PATH)

def save_model_arch(model, SAVE_DIR, MODEL_ARCH_PATH):
    # create the save dir if it doesn't exist:
    if not os.path.exists(SAVE_DIR):
        os.mkdir(SAVE_DIR)
    # save the model architecture and weights:
    model_arch = model.to_json()
    print("Saving model.arch to: %s" % MODEL_ARCH_PATH)
    with open(MODEL_ARCH_PATH, "w") as arch:
        arch.write(model_arch)
    print("FINISHED saving model architecture.")

def save_model_weights(model, SAVE_DIR, MODEL_WEIGHTS_PATH):
    # create the save dir if it doesn't exist:
    if not os.path.exists(SAVE_DIR):
        os.mkdir(SAVE_DIR)
    print("Saving mdoel.weights to: %s" % MODEL_WEIGHTS_PATH)
    model.save_weights(MODEL_WEIGHTS_PATH)
    print("FINISHED saving model weights.")


def load_model(MODEL_ARCH_PATH, MODEL_WEIGHTS_PATH):
    print("Loading model.arch from: %s" % MODEL_ARCH_PATH)
    with open(MODEL_ARCH_PATH, "r") as f:
        model = model_from_json(f.read())
        # model = model_from_json(open('my_model_architecture.json').read())
    print("Loading model.weights from: %s" % MODEL_WEIGHTS_PATH)
    model.load_weights(MODEL_WEIGHTS_PATH)
    return model

