#imports
# get_ipython().magic('load_ext autoreload')
# get_ipython().magic('autoreload 2')

import utils.data_utils_nlp_mse as du
import nlp_proj.funcs as fn

from keras.models import Model
from keras.models import Sequential
from keras import backend as K
from keras.layers import Input, Dense, Embedding, RepeatVector, Dropout, Activation
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.preprocessing import sequence
from keras.utils import np_utils
from keras.optimizers import SGD
from keras.callbacks import Callback
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2, activity_l2
import theano

import scipy as sci
import numpy as np
import os, sys
import pickle
import itertools
import argparse

class MaskedAcc(Callback):
    def __init__(self, val_data):
        super().__init__()
        (self.X_val, self.X_val_1hot, self.mask) = val_data

        self.true_idxs = np.argmax(self.X_val_1hot, axis=2)
        # print("true_idxs")
        # print(self.true_idxs)
        # print(self.true_idxs.shape)
        self.mask = np.where(self.true_idxs == 0, 0, 1)
        # print("self.mask")
        # print(self.mask)
        # print(self.mask.shape)
        flat_mask = self.mask.flatten()
        self.n_masked = (flat_mask.shape[0] - np.sum(flat_mask))
        self.n_not_masked = flat_mask.shape[0] - self.n_masked
        print("n_masked:%d" % self.n_masked)
        print("n_not_masked:%d" % self.n_not_masked)

    def on_train_begin(self, logs={}):
        self.masked_val_accs = []

    def on_epoch_end(self, batch, logs={}):
        preds = self.model.predict_on_batch(self.X_val)
        # print("preds")
        # print(preds)
        # print(preds.shape)

        pred_idxs = np.argmax(preds, axis=2)
        # print("pred_idxs")
        # print(pred_idxs)
        # print(pred_idxs.shape)
        # print("np.sum(pred_idxs)")
        # print(np.sum(pred_idxs))

        pred_idxs *= self.mask
        # print("pred_idxs")
        # print(pred_idxs)
        # print(pred_idxs.shape)
        bool_acc = np.where(pred_idxs == self.true_idxs, 1, 0)
        # print("bool_acc")
        # print(bool_acc)
        # print(bool_acc.shape)

        matching = np.sum(bool_acc)
        print("matching:%d" % matching)
        correct = matching - self.n_masked
        print("correct: %d" % correct)
        masked_val_acc = float(correct) / self.n_not_masked
        self.masked_val_accs.append(masked_val_acc)
        print("\tmasked_val_acc=%f" % masked_val_acc)


def main():
    cmdln = prep_cmdln_parser()
    args = cmdln.parse_args()
    args_dict = vars(args)
    vprint(args, "START AUTOENCODER...")

    maxreclim = 10000
    reclim = sys.getrecursionlimit()
    print("max recursion lim: %d" % (reclim))
    print("resetting max recursion lim to: %d" % (maxreclim))
    sys.setrecursionlimit(maxreclim)
    reclim = sys.getrecursionlimit()
    print("max recursion lim is now: %d" % (reclim))

    for key,value in args_dict.items():
        vprint(args, "%s:\t%s", (key,value))
    if args.GPU == True:
        # theano.config.device = 'gpu'
        # theano.config.floatX = 'float32'
        vprint(args, "Using GPU if available.")

    # LOAD VOCAB:
    idx2w, w2idx, w2v, vocab_list, idx2v = load_vocab(args)

    # LOAD TRAINING DATA AS GENERATORS:
    X_train_gtors = du.amazon_process_gtors(args.TRAINING_DATA_FILE, w2idx, idx2w, idx2v, args.MAXLEN_SENT, args.VOCAB_SIZE, args.BATCH_SIZE, GTOR_LIM=args.GTOR_LIM, RETURN_EMBEDDINGS=False)

    # LOAD VALIDATION DATA AS GENERATORS:
    print("loading validation data as generators...")
    X_val_gtors = du.amazon_process_gtors(args.VAL_DATA_FILE, w2idx, idx2w, idx2v, args.MAXLEN_SENT, args.VOCAB_SIZE, args.BATCH_SIZE, INFINITE=False, RETURN_EMBEDDINGS=False) # False tells the genertor to NOT make it an infinite generator...

    print("converting validation generators to explicit numpy arrays...")
    # CONVERT VALIDATION GENERATORS TO EXPLICIT NUMPY ARRAYS:
    (X_val, X_val_1hot, val_sample_weights) = split_val_data(args, X_val_gtors)

    # SETUP CALLBACK FUCNTIONS:
    early_stopping = EarlyStopping(monitor='val_acc', patience=2)
    checkpoints = ModelCheckpoint(args.CHECKPOINTS_PATH, monitor='val_acc', verbose=0, save_best_only=True, mode='auto')
    masked_accs = MaskedAcc((X_val, X_val_1hot, val_sample_weights))

    callbacks = [masked_accs]
    if args.SAVE_CHECKPOINTS:
        callbacks.append(checkpoints)


    # LOAD OR BUILD MODEL:
    if args.LOAD_MODEL:
        # load the model from file:
        if args.LOAD_ARCH_FILE is not None and args.LOAD_WEIGHTS_FILE is not None:
            autoencoder = fn.load_model(args.LOAD_ARCH_FILE, args.LOAD_WEIGHTS_FILE)
        else:
            print("ERROR: either --load-model-arch or --load-model-weights was not specified in command line args. Exiting.")
            sys.exit(1)
    else:
        # build the model:
        autoencoder = build_model(args, idx2v)

    # COMPILE THE MODEL:
    print("compiling the model...")
    autoencoder.compile(loss='categorical_crossentropy',
              optimizer=args.OPTIMIZER,
              sample_weight_mode="temporal",
              metrics=['accuracy'])

    # SAVE MODEL ARCHITECTURE:
    if args.SAVE_MODEL:
        print("saving the model arch...")
        fn.save_model_arch(autoencoder, os.path.split(args.SAVE_ARCH_FILE)[0], args.SAVE_ARCH_FILE)

    # PRINT BATCH AND EPOCH INFO:
    vprint(args, "BATCH_SIZE: %d, N_EPOCHS %d" % (args.BATCH_SIZE, args.N_EPOCHS))

    # TRAIN THE MODEL:
    if args.TRAIN:
        # hist = autoencoder.fit_generator(X_train_gtors, args.EPOCH_SIZE, args.N_EPOCHS, verbose=1, callbacks=callbacks, validation_data=X_val_gtors, nb_val_samples=1000, class_weight={})
        hist = autoencoder.fit_generator(X_train_gtors, args.EPOCH_SIZE, args.N_EPOCHS, verbose=1, callbacks=callbacks, validation_data=(X_val, X_val_1hot, val_sample_weights), class_weight={})

    # SAVE MODEL WEIGHTS:
    if args.SAVE_MODEL:
        fn.save_model_weights(autoencoder, os.path.split(args.SAVE_ARCH_FILE)[0], args.SAVE_WEIGHTS_FILE)

    if args.VERBOSE:
        print("Printing model (autoencoder) training history:")
        print(hist.history)
        print("Printing model (autoencoder) summary:")
        autoencoder.summary()
    vprint(args, "END AUTOENCODER.")


#############################################################
# def split_val_data(args, X_train_gtors):
#     # SPLIT OFF VALIDATION DATA:
#     (X_val, X_val_1hot) = next(X_train_gtors)
#     val_num = 1
#     while val_num < args.VAL_BATCHES:
#         (X_val_,X_val_1hot_) = next(X_train_gtors)
#         X_val = np.concatenate((X_val, X_val_), axis=0)
#         X_val_1hot = np.concatenate((X_val_1hot, X_val_1hot_), axis=0)
#         val_num += 1
#     # print("X_val.shape: " + str(X_val.shape))
#     # print("X_val_1hot.shape: " + str(X_val_1hot.shape))
#     # print("X_val[0]: ")
#     # print(X_val[0])
#     return (X_val, X_val_1hot)

def split_val_data(args, X_val_gtors):
    # SPLIT OFF VALIDATION DATA:
    val_num = 1
    (X_val, X_val_1hot, val_sample_weights) = next(X_val_gtors)
    for (X_val_, X_val_1hot_, val_sample_weights_) in X_val_gtors:
        if val_num >= args.VAL_BATCHES:
            break
        X_val = np.concatenate((X_val, X_val_), axis=0)
        X_val_1hot = np.concatenate((X_val_1hot, X_val_1hot_), axis=0)
        val_sample_weights = np.concatenate((val_sample_weights, val_sample_weights_), axis=0)
        val_num += 1
    print("X_val.shape: " + str(X_val.shape))
    print("X_val_1hot.shape: " + str(X_val_1hot.shape))
    print("val_sample_weights.shape: " + str(val_sample_weights.shape))
    # print("X_val[0]: ")
    # print(X_val[0])
    return (X_val, X_val_1hot, val_sample_weights)


def build_model(args, idx2v):
    """
        args:                       namespace object containing all the commandline arguments that were passed to the program
                                        e.g.    args.VERBOSE, args.MAXLEN_SENT, args.N_BATCHES, args.SENT2VEC_D, etc.
        idx2v:
            datatype:             numpy 2d array
            rows:                   word indexes in the vocabulary, as defined in word2idx_dict
            cols:                    pretrained word vectors (embeddings), as defined in word2v_dict
            purpose:              array mapping each word from its index to its pretrained embedding vector for use in the lstm autoencoder

        Note: In this function variables ending in "_T" are theano TENSORS (dressed up with Keras attributes),
                 while variables ending in "_L" are keras LAYERS
    """
    ########## ENCODER ############
    # INPUT (TENSOR):
    input_idxs_T = Input(shape=(args.MAXLEN_SENT,), dtype='int32', name="input_idxs_T")

    print(input_idxs_T._keras_shape)
    assert input_idxs_T._keras_shape == (None, args.MAXLEN_SENT)
    print(input_idxs_T.dtype)

    # EMBEDDING LAYER:
    """
        layer input:   int array with shape: (N_BATCHES, MAXLEN_SENT)
                            where each row represents a sentence (prepadded w zeros if needed)
                            and each int in a row is the idx corresponding to given word (string) from the vocab, as defined in word2idx_dict
        layer output: tensor with shape: (N_BATCHES, MAXLEN_SENT, SENT2VEC_D)
                            where each row (entry in N_BATCHES) represents a training example
                            and each training exmaple has shape (MAXLEN_SENT, SENT2VEC_D),
                            so it forms a 2d matrix where each row represents one word in the sentence in embedded (vector) form,
                            and the entries along the col of each row are the values along each dimension of the vector embedding
    """
    embedding_L = Embedding(output_dim=args.WORD2VEC_D, input_dim=args.VOCAB_SIZE + 1, input_length=args.MAXLEN_SENT, mask_zero=True, weights=[idx2v], name="embedding_L")

    # EMBEDDING LAYER OUTPUT (TENSOR):
    input_vecs_T = embedding_L(input_idxs_T)

    print(embedding_L.input_shape) # == (N_BATCHES, MAXLEN_SENT)
    print(embedding_L.output_shape) # == (N_BATCHES, MAXLEN_SENT, WORD2VEC_D)
    assert embedding_L.input_shape == (None, args.MAXLEN_SENT)
    assert embedding_L.output_shape == (None, args.MAXLEN_SENT, args.WORD2VEC_D)
    assert embedding_L.output == input_vecs_T

    # LSTM ENCODING LAYER:
    lstm_encoder_L = LSTM(args.SENT2VEC_D, return_sequences=False, name="lstm_encoder_L",  activation="tanh", inner_activation="hard_sigmoid", dropout_W=args.DROPOUT_ELSTM_W, dropout_U=args.DROPOUT_ELSTM_U)
    # LSTM ENCODING LAYER OUTPUT (TENSOR):
    encoded_sent_T = lstm_encoder_L(input_vecs_T)

    print(lstm_encoder_L.output) # == encoded_sent_T
    print(lstm_encoder_L.output_shape) # == (None, SENT2VEC_D)
    print(encoded_sent_T._keras_shape) # == (None, SENT2VEC_D)
    assert lstm_encoder_L.output == encoded_sent_T
    assert lstm_encoder_L.output_shape == (None, args.SENT2VEC_D)
    assert encoded_sent_T._keras_shape == (None, args.SENT2VEC_D)

    # REPEAT LAYER:
    repeat_L = RepeatVector(args.MAXLEN_SENT, name="repeat_L")
    # REPEAT LAYER OUTPUT (TENSOR):
    encoder_output_T = repeat_L(encoded_sent_T)

    print(repeat_L.output)# == encoder_output_T
    print(repeat_L.output_shape)# == (None, MAXLEN_SENT, SENT2VEC_D)
    print(encoder_output_T._keras_shape)# == (None, MAXLEN_SENT, SENT2VEC_D)
    assert repeat_L.output == encoder_output_T
    assert repeat_L.output_shape == (None, args.MAXLEN_SENT, args.SENT2VEC_D)
    assert encoder_output_T._keras_shape == (None, args.MAXLEN_SENT, args.SENT2VEC_D)

    ########## DECODER ############
    if args.DECODER == "twostep":
        print("using twostep decoder...")
        # LSTM DECODING LAYER:
        decoder_lstm_L = LSTM(args.WORD2VEC_D, return_sequences=True, name="decoder_lstm_L",  activation="tanh", inner_activation="hard_sigmoid", dropout_W=args.DROPOUT_DLSTM_W, dropout_U=args.DROPOUT_DLSTM_U)

        # LSTM DECODING LAYER OUTPUT (TENSOR):
        reconstructed_word_vecs_T = decoder_lstm_L(encoder_output_T)

        print(decoder_lstm_L.output)# == reconstructed_word_vecs_T
        print(decoder_lstm_L.output_shape)# == (None, MAXLEN_SENT, WORD2VEC_D)
        print(reconstructed_word_vecs_T._keras_shape)# == (None, MAXLEN_SENT, WORD2VEC_D)
        assert decoder_lstm_L.output == reconstructed_word_vecs_T
        assert decoder_lstm_L.output_shape == (None, args.MAXLEN_SENT, args.WORD2VEC_D)
        assert reconstructed_word_vecs_T._keras_shape == (None, args.MAXLEN_SENT, args.WORD2VEC_D)

        # SOFTMAX LAYER:
        softmax_L = Dense(output_dim=args.VOCAB_SIZE+1, init='glorot_uniform', activation='softmax', W_regularizer=l2(args.SOFTMAX_L2_W), b_regularizer=l2(args.SOFTMAX_L2_B), name="softmax_L")

        # TIME-DISTRIBUTED SOFTMAX LAYER:
        softmax_td_L = TimeDistributed(softmax_L, batch_input_shape=(None, args.MAXLEN_SENT, args.WORD2VEC_D), name="softmax_td_L")

        # TIME-DISTRIBUTED SOFTMAX LAYER OUTPUT (TENSOR):
        predicted_word_idxs_T = softmax_td_L(reconstructed_word_vecs_T)

        print(softmax_td_L.output)# == predicted_word_idxs_T
        print(softmax_td_L.output_shape)# == (None, MAXLEN_SENT, VOCAB_SIZE+1)
        print(predicted_word_idxs_T._keras_shape)# == (None, MAXLEN_SENT, VOCAB_SIZE+1)
        assert softmax_td_L.output == predicted_word_idxs_T
        assert softmax_td_L.output_shape == (None, args.MAXLEN_SENT, args.VOCAB_SIZE+1)
        assert predicted_word_idxs_T._keras_shape == (None, args.MAXLEN_SENT, args.VOCAB_SIZE+1)

    elif args.DECODER == "softmax":
        print("using (onestep) softmax decoder...")
        # LSTM DECODING LAYER:
        decoder_lstm_L = LSTM(args.VOCAB_SIZE+1, return_sequences=True, name="decoder_lstm_L", activation="softmax", inner_activation="hard_sigmoid",  dropout_W=args.DROPOUT_DLSTM_W, dropout_U=args.DROPOUT_DLSTM_U)
        # LSTM DECODING LAYER OUTPUT (TENSOR):
        predicted_word_idxs_T = decoder_lstm_L(encoder_output_T)
        print(decoder_lstm_L.output)# == predicted_word_idxs_T
        print(decoder_lstm_L.output_shape)# == (None, MAXLEN_SENT, VOCAB_SIZE+1)
        print(predicted_word_idxs_T._keras_shape)# == (None, MAXLEN_SENT, VOCAB_SIZE+1)
        assert decoder_lstm_L.output == predicted_word_idxs_T
        assert decoder_lstm_L.output_shape == (None, args.MAXLEN_SENT, args.VOCAB_SIZE+1)
        assert predicted_word_idxs_T._keras_shape == (None, args.MAXLEN_SENT, args.VOCAB_SIZE+1)

    # INSTANTIATE THE MODEL:
    print("instantiating the model...")
    autoencoder = Model(input=input_idxs_T, output=predicted_word_idxs_T)
    return autoencoder

def load_vocab(args):
    vprint(args, "Loading necessary vocab related data structures from disk...")
    vprint(args, "\tloading: idx2w (python list)...")
    with open(args.IDX2W_FILE, "rb") as f:
        idx2w = pickle.load(f)
    vprint(args, "\tloading: w2idx (python dict)...")
    with open(args.W2IDX_FILE, "rb") as f:
        w2idx = pickle.load(f)
    vprint(args, "\tloading: w2v (python dict)...")
    with open(args.W2V_FILE, "rb") as f:
        w2v = pickle.load(f)
    vprint(args, "\tloading: vocab_list (python list)...")
    with open(args.VOCAB_FILE, "rb") as f:
        vocab_list = pickle.load(f)
    vprint(args, "\tloading: idx2v (numpy array)...")
    idx2v = np.load(args.IDX2V_FILE)
    idx2v = idx2v[0:args.VOCAB_SIZE+1,:]
    vprint(args, "\tidx2v.shape is:")
    vprint(args, idx2v.shape)
    vprint(args, "DONE loading necessary vocab related data structures from disk...")
    assert idx2v.shape == (args.VOCAB_SIZE+1, args.WORD2VEC_D)
    return idx2w, w2idx, w2v, vocab_list, idx2v

def vprint(args, string, fillers=None):
    """
        args: cmdln args object
        str: string to print
    """
    if args.VERBOSE == True:
        if fillers is not None:
            print(string % fillers)
        else:
            print(string)

def prep_cmdln_parser():
    usage = "usage: %prog [options]"
    cmdln = argparse.ArgumentParser(usage)
    cmdln.add_argument("--verbose", action="store_true", dest="VERBOSE", default=False,
                                help="print out more information during runtime [default: %default.")
    cmdln.add_argument("--gpu", action="store_true", dest="GPU", default=False,
                                help="train the model on a GPU (if supported) [default: %default].")
    cmdln.add_argument("--train", action="store_true", dest="TRAIN", default=False,
                                help="train the model [default: %default]. Note, if this set then --training-data=FILE must also be be specified.")
    cmdln.add_argument("--training-data", action="store", dest="TRAINING_DATA_FILE", default="",
                                help="load the training data from specified file [default: %default]. Note, this must be specified in order to train the model.")
    cmdln.add_argument("--val-data", action="store", dest="VAL_DATA_FILE", default="",
                                help="load the validation data from specified file [default: %default]. Note, this must be specified in order to train the model.")
    cmdln.add_argument("--test", action="store_true", dest="TEST", default=False,
                                help="test the model [default: %default]. Note, if this set then --test-data=FILE must also be be specified.")
    cmdln.add_argument("--test-data", action="store", dest="TEST_DATA_FILE", default="",
                                help="load the training data from specified file [default: %default]. Note, this must be specified in order to test the model.")
    cmdln.add_argument("--load-model", action="store_true", dest="LOAD_MODEL", default=False,
                                help="load the model architecture and weights from disk [default: %default]. Note, if this is set then --load-model-arch=FILE and --load-model-weights=FILE must also be set.")
    cmdln.add_argument("--load-model-arch", action="store", dest="LOAD_ARCH_FILE", default="",
                                help="load the model architecture from specified file [default: %default]. Note, --load-model and --load-model-weights=FILE must also be set to actually load the model.")
    cmdln.add_argument("--load-model-weights", action="store", dest="LOAD_WEIGHTS_FILE", default="",
                                help="load the model weights from specified file [default: %default]. Note, --load-model and --load-model-arch=FILE must also be set to actually load the model.")
    cmdln.add_argument("--save-model", action="store_true", dest="SAVE_MODEL", default=False,
                                help="save the model architecture (before training) and weights (after training) to disk [default: %default]. Note, if this is set then --save-model-arch=FILE and --save-model-weights=FILE must also be set.")
    cmdln.add_argument("--save-model-arch", action="store", dest="SAVE_ARCH_FILE", default="",
                                help="save the model architecture (before training) to specified file [default: %default]. Note, --save-model must also be set to actually save the model architecture.")
    cmdln.add_argument("--save-model-weights", action="store", dest="SAVE_WEIGHTS_FILE", default="",
                                help="save the model weights (after training) to specified file [default: %default]. Note, --save-model must also be set to actually save the model weights.")
    cmdln.add_argument("--save-checkpoints", action="store_true", dest="SAVE_CHECKPOINTS", default=False,
                                help="save the model weights to disk at the end of each training epoch [default: %default]. Note, if this is set then --save-checkpoints-weights=FILE must also be set.")
    cmdln.add_argument("--save-checkpoints-weights", action="store", dest="CHECKPOINTS_PATH", default="",
                                help="save the model weights (during training) to specified file at the end of epoch of training  [default: %default]. Note, --save-checkpoints must also be set to actually save the model weights during training. Also note, the filename should have the following format to provide information about each checkpoint file and to ensure they don't overwrite each other: <name>.weights.{epoch:02d}-{val_acc:.2f}.hdf5")
    cmdln.add_argument("--word2v-d", action="store", dest="WORD2VEC_D", default=300, type=int,
                                help="the dimension to use for the word embedding layear of the model [default: %default]. Note, this must match the dimensionality of the index2vector matrix and word2vector dictionary that are passed to the program as well.")
    cmdln.add_argument("--sent2v-d", action="store", dest="SENT2VEC_D", default=256, type=int,
                                help="the dimension to use for the sentence embedding layear of the model [default: %default]. Note, if loading the model from file, this must match the dimensionality of the sentence embedding used in the loaded model.")
    cmdln.add_argument("--vocab-size", action="store", dest="VOCAB_SIZE", default=10000, type=int,
                                help="the size of the vocabulary being used [default: %default]. Note, this must match the size of the vocab dictionary that is being passed to the program.")
    cmdln.add_argument("--batch-size", action="store", dest="BATCH_SIZE", default=64, type=int,
                                help="the batch size to use while training the model [default: %default].")
    cmdln.add_argument("--n-epochs", action="store", dest="N_EPOCHS", default=20, type=int,
                                help="the number of epochs to train the model for [default: %default].")
    cmdln.add_argument("--epoch-size", action="store", dest="EPOCH_SIZE", default=6400, type=int,
                                help="the model uses an infinite generator as its data source (meaning it continually loops over the training data), so this specifies the number of training examples that should be seen before 1 epoch is considered to have passed [default: %default]. Note, this must be a multiple of the batch size.")
    cmdln.add_argument("--maxlen-sent", action="store", dest="MAXLEN_SENT", default=20, type=int,
                                help="the maximum length of a sentence (number of words). All longer sentences will be (front-end) cropped to this length and all shorter sentences will be (front-end) padded (using masking) to this length to allow uniform training of the network [default: %default].")
    cmdln.add_argument("--decoder", action="store", dest="DECODER", default="twostep",
                            help="the type of decoder to use for the autoencoder model...options: 'softmax'=LSTM decoder outputs directly to a 1hot vector using softmax activation, or 'two_step'=LSTM decoder outputs to an intermediary vector of WORD2VEC_D dimensionality (default=300) which is then passed through a Dense softmax activation layer to make predictions [default: %default].")
    cmdln.add_argument("--optimizer", action="store", dest="OPTIMIZER", default="adam",
                            help="the optimizer to use for the autoencoder model (sgd, rmsprop, adagrad, adadelta, adamax, adam, etc.) [default: %default].")
    cmdln.add_argument("--lr-start", action="store", dest="LR_START", default=0.001, type=float,
                            help="the starting learning rate to use while training the autoencoder model [default: %default].")
    cmdln.add_argument("--lr-end", action="store", dest="LR_END", default=0.001, type=float,
                            help="the ending learning rate to use while training the autoencoder model [default: %default].")
    cmdln.add_argument("--lr-hyp", action="store", dest="LR_HYP", default=0.0, type=float,
                            help="hyper parameter related to controlling the learning rate used while training the autoencoder model (e.g. learning rate decay, etc. Note, what this stands for exactly depends on the optimzer being used) [default: %default].")
    # cmdln.add_argument("--dropout-dense", action="store", dest="DROPOUT_DENSE", default=0.0, type=float,
    #                         help="the dropout rate to use for the final dense layer(s) of the autoencoder model [default: %default].")
    cmdln.add_argument("--dropout-elstm-w", action="store", dest="DROPOUT_ELSTM_W", default=0.0, type=float,
                                help="the dropout rate to use for the input gates of the ENCODER lstm layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--dropout-elstm-u", action="store", dest="DROPOUT_ELSTM_U", default=0.0, type=float,
                                help="the dropout rate to use for the recurrent connections of the ENCODER lstm layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--dropout-dlstm-w", action="store", dest="DROPOUT_DLSTM_W", default=0.0, type=float,
                                help="the dropout rate to use for the input gates of the DECODER lstm layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--dropout-dlstm-u", action="store", dest="DROPOUT_DLSTM_U", default=0.0, type=float,
                                help="the dropout rate to use for the recurrent connections of the DECODER lstm layer(s) of the classifier model [default: %default].")
    cmdln.add_argument("--softmax-l2-w", action="store", dest="SOFTMAX_L2_W", default=0.001, type=float,
                                help="the L2 regularization strength to use for the weights on the softmax layer of the decoder model  [default: %default].")
    cmdln.add_argument("--softmax-l2-b", action="store", dest="SOFTMAX_L2_B", default=0.001, type=float,
                                help="the L2 regularization strength to use for the bias on the softmax layer of the decoder model  [default: %default].")
    cmdln.add_argument("--val-batches", action="store", dest="VAL_BATCHES", default=20, type=int,
                                help="the number of batches of the training data to set aside for validation at the end of each epoch. The model is not trained on this data. Note, all validation data is pulled from the front of the training data, so training data should be shuffled in advance. [default: %default].")
    cmdln.add_argument("--gtor-lim", action="store", dest="GTOR_LIM", default=-1, type=int,
                                help="the limit at which to cap the generator used for training data (in other words, if this is set to 10000, then only the first 10000 sentences will be used as training data...default of -1 uses all available data) [default: %default].")
    cmdln.add_argument("--idx2v", action="store", dest="IDX2V_FILE", default="", required=True,
                                help="load the index2vector numpy weights matrix (.npy file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--idx2w", action="store", dest="IDX2W_FILE", default="", required=True,
                                help="load the index2word python list (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--w2idx", action="store", dest="W2IDX_FILE", default="", required=True,
                                help="load the word2index python dict (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--w2v", action="store", dest="W2V_FILE", default="", required=True,
                                help="load the word2vector python dict (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--vocab-list", action="store", dest="VOCAB_FILE", default="", required=True,
                                help="load the vocab python list of tuples (.pickle file) for the specific vocabulary [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--expID", action="store", dest="EXPID", default="",
                                help="name or ID of the experiment [default: %default].")
    return cmdln


if __name__ == "__main__":
    main()

