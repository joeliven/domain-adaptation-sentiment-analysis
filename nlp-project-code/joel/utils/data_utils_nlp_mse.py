import numpy as np
import numpy.random
import scipy as sci

import gensim
from gensim.models import Word2Vec
import nltk

from keras.preprocessing import sequence
from keras.utils import np_utils

import os
import pickle
import json
import gzip

from collections import deque
import itertools
from operator import itemgetter

SENT_START = "S_START"
SENT_END = "S_END"
UNK = "UNK"
MASK = "MASK"

IDX2W = "_idx2w.list"
IDX2V = "_idx2v" # numpy format
W2IDX = "_w2idx.dict"
W2V = "_w2v.dict"
VOCABL = "_vocab.list"
#############################################3
# START AMAZON DATASET UTILS
#############################################3
# Refnote: http://jmcauley.ucsd.edu/data/amazon/links.html
# Reading the data: Data can be treated as python dictionary objects. A simple script to read any of the above the data is as follows:
def amazon_load_ctg_from_gzip_to_dict(path):
    g = gzip.open(path, 'r')
    for review in g:
        yield eval(review)

def amazon_load_dict_from_json(path, GTOR_LIM=-1):
    f = open(path, 'r')
    if GTOR_LIM > -1:
        ct = 0
        for review in f:
            if ct >= GTOR_LIM:
                raise StopIteration
            ct += 1
            yield eval(review)
    else:
        for review in f:
            yield eval(review)

def amazon_load_gtor(path, by_review=True, GTOR_LIM=-1):
    if by_review:
        review_gtor = amazon_load_dict_from_json(path, GTOR_LIM=GTOR_LIM)
        return review_gtor
    else:
        review_gtor = amazon_load_dict_from_json(path, GTOR_LIM=-1)
        return amazon_load_gtor_sentence(review_gtor, GTOR_LIM)

def amazon_load_gtor_sentence(reviews_gtor, GTOR_LIM=-1):
    ct = 0
    sents_stack = deque()
    review = next(reviews_gtor, None)
    if review is not None:
        sents_stack.extend(nltk.sent_tokenize(review["reviewText"]))
    while(review is not None):
        while len(sents_stack) > 0:
            if GTOR_LIM > -1 and ct >= GTOR_LIM:
                raise StopIteration
            ct += 1
            # print("ct:%d" % ct)
            yield sents_stack.popleft()
        review = next(reviews_gtor, None)
        if review is not None:
            sents_stack.extend(nltk.sent_tokenize(review["reviewText"]))


# Refnote: http://jmcauley.ucsd.edu/data/amazon/links.html
# Convert to 'strict' json
# The above data can be read with python 'eval', but is not strict json.
# If you'd like to use some language other than python, you can convert the data to strict json as follows:
def amazon_load_ctg_to_json_helper(path):
  g = gzip.open(path, 'r')
  for l in g:
    yield json.dumps(eval(l))

def amazon_load_ctg_to_json(infile, outfile):
    f = open(outfile, 'w')
    for l in amazon_load_ctg_to_json_helper(infile):
      f.write(l + '\n')

def amazon_split_ctg_by_rating(reviews, outpath):
    """    splits the reviews for a given catetory by rating
            (negative = 1 or 2, neutral = 3, positive = 4 or 5)
            and writes these back out to file, only keeping the following from the dict:
                title
                reviewText
                overall
            saves the info back out to outfile in json format
    """
    outpath = outpath.split(".json")[0]
    out_neg = outpath + "_neg.json"
    print(out_neg)
    out_ntr = outpath + "_ntr.json"
    print(out_ntr)
    out_pos = outpath + "_pos.json"
    print(out_pos)
    ct = 0
    ct_neg = 0
    ct_ntr = 0
    ct_pos = 0
    with open(out_neg, "w") as fout_neg, open(out_ntr, "w") as fout_ntr, open(out_pos, "w") as fout_pos:
        for review in reviews:
            if ct > 10000:
                break
            if ct % 1000 == 0:
                print("ct: " + str(ct) + "\tneg: " + str(ct_neg) + "\tntr: " + str(ct_ntr) + "\tpos: " + str(ct_pos) )
            ct +=1
            out_dict = dict()
            out_dict["overall"] = review["overall"]
            out_dict["helpful"] = review["helpful"]
            out_dict["summary"] = review["summary"]
            out_dict["reviewText"] = review["reviewText"]
            if float(review["overall"]) < 3.0:
                json.dump(out_dict, fout_neg)
                fout_neg.write("\n")
                ct_neg += 1
            elif float(review["overall"]) > 3.0:
                json.dump(out_dict, fout_pos)
                fout_pos.write("\n")
                ct_pos += 1
            else:
                json.dump(out_dict, fout_ntr)
                fout_ntr.write("\n")
                ct_ntr += 1
        print("done:")
        print("ct: " + str(ct) + "\tneg: " + str(ct_neg) + "\tntr: " + str(ct_ntr) + "\tpos: " + str(ct_pos) )
    return ct, ct_neg, ct_ntr, ct_pos


def safe_get_embedding(vsm, w, defaults):
    if w in defaults:
        # print("in defaults")
        return defaults[w]
    try:
        embedding = vsm[w]
        return embedding
    except KeyError:
        if w in defaults:
            return defaults[w]
        else:
            return defaults["UNK"]

def google_news_load_vsm(path):
    if path == None:
        print("Error: path must be specified...exiting.")
        sys.exit(1)
    print("loading pre-trained word2vec model from binary from path:\n\t%s" % (str(path)))
    vsm = Word2Vec.load_word2vec_format(path, binary=True)  # C binary format
    print("FINISHED loading pre-trained word2vec model from binary from path:\n\t%s" % (str(path)))
    return vsm

# Refnote: https://github.com/fchollet/keras/issues/853
def create_idx2v_array(word2idx_dict, word2v_dict, word2v_d, vocab_size):
    """
    word2idx_dict: python dict
            keys (strings):    all the unique words in vocab
            values (ints):      indices from 1 to n_symbols
            purpose:            maps all the words in your dictionary to indices from 1 to n_symbols  (0 is reserved for the masking)
            example:
                                    {
                                     'book': 1,
                                     'movie': 2,
                                     'awful': 3,
                                     ...
                                     'plot': 10,000,
                                    }
    word2v_dict: python dict
            keys (strings):   all the unique words in vocab
            values (numpy arrays): the pretrained word2vec word embedding for each unique word in the vocab
            purpose:            maps all the words in your dictionary to vectors of dimension word2vec_dim
            example:
                                    {
                                     'book': array([0.1,0.5,...,0.7]),
                                     'movie': array([0.2,1.2,...,0.9]),
                                    ...
                                    }
    word2v_d: int
            purpose:            dimensionality of the word embeddings
    vocab_size: int
            purpose:            number of uniques words in the vocabulary of the dataset

    returns:
    widx2v_array:
        datatype:             numpy 2d array
        rows:                   word indexes in the vocabulary, as defined in word2idx_dict
        cols:                    pretrained word vectors (embeddings), as defined in word2v_dict
        purpose:              array mapping each word from its index to its pretrained embedding vector for use in the lstm autencoder

    """
    n_symbols = vocab_size + 1 # adding 1 to account for 0th index (for masking)
    widx2v_array = np.zeros((n_symbols,word2v_d))
    for word,idx in word2idx_dict.items():
        widx2v_array[idx,:] = word2v_dict[word]
    return widx2v_array


# # Refnote: http://www.wildml.com/2015/09/recurrent-neural-networks-tutorial-part-2-implementing-a-language-model-rnn-with-python-numpy-and-theano/
# # Refnote: https://github.com/dennybritz/rnn-tutorial-rnnlm


def save_list(fpath, list):
    with open(fpath, "w") as f:
        for item in list:
            f.write(item + "\n")

def load_list(fpath, dtype):
    lst = list()
    with open(fpath, "r") as f:
        for line in f:
            if dtype == "string":
                lst.append(line.strip())
            elif dtype == "int":
                lst.append(int(line.strip()))
            elif dtype == "float":
                lst.append(float(line.strip()))
    return lst

def save_list_of_tups(fpath, list):
    with open(fpath, "w") as f:
        for item in list:
            f.write(str(item[1]) + ":" + item[0] + "\n")

def load_list_of_tups(fpath):
    lst = list()
    with open(fpath, "r") as f:
        for line in f:
                item = line.strip().split()
                lst.append( ( str(item[1]), int(item[0]) ) )
    return lst


def save_dict_json(fpath, dct):
    with open(fpath, "w") as f:
        json.dump(dct, f)

def load_dict_json(fpath, keytype):
    dct = dict()
    with open(fpath, "r") as f:
        dct = json.load(f)
    if keytype == "int":
        # Refnote: http://stackoverflow.com/questions/21193682/convert-a-string-key-to-int-in-a-dictionary
        dct = {int(k):v for k,v in dct.items()}
    return dct

def amazon_preprocess_datasets(S_PATH, T_PATH, SAVE_DIR, SAVE_NAME, VSM_PATH, WORD2VEC_D=300, VOCAB_SIZE=10000, REMOVE_STOPS=False, STOPLIST_PATH=None, DATA_LIM=None, BRIEF=True, TO_LOWER=True):
    if BRIEF:
        amazon_preprocess_datasets_brief(S_PATH, T_PATH, SAVE_DIR, SAVE_NAME, VSM_PATH, WORD2VEC_D=WORD2VEC_D, VOCAB_SIZE=VOCAB_SIZE, REMOVE_STOPS=REMOVE_STOPS, STOPLIST_PATH=STOPLIST_PATH, DATA_LIM=DATA_LIM, TO_LOWER=TO_LOWER)
    else:
        amazon_preprocess_datasets_verbose(S_PATH, T_PATH, SAVE_DIR, SAVE_NAME, VSM_PATH, WORD2VEC_D=WORD2VEC_D, VOCAB_SIZE=VOCAB_SIZE, REMOVE_STOPS=REMOVE_STOPS, STOPLIST_PATH=STOPLIST_PATH, DATA_LIM=DATA_LIM, TO_LOWER=TO_LOWER)


def amazon_preprocess_datasets_verbose(S_PATH, T_PATH, SAVE_DIR, SAVE_NAME, VSM_PATH, WORD2VEC_D=300, VOCAB_SIZE=10000, REMOVE_STOPS=False, STOPLIST_PATH=None, DATA_LIM=None, TO_LOWER=True):
    # DATA STRUCTURES THAT WE NEED TO BUILD OUT TO THEN SAVE TO FILE:
    """
        S_PATH:         the path of the Source data set
        T_PATH:         the path the Target data set
        SAVE_DIR:    the directory in which all generated files (data structures for this combined vocab) should be saved
        SAVE_NAME:  the name to be prepended to the generated files (data structures for this combined vocab)
    """
    """
        These are the data structures created in this function, then saved to file for easy use during training:
            idx2word_array:     python list of words, sorted by frequency where index 0 is reserved for MASKING in LSTM
                                         and index 1 is the most frequenlty occuring word, and index VOCAB_SIZE is the least frequently occuring word
            word2idx_dict:       python dict, keys are words in vocab, values are indexes
            word2v_dict:          python dict, keys are words in vocab, values are indexes word-embeddings
                                         (e.g. WORD2VEC_D dimensional float vectors)
            idx2v_array:           numpy 1d array where each rownum corresponds to the index of a word
                                         and entries along a row are the values of the word-embedding
            vocab_list:             python list of tuples of the form: (word, ct) sorted by the top
    """
    if DATA_LIM == -1:
        DATA_LIM = None
    print("START Preprocessing data (verbose)...")
    if os.path.exists(SAVE_DIR) and os.path.isdir(SAVE_DIR):
        save_path = os.path.join(SAVE_DIR, SAVE_NAME)
    elif not os.path.exists(SAVE_DIR):
        print("making dir: %s" % SAVE_DIR)
        os.mkdir(SAVE_DIR)
        save_path = os.path.join(SAVE_DIR, SAVE_NAME)
    else:
        print("SAVE_DIR: %s is a file, changing SAVE_DIR name to avoid overwriting the file" % SAVE_DIR)
        SAVE_DIR += "01"
        save_path = os.path.join(SAVE_DIR, SAVE_NAME)
    f_meta = open( save_path + ".meta", "w")
    f_meta.write("Preprocess_mode:verbose\n")
    if DATA_LIM is not None:
        f_meta.write("DATA_LIM:%d\n" % DATA_LIM)
        print("DATA_LIM:%d" % DATA_LIM)
    else:
        f_meta.write("DATA_LIM:None\n")
        print("DATA_LIM:None")
    if TO_LOWER:
        f_meta.write("TO_LOWER:True\n")
        print("TO_LOWER:True")
    else:
        f_meta.write("TO_LOWER:False\n")
        print("TO_LOWER:False")
    f_meta.write("Preprocess_mode:verbose\n")
    f_meta.write("SAVE_DIR:%s\n" % (SAVE_DIR))
    f_meta.write("save_path:%s\n" % (save_path))

    # PREPARE STOPLIST:
    if REMOVE_STOPS == True:
        print("Preparing stoplist...")
        stoplist = set()
        if STOPLIST_PATH == None:
            stoplist = set({'a', 'an', 'the', 'as', 'of', 'at', 'for', 'with', 'to', 'from', '.', ','})
        else:
            with open(STOPLIST_PATH, "r") as stoplist_f:
                for line in stoplist_f:
                    stoplist.add(str(line.strip().lower()))
        f_meta.write("remove_stopwords:")
        for word in stoplist:
            f_meta.write(word + ",")
        f_meta.write("\n")
    else:
        print("Not removing any stopwords...")
        f_meta.write("remove_stopwords:\n")

    # GET GENERATORS FOR DATASETS:
    print("Getting generators for:\n\tSource:\t%s \n\tTarget:\t%s" % (S_PATH, T_PATH) )
    f_meta.write("Source:%s\nTarget:%s\n" % (S_PATH, T_PATH))

    S = amazon_load_dict_from_json(S_PATH)
    T = amazon_load_dict_from_json(T_PATH)
    S_T = [S,T]

    review_lens = list()
    sent_lens = list()
    tot_words = 0
    # CREATE THE COMBINED VOCAB:
    print("Creating the combined vocab from S and T...")
    vocab = dict()
    for Dnum,D in enumerate(S_T):
        rnum = 0
        for review in D:
            rnum += 1
            # tokenize the review into sentences:
            if DATA_LIM is not None and rnum > DATA_LIM:
                break
            if rnum % 10000 ==0:
                print("Data set: %d\tprocessed %d reviews" % (Dnum,rnum))
            if TO_LOWER:
                review_stokenized = nltk.sent_tokenize(review["reviewText"].lower())
            else:
                review_stokenized = nltk.sent_tokenize(review["reviewText"])
            review_lens.append(len(review_stokenized))
            review_wtokenized = [nltk.word_tokenize(sent) for sent in review_stokenized]
            sent_lens += [len(sent) for sent in review_wtokenized]
            words_in_review__ = itertools.chain(*[sent for sent in review_wtokenized])
            words_in_review, words_in_review_ = itertools.tee(words_in_review__)
            words_in_review_ = list(words_in_review_)
            tot_words += len(words_in_review_)
            if REMOVE_STOPS:
                for word in words_in_review:
                    if word not in stoplist:
                        vocab[word] = vocab.get(word, 0) + 1
            else:
                for word in words_in_review:
                    vocab[word] = vocab.get(word, 0) + 1
    print("Creating sorted vocab_list from combined S and T vocab dict...")
    # Refnote: http://stackoverflow.com/questions/1915564/python-convert-a-dictionary-to-a-sorted-list-by-value-instead-of-key
    vocab_list =  sorted(iter(vocab.items()), key=itemgetter(1), reverse=True)
    review_lens = np.asanyarray(review_lens)
    sent_lens = np.asanyarray(sent_lens)
    review_max = np.max(review_lens)
    sent_max = np.max(sent_lens)
    review_mean = np.mean(review_lens)
    sent_mean = np.mean(sent_lens)
    review_std = np.std(review_lens)
    sent_std = np.std(sent_lens)
    review_maxlen_ideal = review_mean + 2*review_std
    sent_maxlen_ideal = sent_mean + 2*sent_std
    print("Tokenized %d reviews." % (review_lens.shape[0]) )
    f_meta.write("reviews_tokenized:\t%d\n" % (review_lens.shape[0]) )
    print( "Tokenized %d sentences." % (sent_lens.shape[0]) )
    f_meta.write( "sents_tokenized:\t%d\n" % (sent_lens.shape[0]) )

    print( "Tokenized %d words." % (tot_words) )
    f_meta.write( "words_tokenized:\t%d\n" % (tot_words) )

    print( "Combined vocab of S and T contains %d unique tokens." % (len(vocab)) )
    f_meta.write( "vocab_size_all:\t%d\n" % (len(vocab)) )

    if len(vocab_list) < VOCAB_SIZE:
        VOCAB_SIZE = len(vocab_list)
    print ("Using vocabulary size %d." % VOCAB_SIZE)
    f_meta.write("vocab_size:\t%d\n" % VOCAB_SIZE)
    vocab_list = vocab_list[0:VOCAB_SIZE-1] # -1 is to account for having to add the UNK token to the vocab
    assert len(vocab_list) == VOCAB_SIZE-1
    print ("Most frequent word in vocab is '%s' and appeared %d times." % (vocab_list[0][0], vocab_list[0][1]) )
    f_meta.write("vocab_most_freq:\t%s,%d\n" % (vocab_list[0][0], vocab_list[0][1]) )

    print ("The least frequent word in our vocabulary is '%s' and appeared %d times." % (vocab_list[-1][0], vocab_list[-1][1]) )
    f_meta.write("vocab_least_freq:\t%s,%d\n" % (vocab_list[-1][0], vocab_list[-1][1]) )

    print ("The mean sentence length in our vocabulary is %f words long with std %f." % (sent_mean, sent_std ))
    f_meta.write("sent_mean:\t%f\n" % (sent_mean))
    f_meta.write("sent_std:\t%f\n" % ( sent_std ))
    print ("The longest sentence in our vocabulary is %d words long." % (sent_max ))
    f_meta.write("sent_max:\t%d\n" % (sent_max ))
    print ("The ideal max sentence length for our vocabulary is %f." % (sent_maxlen_ideal ))
    f_meta.write("sent_maxlen_ideal:\t%f\n" % (sent_maxlen_ideal ))
    print ("The mean review length in our vocabulary is %f sentences long with std %f." % (review_mean, review_std ))
    f_meta.write("review_mean:\t%f\n" % (review_mean ))
    f_meta.write("review_std\t:%f\n" % (review_std ))
    print ("The longest review in our vocabulary is %d sentences long." % (review_max ))
    f_meta.write("review_max:\t%d\n" % (review_max ))
    print ("The ideal max review length for our vocabulary is %d." % (review_maxlen_ideal ))
    f_meta.write("review_maxlen_ideal:\t%f\n" % (review_maxlen_ideal ))

    print("Creating idx2word_array of the combined vocab from S and T...")
    idx2word_array = [word_ct[0] for word_ct in vocab_list] # vocab is a list of tuples: [('word1',freqct),('word2',freqct),('word3',freqct),...]
    idx2word_array.insert(0, UNK)
    idx2word_array.insert(0, MASK)
    print("Creating word2idx_dict of the combined vocab from S and T...")
    word2idx_dict = dict([(w,i) for i,w in enumerate(idx2word_array)])
    SENT_START_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    SENT_END_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    UNK_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    MASK_EMBEDDING = np.zeros((WORD2VEC_D,))
    defaults = {SENT_START: SENT_START_EMBEDDING, SENT_END: SENT_END_EMBEDDING, UNK: UNK_EMBEDDING, MASK: MASK_EMBEDDING}
    vsm = google_news_load_vsm(VSM_PATH)
    print("Creating word2v_dict of the combined vocab from S and T...")
    word2v_dict = dict( [ (w, safe_get_embedding(vsm, w, defaults)) for i,w in enumerate(idx2word_array)])
    print("Creating widx2v_array of the combined vocab from S and T...")
    idx2v_array = create_idx2v_array(word2idx_dict, word2v_dict, WORD2VEC_D, VOCAB_SIZE)

    print("Saving data structures to save_path:\n\t%s\n..." % (save_path))

    # SAVE idx2word_array TO FILE:
    fpath = save_path + IDX2W
    save_list(fpath, idx2word_array)
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(idx2word_array, f)

    # SAVE word2idx_dict TO FILE:
    fpath = save_path + W2IDX
    save_dict_json(fpath, word2idx_dict)
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(word2idx_dict, f)

    # SAVE word2v_dict TO FILE:
    fpath = save_path + W2V
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(word2v_dict, f)

    # SAVE idx2v_array (numpy array) TO FILE:
    fpath = save_path + IDX2V
    np.save(fpath, idx2v_array)
    # np.savetxt(fpath, idx2v_array)

    # SAVE vocab_list (list of word_ct tuples) TO FILE:
    fpath = save_path + VOCABL
    save_list_of_tups(fpath, vocab_list)
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(vocab_list, f)

    f_meta.close()
    print("END Preprocessing data...")



def amazon_preprocess_datasets_brief(S_PATH, T_PATH, SAVE_DIR, SAVE_NAME, VSM_PATH, WORD2VEC_D=300, VOCAB_SIZE=10000, REMOVE_STOPS=False, STOPLIST_PATH=None, DATA_LIM=None, TO_LOWER=True):
    # DATA STRUCTURES THAT WE NEED TO BUILD OUT TO THEN SAVE TO FILE:
    """
        S_PATH:         the path of the Source data set
        T_PATH:         the path the Target data set
        SAVE_DIR:    the directory in which all generated files (data structures for this combined vocab) should be saved
        SAVE_NAME:  the name to be prepended to the
    """
    """
        These are the data structures created in this function, then saved to file for easy use during training:
            idx2word_array:     python list of words, sorted by frequency where index 0 is reserved for MASKING in LSTM
                                         and index 1 is the most frequenlty occuring word, and index VOCAB_SIZE is the least frequently occuring word
            word2idx_dict:       python dict, keys are words in vocab, values are indexes
            word2v_dict:          python dict, keys are words in vocab, values are indexes word-embeddings
                                         (e.g. WORD2VEC_D dimensional float vectors)
            idx2v_array:           numpy 1d array where each rownum corresponds to the index of a word
                                         and entries along a row are the values of the word-embedding
            vocab_list:             python list of tuples of the form: (word, ct) sorted by the top
    """
    print("START Preprocessing data (brief)...")
    if os.path.exists(SAVE_DIR) and os.path.isdir(SAVE_DIR):
        save_path = os.path.join(SAVE_DIR, SAVE_NAME)
    elif not os.path.exists(SAVE_DIR):
        print("making dir: %s" % SAVE_DIR)
        os.mkdir(SAVE_DIR)
        save_path = os.path.join(SAVE_DIR, SAVE_NAME)
    else:
        print("SAVE_DIR: %s is a file, changing SAVE_DIR name to avoid overwriting the file" % SAVE_DIR)
        SAVE_DIR += "01"
        save_path = os.path.join(SAVE_DIR, SAVE_NAME)
    f_meta = open( save_path + ".meta", "w")
    f_meta.write("Preprocess_mode:brief\n")
    if DATA_LIM is not None:
        f_meta.write("DATA_LIM:%d\n" % DATA_LIM)
        print("DATA_LIM:%d" % DATA_LIM)
    else:
        f_meta.write("DATA_LIM:None\n")
        print("DATA_LIM:None")
    if TO_LOWER:
        f_meta.write("TO_LOWER:True\n")
        print("TO_LOWER:True")
    else:
        f_meta.write("TO_LOWER:False\n")
        print("TO_LOWER:False")
    f_meta.write("SAVE_DIR:%s\n" % (SAVE_DIR))
    f_meta.write("save_path:%s\n" % (save_path))

    # PREPARE STOPLIST:
    if REMOVE_STOPS == True:
        print("Prepareing stoplist...")
        stoplist = set()
        if STOPLIST_PATH == None:
            stoplist = set({'a', 'an', 'the', 'as', 'of', 'at', 'for', 'with', 'to', 'from', '.', ','})
        else:
            with open(STOPLIST_PATH, "r") as stoplist_f:
                for line in stoplist_f:
                    stoplist.add(str(line.strip().lower()))
        f_meta.write("remove_stopwords:")
        for word in stoplist:
            f_meta.write(word + ",")
        f_meta.write("\n")
    else:
        print("Not removing any stopwords...")
        f_meta.write("remove_stopwords:\n")

    # GET GENERATORS FOR DATASETS:
    print("Getting generators for:\n\tSource:\t%s \n\tTarget:\t%s" % (S_PATH, T_PATH) )
    f_meta.write("Source:%s\nTarget:%s\n" % (S_PATH, T_PATH))

    S = amazon_load_dict_from_json(S_PATH)
    T = amazon_load_dict_from_json(T_PATH)
    S_T = [S,T]

    review_lens = list()
    tot_words = 0
    # CREATE THE COMBINED VOCAB:
    print("Creating the combined vocab from S and T...")
    vocab = dict()
    rnum = 0
    for Dnum,D in enumerate(S_T):
        rnum = 0
        for review in D:
            rnum += 1
            # tokenize the review into sentences:
            if DATA_LIM is not None and rnum > DATA_LIM:
                break
            if rnum % 10000 ==0:
                print("Data set: %d\tprocessed %d reviews" % (Dnum,rnum))
            if TO_LOWER:
                words_in_review = nltk.word_tokenize(review["reviewText"].lower())
            else:
                words_in_review = nltk.word_tokenize(review["reviewText"])
            review_lens.append(len(words_in_review))
            tot_words += len(words_in_review)
            if REMOVE_STOPS:
                for word in words_in_review:
                    if word not in stoplist:
                        vocab[word] = vocab.get(word, 0) + 1
            else:
                for word in words_in_review:
                    vocab[word] = vocab.get(word, 0) + 1
    print("Creating sorted vocab_list from combined S and T vocab dict...")
    # Refnote: http://stackoverflow.com/questions/1915564/python-convert-a-dictionary-to-a-sorted-list-by-value-instead-of-key
    vocab_list =  sorted(iter(vocab.items()), key=itemgetter(1), reverse=True)
    review_lens = np.asanyarray(review_lens)
    review_max = np.max(review_lens)
    review_mean = np.mean(review_lens)
    review_std = np.std(review_lens)
    print("Tokenized %d reviews." % (review_lens.shape[0]) )
    f_meta.write("reviews_tokenized:\t%d\n" % (review_lens.shape[0]) )

    print( "Tokenized %d words." % (tot_words) )
    f_meta.write( "words_tokenized:\t%d\n" % (tot_words) )

    print( "Combined vocab of S and T contains %d unique tokens." % (len(vocab)) )
    f_meta.write( "vocab_size_all:\t%d\n" % (len(vocab)) )

    if len(vocab_list) < VOCAB_SIZE:
        VOCAB_SIZE = len(vocab_list)
    print ("Using vocabulary size %d." % VOCAB_SIZE)
    f_meta.write("vocab_size:\t%d\n" % VOCAB_SIZE)
    vocab_list = vocab_list[0:VOCAB_SIZE-1] # -1 is to account for having to add the UNK token to the vocab
    assert len(vocab_list) == VOCAB_SIZE-1
    print ("Most frequent word in vocab is '%s' and appeared %d times." % (vocab_list[0][0], vocab_list[0][1]) )
    f_meta.write("vocab_most_freq:\t%s,%d\n" % (vocab_list[0][0], vocab_list[0][1]) )

    print ("The least frequent word in our vocabulary is '%s' and appeared %d times." % (vocab_list[-1][0], vocab_list[-1][1]) )
    f_meta.write("vocab_least_freq:\t%s,%d\n" % (vocab_list[-1][0], vocab_list[-1][1]) )

    print ("The mean review length in our vocabulary is %f words long with std %f." % (review_mean, review_std ))
    f_meta.write("review_mean_words:\t%f\n" % (review_mean ))
    f_meta.write("review_std_words:\t%f\n" % (review_std ))
    print ("The longest review in our vocabulary is %d words long." % (review_max ))
    f_meta.write("review_max_words:\t%d\n" % (review_max ))

    print("Creating idx2word_array of the combined vocab from S and T...")
    idx2word_array = [word_ct[0] for word_ct in vocab_list] # vocab is a list of tuples: [('word1',freqct),('word2',freqct),('word3',freqct),...]
    idx2word_array.insert(0, UNK)
    idx2word_array.insert(0, MASK)
    print("Creating word2idx_dict of the combined vocab from S and T...")
    word2idx_dict = dict([(w,i) for i,w in enumerate(idx2word_array)])
    SENT_START_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    SENT_END_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    UNK_EMBEDDING = np.random.uniform(-1.0, 1.0, (WORD2VEC_D,))
    MASK_EMBEDDING = np.zeros((WORD2VEC_D,))
    defaults = {SENT_START: SENT_START_EMBEDDING, SENT_END: SENT_END_EMBEDDING, UNK: UNK_EMBEDDING, MASK: MASK_EMBEDDING}
    vsm = google_news_load_vsm(VSM_PATH)
    print("Creating word2v_dict of the combined vocab from S and T...")
    word2v_dict = dict( [ (w, safe_get_embedding(vsm, w, defaults)) for i,w in enumerate(idx2word_array)])
    print("Creating widx2v_array of the combined vocab from S and T...")
    idx2v_array = create_idx2v_array(word2idx_dict, word2v_dict, WORD2VEC_D, VOCAB_SIZE)

    print("Saving data structures to save_path:\n\t%s\n..." % (save_path))

    # SAVE idx2word_array TO FILE:
    fpath = save_path + IDX2W
    save_list(fpath, idx2word_array)
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(idx2word_array, f)

    # SAVE word2idx_dict TO FILE:
    fpath = save_path + W2IDX
    save_dict_json(fpath, word2idx_dict)
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(word2idx_dict, f)

    # SAVE word2v_dict TO FILE:
    fpath = save_path + W2V
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(word2v_dict, f)

    # SAVE idx2v_array (numpy array) TO FILE:
    fpath = save_path + IDX2V
    np.save(fpath, idx2v_array)
    # np.savetxt(fpath, idx2v_array)

    # SAVE vocab_list (list of word_ct tuples) TO FILE:
    fpath = save_path + VOCABL
    save_list_of_tups(fpath, vocab_list)
    with open(fpath + ".pickle", "wb") as f:
        pickle.dump(vocab_list, f)

    f_meta.close()
    print("END Preprocessing data...")


def amazon_process_gtors(DECODER, DATA_PATH, w2idx, idx2w, idx2v, MAXLEN_SENT, VOCAB_SIZE, BATCH_SIZE, INFINITE=True, GTOR_LIM=-1, RETURN_EMBEDDINGS=True):
    sent_gtor = amazon_load_gtor(DATA_PATH, by_review=False, GTOR_LIM=GTOR_LIM)
    while True:
        sents = list(itertools.islice(sent_gtor, BATCH_SIZE))
        if not sents or len(sents) < BATCH_SIZE:
            if INFINITE:
                sent_gtor = amazon_load_gtor(DATA_PATH, by_review=False, GTOR_LIM=GTOR_LIM)
                # print("\nfirst review is: ")
                # print(next(sent_gtor))
                # input("\n...press any key to continue\n")
                continue
            else:
                raise StopIteration
        yield amazon_process_gtor_batch(DECODER, sents, w2idx, idx2w, idx2v, MAXLEN_SENT, VOCAB_SIZE, RETURN_EMBEDDINGS)


def amazon_process_gtor_batch(DECODER, batch_of_sents, w2idx, idx2w, idx2v, MAXLEN_SENT, VOCAB_SIZE, RETURN_EMBEDDINGS=True):
    verbose = False
    if verbose:
        print("tokenizing batch sentences into words...")
    sents = [nltk.word_tokenize(sent) for sent in batch_of_sents]

    if verbose:
        tot = 0
        tot += sum([len(sent) for sent in sents])
        if verbose:
            print( "Tokenized %d words." % (tot) )
            print("len(sents) is: %d" % len(sents))

    # Replace all words not in our vocabulary with the unknown token
    for s, sent in enumerate(sents):
        sents[s] = [w if w in w2idx and w2idx[w] < VOCAB_SIZE else UNK for w in sent]

    # Create the training data
    X_train = [[w2idx[w] for w in sent] for sent in sents]
    X_train_out = np.copy(X_train)

    if verbose:
        print("padding sequences to MAXLEN_SENT %d" % MAXLEN_SENT)
    X_train = sequence.pad_sequences(X_train, maxlen=MAXLEN_SENT, padding='post')
    X_train_out = sequence.pad_sequences(X_train_out, maxlen=MAXLEN_SENT, padding='post')

    # create array of sample_weights to mask out masked values in our cost function:
    BATCH_SIZE = X_train.shape[0]
    sample_weights = np.where(X_train == 0, 0, 1)
    assert sample_weights.shape == (BATCH_SIZE, MAXLEN_SENT)

    # convert X_train from (BATCH_SIZE, MAXLEN_SENT) where each entry is an index corresponding to a word
    # to (BATCH_SIZE, MAXLEN_SENT, WORD2VEC_D)
    WORD2VEC_D = idx2v.shape[1]
    if verbose:
        print("WORD2VEC_D=%d" % WORD2VEC_D)
        print("BATCH_SIZE=%d" % BATCH_SIZE)

    assert X_train.shape == (BATCH_SIZE, MAXLEN_SENT)
    X_train_vecs = np.zeros((BATCH_SIZE, MAXLEN_SENT, WORD2VEC_D))
    X_train_vecs = np.asanyarray([[idx2v[idx] for idx in sent] for sent in X_train])

    assert type(X_train_vecs) == np.ndarray
    assert X_train_vecs.shape == (BATCH_SIZE, MAXLEN_SENT, WORD2VEC_D)

    if DECODER == "twostep":
        X_train_1hot = np.asanyarray([np_utils.to_categorical(sent, VOCAB_SIZE+1) for sent in X_train_out], dtype=np.uint16)
        # ^ using uint16 dtype to save memory space...
        if RETURN_EMBEDDINGS:
            return (X_train_vecs, X_train_1hot, sample_weights)
        else:
            return (X_train, X_train_1hot, sample_weights)

    elif DECODER == "mse":
        return (X_train_vecs, X_train_vecs, sample_weights)


def amazon_process_gtors_classifier(encoders, DATA_PATH, w2idx, idx2w, idx2v, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, BATCH_SIZE, EPOCH_SIZE, SENT2VEC_D, INFINITE=True, GTOR_LIM=-1, USES_EMBEDDINGS=True):
    rev_gtor = amazon_load_gtor(DATA_PATH, by_review=True, GTOR_LIM=GTOR_LIM)
    while True:
        reviews = list(itertools.islice(rev_gtor, BATCH_SIZE))
        if not reviews or len(reviews) < BATCH_SIZE:
            if INFINITE:
                rev_gtor = amazon_load_gtor(DATA_PATH, by_review=True, GTOR_LIM=GTOR_LIM)
                continue
            else:
                raise StopIteration
        yield amazon_process_gtor_batch_classifier(encoders, reviews, w2idx, idx2w, idx2v, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, BATCH_SIZE, SENT2VEC_D, USES_EMBEDDINGS)


def amazon_process_gtor_batch_classifier(encoders, batch_of_reviews, w2idx, idx2w, idx2v, MAXLEN_SENT, MAXLEN_REV, VOCAB_SIZE, BATCH_SIZE, SENT2VEC_D, USES_EMBEDDINGS=True):
    verbose = False # change to False after debugging...
    if verbose:
        print("tokenizing batch of reviews into sentences...")
    reviews_by_sent = [nltk.sent_tokenize(review["reviewText"]) for review in batch_of_reviews]

    tot = 0
    tot += sum([len(review) for review in reviews_by_sent])
    if verbose:
        print( "Tokenized %d sentences." % (tot) )
        print("tokenizing review sentences into words...")
    reviews = [[nltk.word_tokenize(sent) for sent in review] for review in reviews_by_sent]
    if verbose:
        print("len(reviews) is: %d" % len(reviews))
        print("len(reviews[0]) is: %d" % len(reviews[0]))
        print("reviews[0] is:")
        print(reviews[0])
        print("len(reviews[0][0]) is: %d" % len(reviews[0][0]))
        print("reviews[0][0] is:")
        print(reviews[0][0])
        print("reviews[0][0][0] is:")
        print(reviews[0][0][0])

    # Replace all words not in our vocabulary with the unknown token
    for r, review in enumerate(reviews):
        for s, sent in enumerate(review):
            reviews[r][s] = [w if w in w2idx and w2idx[w] < VOCAB_SIZE else UNK for w in sent]

    # Convert words to idxs
    reviews = [[[w2idx[w] for w in sent] for sent in review] for review in reviews]

    if verbose:
        print("padding each sentence to MAXLEN_SENT=%d words and each review to MAXLEN_REV=%d sentences..." % (MAXLEN_SENT, MAXLEN_REV))
    X_train_idxs = sequence.pad_sequences([sequence.pad_sequences(review, maxlen=MAXLEN_SENT, padding='post') for review in reviews], maxlen=MAXLEN_REV, padding='pre')
    if verbose:
        print("DONE padding sequences")
        print("type(X_train_idxs)")
        print(type(X_train_idxs))
        print("type(X_train_idxs[0])")
        print(type(X_train_idxs[0]))
        print("type(X_train_idxs[0][0])")
        print(type(X_train_idxs[0][0]))
        print("type(X_train_idxs[0][0][0])")
        print(type(X_train_idxs[0][0][0]))

        assert type(X_train_idxs) is np.ndarray
        # print("len(X_train_idxs=" % (X_train_idxs.shape[0]) )
        # print("len(X_train_idxs[0]=" % (X_train_idxs[0].shape[0]) )
        # print("len(X_train_idxs[10]=" % (X_train_idxs[10].shape[0]) )
        # print("len(X_train_idxs[0][0]=" % len(X_train_idxs[0][0]) )
        # print("len(X_train_idxs[10][1]=" % len(X_train_idxs[10][1]) )
        # print("X_train_idxs[0][0][0]=" % X_train_idxs[0][0][0] )
        # print("X_train_idxs[10][1][1]=" % X_train_idxs[10][1][1] )
        # print("converting X_train_idxs to numpy array...")
        # print("type(X_train_idxs)")
        # print("type(X_train_idxs[0])")
        # print(type(X_train_idxs[0]))
        # assert type(X_train_idxs[0]) is np.ndarray
        # print("type(X_train_idxs[0,0])")
        # print(type(X_train_idxs[0,0]))
        # assert type(X_train_idxs[0,0]) is np.ndarray

    X_train_idxs = np.asanyarray(X_train_idxs)
    # if verbose:
    #     print("type(X_train_idxs)")
    #     print(type(X_train_idxs))
    #     assert type(X_train_idxs) is np.ndarray
    #     print("type(X_train_idxs[0])")
    #     print(type(X_train_idxs[0]))
    #     assert type(X_train_idxs[0]) is np.ndarray
    #     print("type(X_train_idxs[0,0])")
    #     print(type(X_train_idxs[0,0]))
    #     assert type(X_train_idxs[0,0]) is np.ndarray
    #     print("X_train_idxs[0,0]")
    #     print(X_train_idxs[0,0])
    #     print("X_train_idxs[0,9]")
    #     print(X_train_idxs[0,9])
    #     print("X_train_idxs.shape:%s" % str(X_train_idxs.shape))
    #     input("...")


    assert X_train_idxs.shape == (BATCH_SIZE, MAXLEN_REV, MAXLEN_SENT)
    if USES_EMBEDDINGS:
        # convert X_train from (BATCH_SIZE, MAXLEN_SENT) where each entry is an index corresponding to a word
        # to (BATCH_SIZE, MAXLEN_SENT, WORD2VEC_D)
        WORD2VEC_D = idx2v.shape[1]
        if verbose:
            print("WORD2VEC_D=%d" % WORD2VEC_D)
            print("BATCH_SIZE=%d" % BATCH_SIZE)

        X_train_vecs = np.zeros((BATCH_SIZE, MAXLEN_REV, MAXLEN_SENT, WORD2VEC_D))
        X_train_vecs = np.asanyarray([[[idx2v[idx] for idx in sent] for sent in review] for review in X_train_idxs])
        assert type(X_train_vecs) == np.ndarray
        assert X_train_vecs.shape == (BATCH_SIZE, MAXLEN_REV, MAXLEN_SENT, WORD2VEC_D)
        unencoded_revs = X_train_vecs
    else:
        unencoded_revs = X_train_idxs

    # Run each sentence in each review through the autoencoder models to obtain the domain-invariant feature representation:
    X_train = np.zeros((BATCH_SIZE,MAXLEN_REV,len(encoders)*SENT2VEC_D))
    # input("...START K.function transformation")
    for r, review in enumerate(unencoded_revs):
        for e,encoder in enumerate(encoders):
            # print("type(review)")
            # print(type(review))
            # print("review.shape")
            # print(review.shape)
            # input("...")
            encoded_rev = encoder([review,0])[0]
            # encoded_rev = encoder.predict(review, batch_size=MAXLEN_REV) # ALTERNATIVE WAY in case first way doesn't work
            # print("type(encoded_rev)")
            # print(type(encoded_rev))
            # print("encoded_rev.shape")
            # print(encoded_rev.shape)
            # print("encoded_rev[9]")
            # print(encoded_rev[9])
            # input("...")
            X_train[r, : , e*SENT2VEC_D : (e+1)*SENT2VEC_D] = encoded_rev
    # input("...END K.function transformation")

    # if verbose:
    #     print("type(X_train)")
    #     print(type(X_train))
    #     assert type(X_train) is np.ndarray
    #     print("type(X_train[0])")
    #     print(type(X_train[0]))
    #     assert type(X_train[0]) is np.ndarray
    #     print("type(X_train[0,0])")
    #     print(type(X_train[0,0]))
    #     assert type(X_train[0,0]) is np.ndarray
    #     print("X_train.shape: %s" % str(X_train.shape))
    #     assert X_train.shape == (BATCH_SIZE,MAXLEN_REV,len(encoders)*SENT2VEC_D)
    #     print((X_train[0,0]))
    #     input("...")
    #     print((X_train[0,8]))
    #     input("...")
    #     print((X_train[0,9]))
    #     input("...")
    #     print((X_train[2,8]))
    #     input("...")
    #     print((X_train[2,9]))
    #     input("...")

    if verbose:
        print("extracting labels for training data...")
    # Y_train[0] = 0 if the "overall" rating <= 3.0 (i.e. negative sentiment) and 1 if the "overall" rating > 3
    # Y_train = np.asanyarray([0 if float(review["overall"]) <= 3 else 1 for review in batch_of_reviews], dtype=np.uint16)
    Y_train = np.asanyarray([0 if float(review["overall"]) <= 3 else 1 for review in batch_of_reviews], dtype=np.uint16)

    # Y_train = np.asanyarray([[1,0] if review["overall"] <= 3 else [0,1] for review in batch_of_reviews], dtype=np.uint16)
    # Y_train = np.asanyarray([np_utils.to_categorical(sent, VOCAB_SIZE+1) for sent in X_train], dtype=np.uint16)
    # if verbose:
    #     print("DONE extracting Y_train (ratings) data")
    #     print("type(Y_train)")
    #     print(type(Y_train))
    #     assert type(Y_train) is np.ndarray
    #     print("Y_train[0]")
    #     print(Y_train[0])
    #     print("Y_train[1]")
    #     print(Y_train[1])
    #     print("Y_train.shape: %s" % str(Y_train.shape))
    #     assert Y_train.shape == (BATCH_SIZE,)
    #     print("Y_train")
    #     print(Y_train)
    #     input("...")
    # print("X_train")
    # print(X_train)
    # print("Y_train")
    # print(Y_train)
    return (X_train, Y_train)


#############################################3
# END AMAZON DATASET UTILS
#############################################3
