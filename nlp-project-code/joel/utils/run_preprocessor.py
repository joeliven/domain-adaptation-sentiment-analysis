import os
import data_utils_nlp_mse as du
import itertools
import argparse

##########################################################################
# START MAIN
##########################################################################
def main():
    # PROCESS COMMANDLINE ARGS:
    cmdln = prep_cmdln_parser()
    args = cmdln.parse_args()
    args_dict = vars(args)
    vprint(args, "START PREPROCESSOR...")
    # PRINT COMMANDLINE ARGS:
    for key,value in args_dict.items():
        vprint(args, "%s:\t%s", (key,value))
    if args.GPU == True:
        vprint(args, "Using GPU if available/possible.")

    # PREPROCESS THE RAW S&T DOMAIN DATA SETS TO BUILD NECESSARY VOCAB DATASTRUCTURES:
    du.amazon_preprocess_datasets(args.S_PATH, args.T_PATH, args.SAVE_DIR, args.SAVE_NAME, args.VSM_PATH,
                                  WORD2VEC_D=args.WORD2VEC_D, VOCAB_SIZE=args.VOCAB_SIZE, REMOVE_STOPS=args.REMOVE_STOPS,
                                STOPLIST_PATH=args.STOPLIST_PATH, BRIEF=args.BRIEF, DATA_LIM=args.DATA_LIM, TO_LOWER=args.TO_LOWER)
##########################################################################
# END MAIN
##########################################################################


def vprint(args, string, fillers=None):
    """
        args: cmdln args object
        str: string to print
    """
    if args.VERBOSE == True:
        if fillers is not None:
            print(string % fillers)
        else:
            print(string)

def prep_cmdln_parser():
    usage = "usage: %prog [options]"
    cmdln = argparse.ArgumentParser(usage)
    cmdln.add_argument("-v", "--verbose", action="store_true", dest="VERBOSE", default=False,
                                help="print out more information during runtime [default: %default.")
    cmdln.add_argument("--brief", action="store_true", dest="BRIEF", default=False,
                                help="set this flag to use the brief version of the preprocesser/vocab builder (does not compute as many statistics while building vocab, so it runs slightly faster) [default: %default]")
    cmdln.add_argument("--gpu", action="store_true", dest="GPU", default=False,
                                help="train the model on a GPU (if supported) [default: %default].")

    cmdln.add_argument("--s-path", action="store", dest="S_PATH", default="", required=True,
                                help="load the raw Source Domain data from specified file [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--t-path", action="store", dest="T_PATH", default="", required=True,
                                help="load the raw Target Domain data from specified file [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--save-dir", action="store", dest="SAVE_DIR", default="", required=True,
                                help="the directory in which all generated files (data structures for this combined vocab) should be saved [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--save-name", action="store", dest="SAVE_NAME", default="S_T_",
                                help="the name to be prepended to the generated files (data structures for this combined vocab) [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--vsm-path", action="store", dest="VSM_PATH", default="S_T_", required=True,
                                help="the path in which the pretrained Google vector space model (VSM) is stored [default: %default]. Note, this must be specified.")
    cmdln.add_argument("--stoplist-path", action="store", dest="STOPLIST_PATH", default="",
                                help="the path in which the list of stopwords to remove (if any) is stored [default: %default].")
    cmdln.add_argument("--remove-stops", action="store_true", dest="REMOVE_STOPS", default=False,
                                help="indicates that stopwords should be removed while building the vocab during preprocessing [default: %default].")
    cmdln.add_argument("--word2v-d", action="store", dest="WORD2VEC_D", default=300, type=int,
                                help="the dimension to use for the word embedding layear of the model [default: %default]. Note, this must match the dimensionality of the index2vector matrix and word2vector dictionary that are passed to the program as well.")
    cmdln.add_argument("--vocab-size", action="store", dest="VOCAB_SIZE", default=10000, type=int,
                                help="the size limit of the vocabulary being built from the Source (S) and Target (T) datasets [default: %default].")
    cmdln.add_argument("--data-lim", action="store", dest="DATA_LIM", default=-1, type=int,
                                help="set this to limit the amount of the data that is processed while constructing the vocabulary [default: %default].")
    cmdln.add_argument("--to-lower", action="store_true", dest="TO_LOWER", default=False,
                                help="set this flag to avoid converting all words in data set to lower case while processing vocabulary [default: %default].")
    return cmdln


if __name__ == "__main__":
    main()

