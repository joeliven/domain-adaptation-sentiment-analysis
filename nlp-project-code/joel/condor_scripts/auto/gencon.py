#############################################
#          python script to call numerous condor job scripts                     #
#############################################
import sys, os


SCRATCH = "/scratch/cluster/joeliven/datasets/"
HOME = "/u/joeliven/Documents/"

universe = "universe=vanilla"
Initialdir = "Initialdir=/u/joeliven/Documents"
Group = """+Group=\"GRAD\""""
Project = """+Project=\"INSTRUCTIONAL\""""
ProjectDescription = """+ProjectDescription=\"CS388 Final Project\""""
GPUJob = "+GPUJob=true"
# requirements = "requirements=(TARGET.GPUSlot && TitanBlack == True)"
requirements = "requirements=(TARGET.GPUSlot)"

Notification = "Notification=complete"
Notify_user = "Notify_user=joeliven@gmail.com"
Queue="Queue 1"


def write_condor_script(program, S, T, Spct,Tpct, vac, cpu_gpu, auto_type):
    name="%s.%s%s_%s%s.%s" %  (vac, S, Spct, T, Tpct, auto_type)
    # log_name="%s%s_%s%s.%s" %  (S, Spct, T, Tpct, vac)
    savefile="condor." + name + ".sh"
    executable="ml/condor_scripts/" + vac + "/" + "run_" + name + ".sh"
    with open(savefile, 'w') as f:
        Executable = "Executable = %s%s" % (HOME,executable)
        Log="Log=%samazon/logs/%s.log.$(Cluster)" % (SCRATCH, name)
        Output="Output=%samazon/logs/%s.out.$(Cluster)" % (SCRATCH, name)
        Error="Error=%samazon/logs/%s.err.$(Cluster)" % (SCRATCH, name)

        f.write(universe)
        f.write("\n")
        f.write(Initialdir)
        f.write("\n")
        f.write(Executable)
        f.write("\n")
        f.write(Group)
        f.write("\n")
        f.write(Project)
        f.write("\n")
        f.write(ProjectDescription)
        f.write("\n")
        f.write(GPUJob)
        f.write("\n")
        f.write(requirements)
        f.write("\n")
        f.write(Notification)
        f.write("\n")
        f.write(Notify_user)
        f.write("\n")
        f.write(Log)
        f.write("\n")
        f.write(Output)
        f.write("\n")
        f.write(Error)
        f.write("\n")
        f.write(Queue)

        f.write("\n")



################################################################3

def write_bash_script(program, S, T, Spct,Tpct, vac, cpu_gpu, auto_type):
    savefile="run_%s.%s%s_%s%s.%s.sh" %  (vac, S, Spct, T, Tpct, auto_type)
    with open(savefile, 'w') as f:
        f.write('#!/usr/local/bin/bash\n')
        f.write('P="python"\n')
        f.write('PROGRAM="%s"\n' % program)
        f.write('TRAINING_DATA="%samazon/train/%s_%s/%s%s_%s%s.train"\n' % (SCRATCH, S, T, S, Spct, T, Tpct))
        f.write('VAL_DATA="%samazon/train/%s_%s/%s%s_%s%s.val"\n' % (SCRATCH, S, T, S, Spct, T, Tpct))
        f.write('LOAD_MODEL_ARCH="%samazon/autoencoders/%s/%s%s/%s%s_%s%s.%s.arch.json"\n' % (SCRATCH, auto_type, S, T, S, Spct, T, Tpct, vac))
        f.write('LOAD_MODEL_WEIGHTS="%samazon/autoencoders/%s/%s%s/%s%s_%s%s.%s.weights.hdf5"\n' % (SCRATCH, auto_type, S, T, S, Spct, T, Tpct, vac))
        f.write('SAVE_MODEL_ARCH="%samazon/autoencoders/%s/%s%s/%s%s_%s%s.%s.arch.json"\n' % (SCRATCH, auto_type, S, T, S, Spct, T, Tpct, vac))
        f.write('SAVE_MODEL_WEIGHTS="%samazon/autoencoders/%s/%s%s/%s%s_%s%s.%s.weights.final.hdf5"\n' % (SCRATCH, auto_type, S, T, S, Spct, T, Tpct, vac))
        f.write('SAVE_CHECKPOINTS_WEIGHTS="%samazon/autoencoders/%s/%s%s/%s%s_%s%s.%s.weights.hdf5"\n' % (SCRATCH, auto_type, S, T, S, Spct, T, Tpct, vac))
        f.write('WORD2VEC_D="300"\n')
        f.write('SENT2VEC_D="128"\n')
        f.write('VOCAB_SIZE="20000"\n')
        f.write('BATCH_SIZE="32"\n')
        f.write('N_EPOCHS="100"\n')
        f.write('EPOCH_SIZE="64000"\n')
        f.write('MAXLEN_SENT="20"\n')
        f.write('DECODER="mse"\n')
        f.write('OPTIMIZER="adam"\n')
        f.write('LR_START="0.001"\n')
        f.write('LR_END="0.001"\n')
        f.write('LR_HYP="0.001"\n')
        f.write('DROPOUT_ELSTM_U="0.1"\n')
        f.write('DROPOUT_ELSTM_W="0.1"\n')
        f.write('DROPOUT_DLSTM_U="0.1"\n')
        f.write('DROPOUT_DLSTM_W="0.1"\n')
        f.write('SOFTMAX_L2_W="0.0005"\n')
        # f.write('SOFTMAX_L2_B="0.0005"\n')
        f.write('VAL_BATCHES="10"\n')
        f.write('GTOR_LIM="100000"\n')
        f.write('IDX2V="%samazon/vocab/%s_%s/%s_%s_idx2v.npy"\n' % (SCRATCH, S, T, S, T))
        f.write('IDX2W="%samazon/vocab/%s_%s/%s_%s_idx2w.list.pickle"\n' % (SCRATCH, S, T, S, T))
        f.write('W2V="%samazon/vocab/%s_%s/%s_%s_w2v.dict.pickle"\n' % (SCRATCH, S, T, S, T))
        f.write('W2IDX="%samazon/vocab/%s_%s/%s_%s_w2idx.dict.pickle"\n' % (SCRATCH, S, T, S, T))
        f.write('VOCAB_LIST="%samazon/vocab/%s_%s/%s_%s_vocab.list.pickle"\n' % (SCRATCH, S, T, S, T))
        f.write('EXPID="%s.%s%s_%s%s"\n' % (vac, S, Spct, T, Tpct))
        f.write("#############################\n# SET UP ENVIRONMENT\n#############################\n")
        f.write('export PATH="/u/joeliven/anaconda3/bin:$PATH"\n')
        f.write('PYTHONPATH="${PYTHONPATH}:/u/joeliven/Documents/ml/"\n')
        f.write('export PYTHONPATH\n')
        f.write('echo "path: "\n')
        f.write('echo $PATH\n')
        f.write('echo "pythonpath: "\n')
        f.write('echo $PYTHONPATH\n')
        f.write('source activate ml34\n')
        f.write('cuda=/opt/cuda-7.5\n')
        f.write('# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod\n')
        f.write('# export LD_LIBRARY_PATH=$cuDNN/lib64:$cuda/lib64:$LD_LIBRARY_PATH\n')
        f.write('# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod\n')
        f.write('# export CPATH=$cuDNN/include:$CPATH\n')
        f.write('# export LIBRARY_PATH=$cuDNN/lib64:$LD_LIBRARY_PATH\n')
        f.write('# export CUDNN_PATH=$cuDNN\n')
        f.write('export LD_LIBRARY_PATH=$cuda/lib64:$LD_LIBRARY_PATH\n')
        f.write('export LIBRARY_PATH=$LD_LIBRARY_PATH\n')
        f.write('# export CUDNN_PATH=$cuDNN\n')
        f.write('export CUDA_HOME=$cuda\n')
        f.write('echo "pwd: "\n')
        f.write('pwd\n')

        f.write("#############################\n# EXECUTION\n#############################\n")

        # f.write('time "$P" "$PROGRAM" \\ \n')
        f.write('THEANO_FLAGS=device=%s,floatX=float32 time "$P" "$PROGRAM" \\\n' %(cpu_gpu))
        f.write('--verbose \\\n')
        f.write('--gpu \\\n')
        f.write('--train \\\n')
        f.write('--training-data "$TRAINING_DATA" \\\n')
        f.write('--val-data "$VAL_DATA" \\\n')
        # f.write('--test \\\n')
        # f.write('--test-data "$TEST_DATA" \\\n')
        # f.write('--load-model \\\n')
        # f.write('--load-model-arch "$LOAD_MODEL_ARCH" \\\n')
        # f.write('--load-model-weights "$LOAD_MODEL_WEIGHTS" \\\n')
        f.write('--save-model \\\n')
        f.write('--save-model-arch "$SAVE_MODEL_ARCH" \\\n')
        f.write('--save-model-weights "$SAVE_MODEL_WEIGHTS" \\\n')
        f.write('--save-checkpoints \\\n')
        f.write('--save-checkpoints-weights "$SAVE_CHECKPOINTS_WEIGHTS" \\\n')
        f.write('--word2v-d "$WORD2VEC_D" \\\n')
        f.write('--sent2v-d "$SENT2VEC_D" \\\n')
        f.write('--vocab-size "$VOCAB_SIZE" \\\n')
        f.write('--batch-size "$BATCH_SIZE" \\\n')
        f.write('--n-epochs "$N_EPOCHS" \\\n')
        f.write('--epoch-size "$EPOCH_SIZE" \\\n')
        f.write('--maxlen-sent "$MAXLEN_SENT" \\\n')
        f.write('--decoder "$DECODER" \\\n')
        f.write('--optimizer "$OPTIMIZER" \\\n')
        f.write('--lr-start "$LR_START" \\\n')
        f.write('--lr-end "$LR_END" \\\n')
        f.write('--lr-hyp "$LR_HYP" \\\n')
        f.write('--dropout-elstm-w "$DROPOUT_ELSTM_W" \\\n')
        f.write('--dropout-elstm-u "$DROPOUT_ELSTM_U" \\\n')
        f.write('--dropout-dlstm-w "$DROPOUT_DLSTM_W" \\\n')
        f.write('--dropout-dlstm-u "$DROPOUT_DLSTM_U" \\\n')
        f.write('--softmax-l2-w "$SOFTMAX_L2_W" \\\n')
        # f.write('--softmax-l2-b "$SOFTMAX_L2_B" \\\n')
        f.write('--val-batches "$VAL_BATCHES" \\\n')
        f.write('--gtor-lim "$GTOR_LIM" \\\n')
        f.write('--idx2v "$IDX2V" \\\n')
        f.write('--idx2w "$IDX2W" \\\n')
        f.write('--w2idx "$W2IDX" \\\n')
        f.write('--w2v "$W2V" \\\n')
        f.write('--w2idx "$W2IDX" \\\n')
        f.write('--vocab-list "$VOCAB_LIST" \\\n')
        f.write('--expID "$EXPID"\n')
        f.write('source deactivate\n')





##################################################################
S_T = [
        ("B","E"),
        ("B","H"),
        ("B","M"),
        ("E","H"),
        ("E","M"),
        ]
Spct_Tpct = [
        ("100","0"),
        ("50","50"),
        ("0","100"),
]

vac="auto"
program="ml/nlp_proj/lstm_autoencoder_gtor_mse.py"
Spct="100"
Tpct="0"
vac="auto"
cpu_gpu="gpu"
auto_type="mse128"

params = [[program, s,t, Spct,Tpct, vac, cpu_gpu, auto_type] for (s,t) in S_T for (Spct,Tpct) in Spct_Tpct]

for args in params:
    write_condor_script(*args)

for i,args in enumerate(params):
    write_bash_script(*args)
#     # print("i:%d"% i)
#     # print(args)






