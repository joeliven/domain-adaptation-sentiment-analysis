universe=vanilla
Initialdir=/u/joeliven/Documents
Executable = /u/joeliven/Documents/ml/condor_scripts/auto/run_auto.B50_E50.mse128.sh
+Group="GRAD"
+Project="INSTRUCTIONAL"
+ProjectDescription="CS388 Final Project"
+GPUJob=true
requirements=(TARGET.GPUSlot)
Notification=complete
Notify_user=joeliven@gmail.com
Log=/scratch/cluster/joeliven/datasets/amazon/logs/auto.B50_E50.mse128.log.$(Cluster)
Output=/scratch/cluster/joeliven/datasets/amazon/logs/auto.B50_E50.mse128.out.$(Cluster)
Error=/scratch/cluster/joeliven/datasets/amazon/logs/auto.B50_E50.mse128.err.$(Cluster)
Queue 1
