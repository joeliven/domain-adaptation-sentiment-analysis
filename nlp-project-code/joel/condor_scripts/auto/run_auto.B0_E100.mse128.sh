#!/usr/local/bin/bash
P="python"
PROGRAM="ml/nlp_proj/lstm_autoencoder_gtor_mse.py"
TRAINING_DATA="/scratch/cluster/joeliven/datasets/amazon/train/B_E/B0_E100.train"
VAL_DATA="/scratch/cluster/joeliven/datasets/amazon/train/B_E/B0_E100.val"
LOAD_MODEL_ARCH="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.arch.json"
LOAD_MODEL_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.weights.hdf5"
SAVE_MODEL_ARCH="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.arch.json"
SAVE_MODEL_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.weights.final.hdf5"
SAVE_CHECKPOINTS_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.weights.hdf5"
WORD2VEC_D="300"
SENT2VEC_D="128"
VOCAB_SIZE="20000"
BATCH_SIZE="32"
N_EPOCHS="100"
EPOCH_SIZE="64000"
MAXLEN_SENT="20"
DECODER="mse"
OPTIMIZER="adam"
LR_START="0.001"
LR_END="0.001"
LR_HYP="0.001"
DROPOUT_ELSTM_U="0.1"
DROPOUT_ELSTM_W="0.1"
DROPOUT_DLSTM_U="0.1"
DROPOUT_DLSTM_W="0.1"
SOFTMAX_L2_W="0.0005"
VAL_BATCHES="10"
GTOR_LIM="100000"
IDX2V="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_idx2v.npy"
IDX2W="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_idx2w.list.pickle"
W2V="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_w2v.dict.pickle"
W2IDX="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_w2idx.dict.pickle"
VOCAB_LIST="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_vocab.list.pickle"
EXPID="auto.B0_E100"
#############################
# SET UP ENVIRONMENT
#############################
export PATH="/u/joeliven/anaconda3/bin:$PATH"
PYTHONPATH="${PYTHONPATH}:/u/joeliven/Documents/ml/"
export PYTHONPATH
echo "path: "
echo $PATH
echo "pythonpath: "
echo $PYTHONPATH
source activate ml34
cuda=/opt/cuda-7.5
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export LD_LIBRARY_PATH=$cuDNN/lib64:$cuda/lib64:$LD_LIBRARY_PATH
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export CPATH=$cuDNN/include:$CPATH
# export LIBRARY_PATH=$cuDNN/lib64:$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export LD_LIBRARY_PATH=$cuda/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export CUDA_HOME=$cuda
echo "pwd: "
pwd
#############################
# EXECUTION
#############################
THEANO_FLAGS=device=gpu,floatX=float32 time "$P" "$PROGRAM" \
--verbose \
--gpu \
--train \
--training-data "$TRAINING_DATA" \
--val-data "$VAL_DATA" \
--save-model \
--save-model-arch "$SAVE_MODEL_ARCH" \
--save-model-weights "$SAVE_MODEL_WEIGHTS" \
--save-checkpoints \
--save-checkpoints-weights "$SAVE_CHECKPOINTS_WEIGHTS" \
--word2v-d "$WORD2VEC_D" \
--sent2v-d "$SENT2VEC_D" \
--vocab-size "$VOCAB_SIZE" \
--batch-size "$BATCH_SIZE" \
--n-epochs "$N_EPOCHS" \
--epoch-size "$EPOCH_SIZE" \
--maxlen-sent "$MAXLEN_SENT" \
--decoder "$DECODER" \
--optimizer "$OPTIMIZER" \
--lr-start "$LR_START" \
--lr-end "$LR_END" \
--lr-hyp "$LR_HYP" \
--dropout-elstm-w "$DROPOUT_ELSTM_W" \
--dropout-elstm-u "$DROPOUT_ELSTM_U" \
--dropout-dlstm-w "$DROPOUT_DLSTM_W" \
--dropout-dlstm-u "$DROPOUT_DLSTM_U" \
--softmax-l2-w "$SOFTMAX_L2_W" \
--val-batches "$VAL_BATCHES" \
--gtor-lim "$GTOR_LIM" \
--idx2v "$IDX2V" \
--idx2w "$IDX2W" \
--w2idx "$W2IDX" \
--w2v "$W2V" \
--w2idx "$W2IDX" \
--vocab-list "$VOCAB_LIST" \
--expID "$EXPID"
source deactivate
