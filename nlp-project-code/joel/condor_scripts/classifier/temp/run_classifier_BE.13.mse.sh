#!/usr/local/bin/bash

P="python"
PROGRAM="ml/nlp_proj/lstm_classifier_mse.py"

#############################
# MODIFY THESE PATHS/VARIABLES AS NEEDED
#############################

# MODIFY THESE PATHS/VARIABLES AS NEEDED
TRAINING_DATA="datasets/amazon/balanced/B_E/Books100_Electronics0_100000.txt"
VAL_DATA="datasets/amazon/balanced/B_E/Books100_Electronics0_1000_val.txt"
TEST_DATA_SOURCE="../../datasets/amazon/balanced/test/B_test.txt"
TEST_DATA_TARGET="../../datasets/amazon/balanced/test/E_test.txt"

NUM_ENCODERS="3"
AUTOENCODER_ARCH_FILE1="datasets/amazon/balanced/B_E/autoencoder_100000/B100_E0_autoencoder.13.mse.arch.json"
AUTOENCODER_WEIGHTS_FILE1="datasets/amazon/balanced/B_E/autoencoder_100000/B100_E0_autoencoder.13.mse.weights.hdf5"
# AUTOENCODER_ARCH_FILE2="datasets/amazon/balanced/B_E/autoencoder_100000/B75_E25_autoencoder.0.arch.json"
# AUTOENCODER_WEIGHTS_FILE2="datasets/amazon/balanced/B_E/autoencoder_100000/B75_E25_autoencoder.0.weights.hdf5"
AUTOENCODER_ARCH_FILE3="datasets/amazon/balanced/B_E/autoencoder_100000/B50_E50_autoencoder.13.mse.arch.json"
AUTOENCODER_WEIGHTS_FILE3="datasets/amazon/balanced/B_E/autoencoder_100000/B50_E50_autoencoder.13.mse.weights.hdf5"
# AUTOENCODER_ARCH_FILE4="datasets/amazon/balanced/B_E/autoencoder_100000/B25_E75_autoencoder.0.arch.json"
# AUTOENCODER_WEIGHTS_FILE4="datasets/amazon/balanced/B_E/autoencoder_100000/B25_E75_autoencoder.0.weights.hdf5"
AUTOENCODER_ARCH_FILE5="datasets/amazon/balanced/B_E/autoencoder_100000/B0_E100_autoencoder.13.mse.arch.json"
AUTOENCODER_WEIGHTS_FILE5="datasets/amazon/balanced/B_E/autoencoder_100000/B0_E100_autoencoder.13.mse.weights.hdf5"

LOAD_MODEL_ARCH="datasets/amazon/balanced/B_E/classifier_100000/BE_classifier.13.mse.arch.json"
LOAD_MODEL_WEIGHTS="datasets/amazon/balanced/B_E/classifier_100000/BE_classifier.13.mse.weights.hdf5"

SAVE_MODEL_ARCH="datasets/amazon/balanced/B_E/classifier_100000/BE_classifier.13.mse.arch.json"
SAVE_MODEL_WEIGHTS="datasets/amazon/balanced/B_E/classifier_100000/BE_classifier.13.mse.weights.final.hdf5"

SAVE_CHECKPOINTS_WEIGHTS="datasets/amazon/balanced/B_E/classifier_100000/BE_classifier.13.mse.weights.hdf5"

WORD2VEC_D="300"
SENT2VEC_D="128"
REV2VEC_D="300"
VOCAB_SIZE="20000" #10000
BATCH_SIZE="25" #64
N_EPOCHS="2" #20
EPOCH_SIZE="10000" #6400
MAXLEN_SENT="20"
MAXLEN_REV="5" #10
OPTIMIZER="adam"
LR_START="0.001"
LR_END="0.001"
LR_HYP="0.001"
DROPOUT_LSTM_W="0.2" # dropout rate for input gates (note that 0.2 for both _U and _W breaks the program on my pc due to recursion limit)
DROPOUT_LSTM_U="0.2" # dropout rate for recurrent connections (note that 0.2 for both _U and _W breaks the program on my pc due to recursion limit)
DENSE_L2_W="0.001"
DENSE_L2_B="0.001"
VAL_BATCHES="5" #20
TEST_BATCHES="5" #20
GTOR_LIM="10000" #20
GTOR_LIM_TEST="10000" #20
IDX2V="datasets/amazon/balanced/B_E/vocab_20000/BE_idx2v.npy"
IDX2W="datasets/amazon/balanced/B_E/vocab_20000/BE_idx2w.list.pickle"
W2IDX="datasets/amazon/balanced/B_E/vocab_20000/BE_w2idx.dict.pickle"
W2V="datasets/amazon/balanced/B_E/vocab_20000/BE_w2v.dict.pickle"
VOCAB_LIST="datasets/amazon/balanced/B_E/vocab_20000/BE_vocab.list.pickle"
EXPID="BE_classifier.13.mse"


#############################
# SETUP ENV
#############################

# added by Anaconda2 2.4.0 installer
# export PATH="/u/joeliven/anaconda3/bin:/u/joeliven/Documents/ml:$PATH"
export PATH="/u/joeliven/anaconda3/bin:$PATH"
PYTHONPATH="${PYTHONPATH}:/u/joeliven/Documents/ml/"
export PYTHONPATH
echo "path: "
echo $PATH
echo "pythonpath: "
echo $PYTHONPATH

source activate ml34

cuda=/opt/cuda-7.5
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export LD_LIBRARY_PATH=$cuDNN/lib64:$cuda/lib64:$LD_LIBRARY_PATH
# export CPATH=$cuDNN/include:$CPATH
# export LIBRARY_PATH=$cuDNN/lib64:$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN

export LD_LIBRARY_PATH=$cuda/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN

export CUDA_HOME=$cuda
echo "pwd: "
pwd


#############################
# EXECUTION
#############################

THEANO_FLAGS=device=gpu,floatX=float32 time "$P" "$PROGRAM" \
--verbose \
--gpu \
--uses-embeddings \
--train \
--training-data "$TRAINING_DATA" \
--test \
--test-data-source "$TEST_DATA_SOURCE" \
--test-data-target "$TEST_DATA_TARGET" \
--val-data "$VAL_DATA" \
--num-encoders "$NUM_ENCODERS" \
--autoencoder-arch "$AUTOENCODER_ARCH_FILE3" \
--autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE3" \
--save-model \
--save-model-arch "$SAVE_MODEL_ARCH" \
--save-model-weights "$SAVE_MODEL_WEIGHTS" \
--save-checkpoints \
--save-checkpoints-weights "$SAVE_CHECKPOINTS_WEIGHTS" \
--word2v-d "$WORD2VEC_D" \
--sent2v-d "$SENT2VEC_D" \
--rev2v-d "$REV2VEC_D" \
--vocab-size "$VOCAB_SIZE" \
--batch-size "$BATCH_SIZE" \
--n-epochs "$N_EPOCHS" \
--epoch-size "$EPOCH_SIZE" \
--maxlen-sent "$MAXLEN_SENT" \
--maxlen-rev "$MAXLEN_REV" \
--optimizer "$OPTIMIZER" \
--lr-start "$LR_START" \
--lr-end "$LR_END" \
--lr-hyp "$LR_HYP" \
--dropout-lstm-w "$DROPOUT_LSTM_W" \
--dropout-lstm-u "$DROPOUT_LSTM_U" \
--dense-l2-w "$DENSE_L2_W" \
--dense-l2-b "$DENSE_L2_B" \
--val-batches "$VAL_BATCHES" \
--test-batches "$TEST_BATCHES" \
--gtor-lim "$GTOR_LIM" \
--gtor-lim-test "$GTOR_LIM_TEST" \
--idx2v "$IDX2V" \
--idx2w "$IDX2W" \
--w2idx "$W2IDX" \
--w2v "$W2V" \
--vocab-list "$VOCAB_LIST" \
--expID "$EXPID"

# --autoencoder-arch "$AUTOENCODER_ARCH_FILE1" \
# --autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE1" \
# --autoencoder-arch "$AUTOENCODER_ARCH_FILE2" \
# --autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE2" \
# --autoencoder-arch "$AUTOENCODER_ARCH_FILE3" \
# --autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE3" \
# --autoencoder-arch "$AUTOENCODER_ARCH_FILE4" \
# --autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE4" \
# --autoencoder-arch "$AUTOENCODER_ARCH_FILE5" \
# --autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE5" \

# --test \
# --test-data "$TEST_DATA" \
# --load-model \
# --load-model-arch "$LOAD_MODEL_ARCH" \
# --load-model-weights "$LOAD_MODEL_WEIGHTS" \


source deactivate

####################################################################
#!/usr/local/bin/bash
P="python"
PROGRAM="ml/nlp_proj/lstm_autoencoder_gtor_mse.py"
TRAINING_DATA="/scratch/cluster/joeliven/datasets/amazon/train/B_E/B0_E100.train"
VAL_DATA="/scratch/cluster/joeliven/datasets/amazon/train/B_E/B0_E100.val"
LOAD_MODEL_ARCH="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.arch.json"
LOAD_MODEL_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.weights.hdf5"
SAVE_MODEL_ARCH="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.arch.json"
SAVE_MODEL_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.weights.final.hdf5"
SAVE_CHECKPOINTS_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.weights.hdf5"
WORD2VEC_D="300"
SENT2VEC_D="128"
VOCAB_SIZE="20000"
BATCH_SIZE="32"
N_EPOCHS="100"
EPOCH_SIZE="64000"
MAXLEN_SENT="20"
DECODER="mse"
OPTIMIZER="adam"
LR_START="0.001"
LR_END="0.001"
LR_HYP="0.001"
DROPOUT_ELSTM_U="0.1"
DROPOUT_ELSTM_W="0.1"
DROPOUT_DLSTM_U="0.1"
DROPOUT_DLSTM_W="0.1"
SOFTMAX_L2_W="0.0005"
VAL_BATCHES="10"
GTOR_LIM="100000"
IDX2V="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_idx2v.npy"
IDX2W="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_idx2w.list.pickle"
W2V="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_w2v.dict.pickle"
W2IDX="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_w2idx.dict.pickle"
VOCAB_LIST="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_vocab.list.pickle"
EXPID="auto.B0_E100"
#############################
# SET UP ENVIRONMENT
#############################
export PATH="/u/joeliven/anaconda3/bin:$PATH"
PYTHONPATH="${PYTHONPATH}:/u/joeliven/Documents/ml/"
export PYTHONPATH
echo "path: "
echo $PATH
echo "pythonpath: "
echo $PYTHONPATH
source activate ml34
cuda=/opt/cuda-7.5
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export LD_LIBRARY_PATH=$cuDNN/lib64:$cuda/lib64:$LD_LIBRARY_PATH
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export CPATH=$cuDNN/include:$CPATH
# export LIBRARY_PATH=$cuDNN/lib64:$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export LD_LIBRARY_PATH=$cuda/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export CUDA_HOME=$cuda
echo "pwd: "
pwd
#############################
# EXECUTION
#############################
THEANO_FLAGS=device=gpu,floatX=float32 time "$P" "$PROGRAM" \
--verbose \
--gpu \
--train \
--training-data "$TRAINING_DATA" \
--val-data "$VAL_DATA" \
--save-model \
--save-model-arch "$SAVE_MODEL_ARCH" \
--save-model-weights "$SAVE_MODEL_WEIGHTS" \
--save-checkpoints \
--save-checkpoints-weights "$SAVE_CHECKPOINTS_WEIGHTS" \
--word2v-d "$WORD2VEC_D" \
--sent2v-d "$SENT2VEC_D" \
--vocab-size "$VOCAB_SIZE" \
--batch-size "$BATCH_SIZE" \
--n-epochs "$N_EPOCHS" \
--epoch-size "$EPOCH_SIZE" \
--maxlen-sent "$MAXLEN_SENT" \
--decoder "$DECODER" \
--optimizer "$OPTIMIZER" \
--lr-start "$LR_START" \
--lr-end "$LR_END" \
--lr-hyp "$LR_HYP" \
--dropout-elstm-w "$DROPOUT_ELSTM_W" \
--dropout-elstm-u "$DROPOUT_ELSTM_U" \
--dropout-dlstm-w "$DROPOUT_DLSTM_W" \
--dropout-dlstm-u "$DROPOUT_DLSTM_U" \
--softmax-l2-w "$SOFTMAX_L2_W" \
--val-batches "$VAL_BATCHES" \
--gtor-lim "$GTOR_LIM" \
--idx2v "$IDX2V" \
--idx2w "$IDX2W" \
--w2idx "$W2IDX" \
--w2v "$W2V" \
--w2idx "$W2IDX" \
--vocab-list "$VOCAB_LIST" \
--expID "$EXPID"
source deactivate

