universe=vanilla
Initialdir=/u/joeliven/Documents
Executable = /u/joeliven/Documents/ml/condor_scripts/classifier/run_classifier.EB.mse128_64.sh
+Group="GRAD"
+Project="INSTRUCTIONAL"
+ProjectDescription="CS388 Final Project"
+GPUJob=true
requirements=(TARGET.GPUSlot)
Notification=complete
Notify_user=joeliven@gmail.com
Log=/scratch/cluster/joeliven/datasets/amazon/logs/classifier.EB.mse128_64.log.$(Cluster)
Output=/scratch/cluster/joeliven/datasets/amazon/logs/classifier.EB.mse128_64.out.$(Cluster)
Error=/scratch/cluster/joeliven/datasets/amazon/logs/classifier.EB.mse128_64.err.$(Cluster)
Queue 1
