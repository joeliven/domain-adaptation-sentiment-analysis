#
# python script to submit numerous condor job scripts
#

import sys, os

files = [f for f in os.listdir('.') if(f.startswith('condor.test.') and os.path.isfile(f))]
print("condor jobs to submit: " + str(len(files)))
ct =0
for file in files:
    ct += 1
    print(str(ct) + ": " + file)

    cmd = "condor_submit " + file + '&'
    os.system(cmd)

print("done submitting " + str(ct) + " condor jobs.")
