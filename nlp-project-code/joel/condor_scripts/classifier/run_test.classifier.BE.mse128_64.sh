#!/usr/local/bin/bash
P="python"
PROGRAM="ml/nlp_proj/lstm_classifier_mse.py"
TRAINING_DATA="/scratch/cluster/joeliven/datasets/amazon/train/B_E/B100_E0.train"
VAL_DATA="/scratch/cluster/joeliven/datasets/amazon/train/B_E/B100_E0.val"
TEST_DATA_SOURCE="/scratch/cluster/joeliven/datasets/amazon/test/B.test"
TEST_DATA_TARGET="/scratch/cluster/joeliven/datasets/amazon/test/E.test"
LOAD_MODEL_ARCH="/scratch/cluster/joeliven/datasets/amazon/classifiers/mse128_64/BE/BE.classifier.arch.json"
LOAD_MODEL_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/classifiers/mse128_64/BE/BE.classifier.weights.hdf5"
SAVE_MODEL_ARCH="/scratch/cluster/joeliven/datasets/amazon/classifiers/mse128_64/BE/BE.classifier.arch.json"
SAVE_MODEL_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/classifiers/mse128_64/BE/BE.classifier.weights.final.hdf5"
SAVE_CHECKPOINTS_WEIGHTS="/scratch/cluster/joeliven/datasets/amazon/classifiers/mse128_64/BE/BE.classifier.weights.hdf5"
NUM_ENCODERS="3"
AUTOENCODER_ARCH_FILE1="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B100_E0.auto.arch.json"
AUTOENCODER_WEIGHTS_FILE1="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B100_E0.auto.weights.hdf5"
AUTOENCODER_ARCH_FILE2="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B50_E50.auto.arch.json"
AUTOENCODER_WEIGHTS_FILE2="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B50_E50.auto.weights.hdf5"
AUTOENCODER_ARCH_FILE3="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.arch.json"
AUTOENCODER_WEIGHTS_FILE3="/scratch/cluster/joeliven/datasets/amazon/autoencoders/mse128/BE/B0_E100.auto.weights.hdf5"
WORD2VEC_D="300"
SENT2VEC_D="128"
REV2VEC_D="64"
VOCAB_SIZE="20000"
BATCH_SIZE="32"
N_EPOCHS="50"
EPOCH_SIZE="32000"
MAXLEN_SENT="20"
MAXLEN_REV="5"
OPTIMIZER="adam"
LR_START="0.001"
LR_END="0.001"
LR_HYP="0.001"
DROPOUT_LSTM_U="0.1"
DROPOUT_LSTM_W="0.1"
DENSE_L2_W="0.0005"
DENSE_L2_B="0.0005"
VAL_BATCHES="10"
TEST_BATCHES="20"
GTOR_LIM="100000"
GTOR_LIM_TEST="10000"
IDX2V="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_idx2v.npy"
IDX2W="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_idx2w.list.pickle"
W2V="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_w2v.dict.pickle"
W2IDX="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_w2idx.dict.pickle"
VOCAB_LIST="/scratch/cluster/joeliven/datasets/amazon/vocab/B_E/B_E_vocab.list.pickle"
EXPID="classifier.BE"
#############################
# SET UP ENVIRONMENT
#############################
export PATH="/u/joeliven/anaconda3/bin:$PATH"
PYTHONPATH="${PYTHONPATH}:/u/joeliven/Documents/ml/"
export PYTHONPATH
echo "path: "
echo $PATH
echo "pythonpath: "
echo $PYTHONPATH
source activate ml34
cuda=/opt/cuda-7.5
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export LD_LIBRARY_PATH=$cuDNN/lib64:$cuda/lib64:$LD_LIBRARY_PATH
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export CPATH=$cuDNN/include:$CPATH
# export LIBRARY_PATH=$cuDNN/lib64:$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export LD_LIBRARY_PATH=$cuda/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export CUDA_HOME=$cuda
echo "pwd: "
pwd
#############################
# EXECUTION
#############################
THEANO_FLAGS=device=gpu,floatX=float32 time "$P" "$PROGRAM" \
--verbose \
--gpu \
--uses-embeddings \
--test \
--test-data-source "$TEST_DATA_SOURCE" \
--test-data-target "$TEST_DATA_TARGET" \
--load-model \
--load-model-arch "$LOAD_MODEL_ARCH" \
--load-model-weights "$LOAD_MODEL_WEIGHTS" \
--num-encoders "$NUM_ENCODERS" \
--autoencoder-arch "$AUTOENCODER_ARCH_FILE1" \
--autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE1" \
--autoencoder-arch "$AUTOENCODER_ARCH_FILE2" \
--autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE2" \
--autoencoder-arch "$AUTOENCODER_ARCH_FILE3" \
--autoencoder-weights "$AUTOENCODER_WEIGHTS_FILE3" \
--word2v-d "$WORD2VEC_D" \
--sent2v-d "$SENT2VEC_D" \
--rev2v-d "$REV2VEC_D" \
--vocab-size "$VOCAB_SIZE" \
--batch-size "$BATCH_SIZE" \
--n-epochs "$N_EPOCHS" \
--epoch-size "$EPOCH_SIZE" \
--maxlen-sent "$MAXLEN_SENT" \
--maxlen-rev "$MAXLEN_REV" \
--optimizer "$OPTIMIZER" \
--lr-start "$LR_START" \
--lr-end "$LR_END" \
--lr-hyp "$LR_HYP" \
--dropout-lstm-w "$DROPOUT_LSTM_W" \
--dropout-lstm-u "$DROPOUT_LSTM_U" \
--dense-l2-w "$DENSE_L2_W" \
--dense-l2-b "$DENSE_L2_B" \
--val-batches "$VAL_BATCHES" \
--test-batches "$TEST_BATCHES" \
--gtor-lim "$GTOR_LIM" \
--gtor-lim-test "$GTOR_LIM_TEST" \
--idx2v "$IDX2V" \
--idx2w "$IDX2W" \
--w2idx "$W2IDX" \
--w2v "$W2V" \
--vocab-list "$VOCAB_LIST" \
--expID "$EXPID"
source deactivate
