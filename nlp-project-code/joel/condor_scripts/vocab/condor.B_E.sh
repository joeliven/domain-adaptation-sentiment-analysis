universe=vanilla
Initialdir=/u/joeliven/Documents
Executable = /u/joeliven/Documents/ml/condor_scripts/vocab/B_E.sh
+Group="GRAD"
+Project="INSTRUCTIONAL"
+ProjectDescription="CS388 Final Project"
Notification=complete
Notify_user=joeliven@gmail.com
Log=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.B_E.log.$(Cluster)
Output=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.B_E.out.$(Cluster)
Error=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.B_E.err.$(Cluster)
Queue 1
