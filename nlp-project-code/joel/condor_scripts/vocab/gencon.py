#############################################
#          python script to call numerous condor job scripts                     #
#############################################
import sys, os


SCRATCH = "/scratch/cluster/joeliven/datasets/"
HOME = "/u/joeliven/Documents/"

universe = "universe=vanilla"
Initialdir = "Initialdir=/u/joeliven/Documents"
Group = """+Group=\"GRAD\""""
Project = """+Project=\"INSTRUCTIONAL\""""
ProjectDescription = """+ProjectDescription=\"CS388 Final Project\""""
Notification = "Notification=complete"
Notify_user = "Notify_user=joeliven@gmail.com"
Queue="Queue 1"


def write_condor_script(vac, S,T):
    name = S+"_"+T
    savefile="condor."+S+"_"+T+".sh"
    executable="ml/condor_scripts/" + vac + "/" + name + ".sh"
    with open(savefile, 'w') as f:
        Executable = "Executable = %s%s" % (HOME,executable)
        Log="Log=%samazon/logs/%s.%s.log.$(Cluster)" % (SCRATCH, vac, name)
        Output="Output=%samazon/logs/%s.%s.out.$(Cluster)" % (SCRATCH, vac, name)
        Error="Error=%samazon/logs/%s.%s.err.$(Cluster)" % (SCRATCH, vac, name)

        f.write(universe)
        f.write("\n")
        f.write(Initialdir)
        f.write("\n")
        f.write(Executable)
        f.write("\n")
        f.write(Group)
        f.write("\n")
        f.write(Project)
        f.write("\n")
        f.write(ProjectDescription)
        f.write("\n")
        f.write(Notification)
        f.write("\n")
        f.write(Notify_user)
        f.write("\n")
        f.write(Log)
        f.write("\n")
        f.write(Output)
        f.write("\n")
        f.write(Error)
        f.write("\n")
        f.write(Queue)

        f.write("\n")



################################################################3

def write_bash_script(program, S, T, Spct,Tpct, vac, cpu_gpu):
    savefile=S+"_"+T+".sh"
    with open(savefile, 'w') as f:
        f.write('#!/usr/local/bin/bash\n')
        f.write('P="python"\n')
        f.write('PROGRAM="%s"\n' % program)
        f.write('S_PATH="%samazon/train/%s_%s/%s%s_%s%s.train"\n' % (SCRATCH, S, T, S, Spct, T, Tpct))
        f.write('T_PATH="%samazon/train/%s_%s/%s%s_%s%s.train"\n' % (SCRATCH, S, T, S, Tpct, T, Spct))
        f.write('SAVE_DIR="%samazon/%s/%s_%s"\n' % (SCRATCH, vac, S, T))
        f.write('SAVE_NAME="%s_%s"\n' % (S,T))
        f.write('VSM_PATH="%svsms/word2vec/google_news/GoogleNews-vectors-negative300.bin"\n' % (SCRATCH))
        f.write('WORD2V_D="300"\n')
        f.write('VOCAB_SIZE="20000"\n')
        f.write('DATA_LIM="100000"\n')
        f.write("#############################\n# SET UP ENVIRONMENT\n#############################\n")

        f.write('export PATH="/u/joeliven/anaconda3/bin:$PATH"\n')
        f.write('PYTHONPATH="${PYTHONPATH}:/u/joeliven/Documents/ml/"\n')
        f.write('export PYTHONPATH\n')
        f.write('echo "path: "\n')
        f.write('echo $PATH\n')
        f.write('echo "pythonpath: "\n')
        f.write('echo $PYTHONPATH\n')
        f.write('source activate ml34\n')
        f.write('cuda=/opt/cuda-7.5\n')
        f.write('# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod\n')
        f.write('# export LD_LIBRARY_PATH=$cuDNN/lib64:$cuda/lib64:$LD_LIBRARY_PATH\n')
        f.write('# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod\n')
        f.write('# export CPATH=$cuDNN/include:$CPATH\n')
        f.write('# export LIBRARY_PATH=$cuDNN/lib64:$LD_LIBRARY_PATH\n')
        f.write('# export CUDNN_PATH=$cuDNN\n')
        f.write('export LD_LIBRARY_PATH=$cuda/lib64:$LD_LIBRARY_PATH\n')
        f.write('export LIBRARY_PATH=$LD_LIBRARY_PATH\n')
        f.write('# export CUDNN_PATH=$cuDNN\n')
        f.write('export CUDA_HOME=$cuda\n')
        f.write('echo "pwd: "\n')
        f.write('pwd\n')

        f.write("#############################\n# EXECUTION\n#############################\n")
        f.write('THEANO_FLAGS=device=%s,floatX=float32 time "$P" "$PROGRAM" \\\n' %(cpu_gpu))
        # f.write('time "$P" "$PROGRAM" \\ \n')
        f.write('--verbose \\\n')
        f.write('--brief \\\n')
        f.write('--gpu \\\n')
        f.write('--s-path "$S_PATH" \\\n')
        f.write('--t-path "$T_PATH" \\\n')
        f.write('--save-dir "$SAVE_DIR" \\\n')
        f.write('--save-name "$SAVE_NAME" \\\n')
        f.write('--vsm-path "$VSM_PATH" \\\n')
        f.write('--word2v-d "$WORD2V_D" \\\n')
        f.write('--vocab-size "$VOCAB_SIZE" \\\n')
        f.write('--data-lim "$DATA_LIM" \\\n')
        f.write('--to-lower \n')
        f.write('source deactivate\n')



S_T = [
        ("B","E"),
        ("B","H"),
        ("B","M"),
        ("E","H"),
        ("E","M"),
        ]

vac="vocab"
program="ml/utils/run_preprocessor.py"
Spct="100"
Tpct="0"
vac="vocab"
cpu_gpu="cpu"

params1 = [[vac, s,t] for (s,t) in S_T]
for args in params1:
    write_condor_script(*args)


params2 = [[program, s,t, Spct, Tpct, vac, cpu_gpu] for (s,t) in S_T]
for args in params2:
    write_bash_script(*args)






