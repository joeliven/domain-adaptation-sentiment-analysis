universe=vanilla
Initialdir=/u/joeliven/Documents
Executable = /u/joeliven/Documents/ml/condor_scripts/vocab/B_M.sh
+Group="GRAD"
+Project="INSTRUCTIONAL"
+ProjectDescription="CS388 Final Project"
Notification=complete
Notify_user=joeliven@gmail.com
Log=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.B_M.log.$(Cluster)
Output=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.B_M.out.$(Cluster)
Error=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.B_M.err.$(Cluster)
Queue 1
