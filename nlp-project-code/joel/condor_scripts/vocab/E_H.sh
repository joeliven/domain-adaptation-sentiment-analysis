#!/usr/local/bin/bash
P="python"
PROGRAM="ml/utils/run_preprocessor.py"
S_PATH="/scratch/cluster/joeliven/datasets/amazon/train/E_H/E100_H0.train"
T_PATH="/scratch/cluster/joeliven/datasets/amazon/train/E_H/E0_H100.train"
SAVE_DIR="/scratch/cluster/joeliven/datasets/amazon/vocab/E_H"
SAVE_NAME="E_H"
VSM_PATH="/scratch/cluster/joeliven/datasets/vsms/word2vec/google_news/GoogleNews-vectors-negative300.bin"
WORD2V_D="300"
VOCAB_SIZE="20000"
DATA_LIM="100000"
#############################
# SET UP ENVIRONMENT
#############################
export PATH="/u/joeliven/anaconda3/bin:$PATH"
PYTHONPATH="${PYTHONPATH}:/u/joeliven/Documents/ml/"
export PYTHONPATH
echo "path: "
echo $PATH
echo "pythonpath: "
echo $PYTHONPATH
source activate ml34
cuda=/opt/cuda-7.5
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export LD_LIBRARY_PATH=$cuDNN/lib64:$cuda/lib64:$LD_LIBRARY_PATH
# cuDNN=/u/ebanner/builds/cudnn-7.0-linux-x64-v3.0-prod
# export CPATH=$cuDNN/include:$CPATH
# export LIBRARY_PATH=$cuDNN/lib64:$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export LD_LIBRARY_PATH=$cuda/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$LD_LIBRARY_PATH
# export CUDNN_PATH=$cuDNN
export CUDA_HOME=$cuda
echo "pwd: "
pwd
#############################
# EXECUTION
#############################
THEANO_FLAGS=device=cpu,floatX=float32 time "$P" "$PROGRAM" \
--verbose \
--brief \
--gpu \
--s-path "$S_PATH" \
--t-path "$T_PATH" \
--save-dir "$SAVE_DIR" \
--save-name "$SAVE_NAME" \
--vsm-path "$VSM_PATH" \
--word2v-d "$WORD2V_D" \
--vocab-size "$VOCAB_SIZE" \
--data-lim "$DATA_LIM" \
--to-lower 
source deactivate
