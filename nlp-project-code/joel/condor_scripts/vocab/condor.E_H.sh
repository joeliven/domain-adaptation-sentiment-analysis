universe=vanilla
Initialdir=/u/joeliven/Documents
Executable = /u/joeliven/Documents/ml/condor_scripts/vocab/E_H.sh
+Group="GRAD"
+Project="INSTRUCTIONAL"
+ProjectDescription="CS388 Final Project"
Notification=complete
Notify_user=joeliven@gmail.com
Log=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.E_H.log.$(Cluster)
Output=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.E_H.out.$(Cluster)
Error=/scratch/cluster/joeliven/datasets/amazon/logs/vocab.E_H.err.$(Cluster)
Queue 1
