#!/bin/zsh

function hostname.get() {
        if [ $# -eq 0 ]; then
                echo "ls5"
        else
                echo "${1}"
        fi
}

function signin() {
        hostname=$(hostname.get "$@")
        ssh kaiyuanw@${hostname}.tacc.utexas.edu
}

function L2R() {
        if [ $# -lt 2 -o $# -gt 4 ]; then
                echo "Invalid arguments."
                return
        fi
        if [ "${1}" = "-r" ]; then
                mode="${1}"; shift
        else
                mode=""
        fi
        localPath=${1}; shift
        remotePath=${1}; shift
        hostname=$(hostname.get "$@")
        scp ${mode} ${localPath} kaiyuanw@${hostname}.tacc.utexas.edu:${remotePath}
}

function R2L() {
        if [ $# -lt 2 -o $# -gt 4 ]; then
                echo "Invalid arguments."
                return
        fi
        if [ "${1}" = "-r" ]; then
                mode="${1}"; shift
        else
                mode=""
        fi
        remotePath=${1}; shift
        localPath=${1}; shift
        hostname=$(hostname.get "$@")
        scp ${mode} kaiyuanw@${hostname}.tacc.utexas.edu:${remotePath} ${localPath}
}

function help() {
        echo "Try commands like:"
        echo "./help.sh --connectTACC"
        echo "./help.sh --r2l -r /u/mooney/cs388-code/nlp/lm/ P1"
        echo "./help.sh --l2r -r P1 /u/kaiyuanw"
        echo "./help.sh --p2-jobs-set"
        echo "./help.sh --p2-corpus-set"
        echo "./help.sh --p2-logs-get"
}

function p2.condorjobs.set() {
        L2R -r P2/condor-jobs /u/kaiyuanw/NLP/P2
}

function p2.corpus.set() {
        L2R -r P2/MalletFormatter/mallet-data /u/kaiyuanw/NLP/P2/MalletFormatter
}

function p2.logs.get() {
        R2L -r /u/kaiyuanw/NLP/P2/kw26528/logs P2/kw26528
}

function main() {
        case $1 in
        -c|--connectTACC) shift
                signin "$@"
                ;;
        -u|--l2r) shift
                L2R "$@"
                ;;
        -d|--r2l) shift
                R2L "$@"
                ;;
        -h|--help) shift
                help "$@"
                ;;
        --p2-jobs-set) shift
                p2.condorjobs.set
                ;;
        --p2-corpus-set) shift
                p2.corpus.set
                ;;
        --p2-logs-get) shift
                p2.logs.get
                ;;
        *)
                echo "Check arguments";;
        esac
}

main $@